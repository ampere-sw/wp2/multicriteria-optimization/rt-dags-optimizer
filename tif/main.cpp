/*
TIF (Top Island First) heusristic to set island placement/frequency configurations for DAGs.

Authors: 
    * Alexandre Amory (June 2022), ReTiS Lab, Scuola Sant'Anna, Pisa, Italy.

Usage Example:
$> cd heuristics/tif
$> mkdir build; cd build; cmake ..; make
$> ./tif 
*/

// std c++ libs
#include <fstream>
#include <iostream>
#include <string>
#include <cstdlib>
#include <cassert>
#include <cstring>
#include <cmath>
#include <chrono>
#include <algorithm>

#include <experimental/filesystem>
#ifdef __cpp_lib_filesystem
    #include <filesystem>
    using fs = std::filesystem;
#elif __cpp_lib_experimental_filesystem
    #include <experimental/filesystem>
    namespace fs = std::experimental::filesystem;
#else
    #error "no filesystem support ='("
#endif

// external c++ libs
#include <yaml-cpp/yaml.h>

// internal c++ libs
#include <check_yaml.h>
#include <hw_model_mat.h>
#include <sw_model_mat.h>
#include <eval_cfg.h>
#include <output_formats.h>
#include <arguments.h>
#include <log.h>

using namespace std;

/////////////////
// those static/globals are declared here just to avoid changing the original code
/////////////////
// static class attribute related to the transformation of the unrelated set into string, 
// used in the minizinc model run during the utilization check
string UnrelSeq::s_minizinc_str;
uint8_t UnrelSeq::s_longest_set;

// counts how many times minizinc was executed
uint32_t g_minizinc_counter=0;
uint32_t g_minizinc_ok_counter=0;
uint32_t g_worst_fit_counter=0;


// globals actually used
uint8_t g_n_islands;
uint8_t g_n_dags;
uint32_t g_n_tasks;
vector <uint32_t> g_tasks_per_dag;


class TIF_t{
public:
    float deadline;
    float wcet;
    uint32_t task_id;
    uint32_t dag_id;

    TIF_t(){
        deadline = 0.0f;
        wcet = 0.0f;
        task_id = 0;
        dag_id = 0;
    }
};

std::ostream& operator<<(std::ostream &out, TIF_t const&v) {
    out << v.dag_id << ", " << v.task_id << ", " << v.deadline << ", " << v.wcet;
    return out;
}

void usage(){
    cout << "TIF (Top Island First) heusristic to set island placement/frequency configurations for DAGs.\n";
    cout << "2022, ReTiS Laboratory, Scuola Sant'Anna, Pisa, Italy\n";
    cout << "Usage: tif <SW YAML file> <HW YAML file> [options...]\n";
    cout << "  Options:\n";
    cout << "    -h|--help ................ Show this help message and exit\n";
    cout << "    -oyaml filename.yaml .... Specify destination filename for the the optimization output\n";
    cout << "    -ip power ............... Specify the upper bound for power\n";
}

// set freq_idx and task placement such that it represents 
// all tasks running at the highest capacity island with the max frequency
void max_performance( const SwModelMat* sw, const HwModelMat* hw,
    vector <uint8_t> *freq_idx, vector < vector < uint32_t > >  *placement){

    uint8_t i,d;

    freq_idx->resize(g_n_islands);
    placement->resize(g_n_islands);
    for(uint32_t i=0;i<g_n_islands;++i){
        (*placement)[i].resize(g_n_dags);
    }

    // assign the highest freq to each island
    for (i=0;i<g_n_islands;++i){
        (*freq_idx)[i] = hw->islands[i].freqs.size()-1;
    }

/*  example of semantics
    vector < vector < uint32_t > >  placement = {
        {1,1},   // island 0 task task 0 of dag0 and task 0 of dag1
        {2,0},   // island 1 task task 1 of dag0 and no task of dag1
        {4,0}    // island 2 task task 1 of dag0 and no task of dag1
    };
*/
    // no task is assigned to the lowest capacity islands
    for(i=0;i<g_n_islands-1;++i){
        for(d=0;d<g_n_dags;++d){
            (*placement)[i][d] = 0;
        }
    }
    // all tasks are assigned to the highest capacity island
    for(d=0;d<g_n_dags;++d){
        // set all bits to 1
        (*placement)[g_n_islands-1][d] = (uint32_t)(pow(2.0,(double)g_tasks_per_dag[d])-1.0);
    }    
}

void  update_task_list(vector <TIF_t> *tasks, 
    const vector < vector <float> > *wcet, 
    const vector < vector <float> > *dline){

    uint32_t t_idx=0;
    uint8_t d,t;
    for (d=0;d<g_n_dags;++d){
        for (t=0;t<g_tasks_per_dag[d];++t){
            (*tasks)[t_idx].deadline = (*dline)[d][t];
            (*tasks)[t_idx].wcet = (*wcet)[d][t];
            (*tasks)[t_idx].dag_id = d;
            (*tasks)[t_idx].task_id = t;
            ++t_idx;
        }
    }
    
    // and sort the TIF data in descending deadline, which is proportional to the wcet.
    // this way, the 'heaviest' tasks are first
    sort(tasks->begin(),tasks->end(),
        [] (const auto& lhs, const auto& rhs) {
        return lhs.deadline > rhs.deadline;
    });

    LOG(INFO,"Sorted task list:");
    #if LOG_LEVEL >= 3 // info mode
        cout << "dag, task, deadline, wcet\n";
        for(t=0; t< tasks->size(); ++t){
            cout << (*tasks)[t] << endl;
        }
    #endif

} 

// move a task t a island up or down
// return false when the no move is performed, e.g. when the task is already at the min island in case of move_down
bool move_island(TIF_t *t, vector < vector < uint32_t > >  *placement, bool move_down){
    int i;
    uint32_t mask;

    for(i=g_n_islands-1;i>=0;--i){
        mask = (*placement)[i][t->dag_id] & 1 << t->task_id;
        if (mask){
            // and place it on the new one
            if (move_down){
                if(i>0){
                    LOG(INFO,"Moving task %u of dag %u from island %u to island %u\n",t->task_id, t->dag_id, i, i-1);
                    (*placement)[i-1][t->dag_id] |= 1 << t->task_id;
                }else{
                    LOG(INFO,"Task %u already at the min island\n",t->task_id);
                    // task already at the minimal island
                    return false;
                }
            }else{
                if (i<g_n_islands-1){
                    LOG(INFO,"Moving task %u of dag %u from island %u to island %u\n",t->task_id, t->dag_id, i, i+1);
                    (*placement)[i+1][t->dag_id] |= 1 << t->task_id;
                }else{
                    LOG(INFO,"Task %u already at the max island\n",t->task_id);
                    // task already at the top island
                    return false;
                }
            }
            // remove task t from the old island ...
            (*placement)[i][t->dag_id] &= ~mask;
            goto end_downgrade_island;
        }
    }
    end_downgrade_island:
    return true;
}



int main(int argc, char* argv[])
{
    string plat_name;
    string plat_filename;
    string dag_filename;
    string output_yaml;
    float starting_power = std::numeric_limits<float>::infinity();

    auto start = chrono::high_resolution_clock::now();
    parse_arguments(argc, argv, dag_filename, plat_name, plat_filename, 
        output_yaml, &starting_power);

    // check whether the yaml has the expected format
    if (!check_yaml(dag_filename.c_str(),plat_filename.c_str())){
        usage();
        exit(EXIT_FAILURE);
    }

    // load the YAML files
    HwModelMat hw(plat_filename.c_str());
    SwModelMat sw(hw.n_islands,dag_filename.c_str());
    
    g_n_islands = (uint8_t)hw.islands.size();
    g_n_dags = (uint8_t)sw.sw_model.dags.size();
    g_n_tasks = sw.sw_model.get_n_actual_tasks();
    g_tasks_per_dag.resize(g_n_dags);
    uint8_t d,t;
    for (d=0;d<g_n_dags;++d){
        g_tasks_per_dag[d] = sw.sw_model.dags[d].tasks.size();
    }    

    // get the wcet for each task assuming that they are all assigned to the top capacity island w its highest frequency
    vector <uint8_t> freq_idx;
    vector < vector < uint32_t > >  placement;
    max_performance(&sw,&hw,&freq_idx,&placement);

    // configure and run the constraint checker, ignoring constraint errors
    EvalCfg evalCfg(&sw,&hw,true);
    float curr_power; // get the power calculated for the current mapping configuration
    float best_power;
    evalCfg.evaluate(&placement, &freq_idx, starting_power, &curr_power);
    const vector < vector <float> > *wcet = evalCfg.get_wcet_ptr();
    const vector < vector <float> > *dline = evalCfg.get_deadline_ptr();

    // copy data from the evalCfg into TIF struct and sort it in descending workload size
    vector <TIF_t> tasks(g_n_tasks);
    update_task_list(&tasks, wcet, dline);
 
    // now the constraints must be checked
    EvalCfg evalCfg_loop(&sw,&hw);
    evalCfg_loop.evaluate(&placement, &freq_idx, starting_power, &curr_power);
    best_power = curr_power;
    wcet = evalCfg_loop.get_wcet_ptr();
    dline = evalCfg_loop.get_deadline_ptr();
    const bool move_down = true;

    // initial task placement without reducing the frequency
    // keep trying to move tasks from island to island until no task can be moved
    cout << "\n\n#######################################\n";
    cout <<     "Moving tasks to lower capacity islands:\n";
    cout <<     "#######################################\n\n";    
    t=0;
    do{
        // keep moving the task to lower capacity island until any constraint fail
        if (!move_island(&(tasks[t]),&placement,move_down)){
            // go to the next task
            t++;
            continue;
        }
        if (evalCfg_loop.evaluate(&placement, &freq_idx, best_power, &curr_power)){
            // keep this new mapping
            update_task_list(&tasks, wcet, dline);
            // print the current result
            evalCfg_loop.dump();
            // restart the search
            t=0;
            // it is not expected to have a move that does not reduce the power
            assert(curr_power<=best_power);
            best_power = curr_power;
        }else{
            // revert to the previous mapping
            move_island(&(tasks[t]),&placement,!move_down);
            // go to the next task
            t++;
        }
    }while(t< tasks.size());

    // reevaluate again just to revert the internal values in case the last execution have been reverted
    evalCfg_loop.evaluate(&placement, &freq_idx, best_power, &curr_power);
    assert(curr_power<=best_power);
    best_power = curr_power;
    evalCfg_loop.dump();
    cout << "\n\n#########################################\n";
    cout <<     "Result from task placement onto islands:\n";
    cout <<     "#########################################\n";
    // compact output format used to ease comparison with other optimization tools
    cout << compact_output_format (&hw, &sw, &placement,&freq_idx,best_power) << endl;
    cout << "\n\n";

    // keep the previous placement but try to reduce freq
    // for each island
    //   // keep reducing the freq until the constraints fail
    //   stop=false
    //   do {
    //     reduce freq
    //     if (!constraints)
    //       revert freq change
    //       stop
    //   } while(!stop);

    cout << "\n\n################################\n";
    cout <<     "Reducing the island frequencies:\n";
    cout <<     "################################\n\n";
    for(uint8_t i=0;i<g_n_islands;++i){
        bool stop=false;
        assert(freq_idx[i]>0);
        do{
            // keep reducing the freq of island i until it fails
            freq_idx[i]--;
            if (evalCfg_loop.evaluate(&placement, &freq_idx, best_power, &curr_power)){
                // keep this new mapping
                update_task_list(&tasks, wcet, dline);
                // print the current result
                evalCfg_loop.dump();
                // it is not expected to have a move that does not reduce the power
                assert(curr_power<=best_power);
                best_power = curr_power;
            }else{
                // revert to the previous freq change
                freq_idx[i]++;
                stop= true;
            }
        }while(!stop && freq_idx[i]>0);

    }

    // reevaluate again just to revert the internal values in case the last execution have been reverted
    bool valid_solution = evalCfg_loop.evaluate(&placement, &freq_idx, best_power, &curr_power);
    assert(curr_power<=best_power);
    best_power = curr_power;
    cout << "\n";
    evalCfg_loop.dump();

    // Get the execution time 
    auto duration = chrono::duration_cast<chrono::microseconds>(chrono::high_resolution_clock::now() - start);
    cout << endl << "Execution time : " << duration.count() << " microseconds\n\n";

    cout << "####################################################\n";
    cout << "Final result after reducing the island frequencies:\n";
    cout << "####################################################\n";
    // compact output format used to ease comparison with other optimization tools
    cout << compact_output_format (&hw, &sw, &placement,&freq_idx,best_power) << endl;

    if (valid_solution){
        // detailed output format used to gather statistics
        string detailed_output = evalCfg_loop.dump_yaml();
        detailed_yaml_output_format(detailed_output,
            "tif", plat_name, plat_filename, dag_filename, duration.count(), output_yaml);
        cout << "Valid solution found !\n\n";
    }else{
        cout << "Unable to find a valid solution !\n\n";
    }
    // the return code is 0 only if a valid solution was found
    return valid_solution ? 0 : 1;
}
