# TIF - Top Island First

This is supposed to be a faster heuristic than bb-search, but farther from the optimal solution. This is also potentially more scalable than bb-search.

**TODO**: still need to fix the heuristic name

## Assumptions

 - Islands are sorted in deacreasing capacity
 - Frequencies are sorted in deacreasing order

## Steps

- Calculate wcet/deadline for each task assuming they are all assigned to the top island at its top frequency. no constraint it checked at this point;
- Create a list with tasks, ordered by decreasing deadline (it could be wcet, slack, etc);
- Fill the 1st island with the first tasks until some constraint fail, probably the utilization constraint;
- Then go to the next (lower capacity) island and keep adding the remaning tasks until some constraint fail;
- Keep doing this until all lowest island is 'full' or all tasks were placed
- if all tasks were placed and the current mapping holds the constraints, then try to reduce the frequency of the 1st island as long as the constraints are still met. Continue reducing the frequency of all other islands until all islands are covered.


## TODO
 - what happens after all tasks are placed onto the top island but the constraint are not met?!?!?
   - build an exemllpe with this corner case

## Compile 

```
$> mkdir build; cd build
$> cmake ..
$> make
```

## Running an example

```
$> cd power-aware-optimization/bb-search/data
$> ../../heuristics/LIF/build/lif ../ex1-sw.yaml odroid-xu3-hw.yaml
...
#######################################
Moving tasks to lower capacity islands:
#######################################

power: 3.2773 mapping [2:;0,1,3,4,5,6,7:0] at freq [1500 hz,2000 hz]
power: 3.1471 mapping [2,5:;0,1,3,4,6,7:0] at freq [1500 hz,2000 hz]
power: 3.0530 mapping [2,5,7:;0,1,3,4,6:0] at freq [1500 hz,2000 hz]
power: 2.9589 mapping [0,2,5,7:;1,3,4,6:0] at freq [1500 hz,2000 hz]
power: 2.8648 mapping [0,1,2,5,7:;3,4,6:0] at freq [1500 hz,2000 hz]
power: 2.7707 mapping [0,1,2,5,7:;3,4,6:0] at freq [1500 hz,2000 hz]

#########################################
Result from task placement onto islands:
#########################################

BB-SEARCH FORMAT:
Power: 2.770652 W
Island 0 (1500.00 Hz): 0,1,0,1,0,0,1,1,1,
Island 1 (2000.00 Hz): 1,0,1,0,1,1,0,0,0,

################################
Reducing the island frequencies:
################################

power: 2.7707 mapping [0,1,2,5,7:;3,4,6:0] at freq [1400 hz,2000 hz]
power: 2.7300 mapping [0,1,2,5,7:;3,4,6:0] at freq [1400 hz,1900 hz]
power: 2.4709 mapping [0,1,2,5,7:;3,4,6:0] at freq [1400 hz,1800 hz]
power: 2.2379 mapping [0,1,2,5,7:;3,4,6:0] at freq [1400 hz,1700 hz]
power: 2.0781 mapping [0,1,2,5,7:;3,4,6:0] at freq [1400 hz,1600 hz]
power: 1.9334 mapping [0,1,2,5,7:;3,4,6:0] at freq [1400 hz,1600 hz]

####################################################
Final result after reducing the island frequencies:
####################################################

BB-SEARCH FORMAT:
Power: 1.933354 W
Island 0 (1400.00 Hz): 0,1,0,1,0,0,1,1,1,
Island 1 (1600.00 Hz): 1,0,1,0,1,1,0,0,0,
```

