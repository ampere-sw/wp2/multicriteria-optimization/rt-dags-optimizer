Multicriteria-optimization of real-time DAGs on heterogeneous platforms
================================================================================

This project contains software tools for multi-criteria optimization
of real-time DAGs running under SCHED_DEADLINE on Linux, on top of
high-performance embedded platforms with multi-core, GPU and FPGA
hardware acceleration.

The tools are the following:
- dag: optimum solver through the use of the Gurobi (that must be pre-installed on the system)
- tif: heuristic top-island-first solver
- bb-search: heuristic optimizer inspired to branch-and-bound solvers


COMPILING

Just type make in the top folder.
The tools can be found in:

- dag/dag
- tif/build/tif
- bb-search/build/bb-search


USE

Just launch any of the tools with -h, to get a descriptive help
message on its usage.

For additional information, refer to the documentation of the individual tools located in
- tif/readme.md
- bb-search/readme.md

