#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <sys/time.h> //gettimeofday
#include <float.h>
#include <sys/types.h>
#include <unistd.h>

#include "dag.h"
#include "log.h"


sys_t *p_sys = NULL;
char *out_lp_fname = NULL;
char *out_sol_fname = NULL;
char *out_sols_pname = "gurobi_sols";
char *out_dot_fname = NULL;
char *out_yml_fname = NULL;
char *out_sh_fname = NULL;
char *out_yml_rtsim_dags_fname = NULL;
char *out_yml_rtsim_isl_fname = NULL;
char *out_yaml_fname = NULL;
char *gurobi_log_fname = NULL;
char *platform_name = NULL;
int gurobi_max_threads = MAX_GUROBI_THREADS;
// power upper bound. Most likely calculated by a cheap/fast heuristics
// TODO: upper bound logic not implemented into the gurobi model
float initial_power=FLT_MAX;
float lp_scale = 0.0;
float max_util = DEF_MAX_UTIL;
int create_non_binary_bounds = 0;

// Tom's simplest example
// s { {a1 a2} | {b1 b2} } e
dag_t *build_dag1() {
  printf("ex1:\n");
  dag_t *p_dag = malloc(sizeof(dag_t));
  assert(p_dag != NULL);
  dag_init(p_dag, 100, 100);
  int s = dag_add_elem(p_dag, 10, 0, -1);
  int a1 = dag_add_elem(p_dag, 5, 0, -1);
  int a2 = dag_add_elem(p_dag, 8, 0, -1);
  int b1 = dag_add_elem(p_dag, 7, 0, -1);
  int b2 = dag_add_elem(p_dag, 4, 0, -1);
  int e = dag_add_elem(p_dag, 3, 0, -1);
  dag_add_prev(p_dag, a1, s);
  dag_add_prev(p_dag, a2, a1);
  dag_add_prev(p_dag, b1, s);
  dag_add_prev(p_dag, b2, b1);
  dag_add_prev(p_dag, e, a2);
  dag_add_prev(p_dag, e, b2);
  dag_comp_unrelated(p_dag);
  dag_dump(p_dag);
  printf("Unrelated: ");
  dag_dump_unrelated(p_dag);

  dag_path_t p;
  dag_path_init(&p);
  int len = dag_longest_path(p_dag, p_sys, s, e, &p);
  printf("longest path: %d [", len);
  dag_path_dump(&p);
  printf(" ]\n");

  dag_set_deadlines(p_dag, p_sys, s, e, p_dag->deadline, 1);
  dag_dump(p_dag);

  printf("\n");

  return p_dag;
}

/* Alexandre's example (formatted with graph-easy :-(, dot -Tpdf makes a much better job!)
  +-------------------------------------+
  |                                     |
  |  +----+     +----+     +----+     +----+     +----+     +----+
  |  | n3 | <-- | n0 | --> | n1 | --> |    | --> | n6 | --> | n9 | <+
  |  +----+     +----+     +----+     |    |     +----+     +----+  |
  |    |          |                   |    |                  ^     |
  |    |          |                   | n4 |                  |     |
  |    v          v                   |    |                  |     |
  |  +----+     +----+                |    |     +----+       |     |
  |  | n5 | <-- | n2 | -------------> |    | --> | n8 | ------+-----+
  |  +----+     +----+                +----+     +----+       |
  |    |                                                      |
  |    |                                                      |
  |    v                                                      |
  |  +----+                                                   |
  +> | n7 | --------------------------------------------------+
     +----+
*/
void ex2() {
  printf("ex2:\n");
  dag_t d;
  dag_init(&d, 100, 100);
  int n[10]; 
  for (int i=0; i < 10; i++){
    n[i] = dag_add_elem(&d, 10, 0, -1);
  }
  d.elems[n[0]].C = 0;
  d.elems[n[3]].C = 15;
  d.elems[n[9]].C = 0;
  dag_add_prev(&d, n[1], n[0]);
  dag_add_prev(&d, n[2], n[0]);
  dag_add_prev(&d, n[3], n[0]);
  dag_add_prev(&d, n[4], n[1]);
  dag_add_prev(&d, n[4], n[2]);
  dag_add_prev(&d, n[5], n[2]);
  dag_add_prev(&d, n[5], n[3]);
  dag_add_prev(&d, n[8], n[3]);
  dag_add_prev(&d, n[6], n[4]);
  dag_add_prev(&d, n[7], n[4]);
  dag_add_prev(&d, n[8], n[4]);
  dag_add_prev(&d, n[7], n[5]);
  dag_add_prev(&d, n[9], n[6]);
  dag_add_prev(&d, n[9], n[7]);
  dag_add_prev(&d, n[9], n[8]);

  dag_comp_unrelated(&d);
  dag_dump(&d);
  printf("Unrelated: ");
  dag_dump_unrelated(&d);
  // { 0, }, { 9, }, { 6, 7, 8, }, { 3, 6, }, { 5, 6, 8, }, { 1, 5, }, { 4, 5, }, { 1, 2, 3, }, { 3, 4, }, 

  dag_path_t p;
  dag_path_init(&p);
  int len = dag_longest_path(&d, p_sys, n[0], n[9], &p);
  printf("longest path: %d [", len);
  dag_path_dump(&p);
  printf(" ]\n");

  dag_set_deadlines(&d, p_sys, n[0], n[9], d.deadline, 1);
  dag_dump(&d);
  dag_dump_dot(&d, "ex2.dot");

  dag_cleanup(&d);
  printf("\n");
}

// raspberry pi 4, model B,  with a Broadcom BCM2711B0, which includes four Cortex-A72 CPUs
sys_t *build_raspberry_pi4b() {
  sys_t *p_sys = malloc(sizeof(sys_t));
  assert(p_sys != NULL);
  sys_init(p_sys);
  island_t *cpus = sys_add_island(p_sys, 1.0, 4, SYS_SCHED_EDF, CPU);
  // adopting MHz and W as standard units
  island_add_freq(cpus, 1500, 3.67352153779488,	3.110947);
  island_add_freq(cpus, 1400, 3.60407547355613,	3.084576);
  island_add_freq(cpus, 1300, 3.53263941668691,	3.057185);
  island_add_freq(cpus, 1200, 3.46428389764674,	3.03105516549649);
  island_add_freq(cpus, 1100, 3.39549793901795,	3.00343429286608);
  island_add_freq(cpus, 1000, 3.32966816187627,	2.97858358358358);
  island_add_freq(cpus, 900	, 3.26295850316887,	2.95137048192771);
  island_add_freq(cpus, 800	, 3.19891252784989,	2.92600250626566);
  island_add_freq(cpus, 700	, 3.13479404570291,	2.89901879699248);
  island_add_freq(cpus, 600	, 3.04526071906541,	2.855955);
  
  return p_sys;	
}


// ZCU102 board assuming the FPGA w a single slot with capacity 5x compared to the cpus
sys_t *build_zcu102_1slot() {
  sys_t *p_sys = malloc(sizeof(sys_t));
  assert(p_sys != NULL);
  sys_init(p_sys);
  island_t *cpus = sys_add_island(p_sys, 1.0, 4, SYS_SCHED_EDF, CPU);
  // adopting MHz and W as standard units
  island_add_freq(cpus, 1199.0, 1.8577986408087  , 1.636175);
  island_add_freq(cpus, 599.0 , 1.79887623923541 , 1.621317);
  island_add_freq(cpus, 399.0 , 1.77674763857868 , 1.618454);
  island_add_freq(cpus, 299.0 , 1.74844941100068 , 1.6114  );

  island_t *p_slot0 = sys_add_island(p_sys, 5.0, 1, SYS_SCHED_EDF, ACCELERATOR);
  island_add_freq(p_slot0, 100.00, 1.74844941100068, 1.6114);
  
  return p_sys;	
}

// ODROID XU3 (half of the available frequencies)
sys_t *build_biglittle_half(int n_big, int n_little, float cap_little) {
  sys_t *p_sys = malloc(sizeof(sys_t));
  assert(p_sys != NULL);
  sys_init(p_sys);
  island_t *p_big = sys_add_island(p_sys, 1.0, n_big, SYS_SCHED_EDF, CPU);
  // adopting MHz and W as standard units
  island_add_freq(p_big, 2000.00, 2.08265621783625	, 0.412970993421053);
  island_add_freq(p_big, 1800.00, 1.50253770423977	, 0.282202837719298);
  island_add_freq(p_big, 1600.00, 1.13424830635965	, 0.210462201754386);
  island_add_freq(p_big, 1400.00, 0.868902175	    , 0.161186936403509);
  island_add_freq(p_big, 1200.00, 0.667705702997076	, 0.12385000877193 );
  island_add_freq(p_big, 1000.00, 0.500931594005848	, 0.095939989035088);
  island_add_freq(p_big,  800.00, 0.360382067178363	, 0.070789282894737);
  island_add_freq(p_big,  600.00, 0.266906553801169	, 0.055386111842105);
  island_add_freq(p_big,  400.00, 0.18774763011696	, 0.04395965131579 );
  island_add_freq(p_big,  200.00, 0.10236486498538	, 0.029714405701754);

  island_t *p_lit = sys_add_island(p_sys, cap_little, n_little, SYS_SCHED_EDF, CPU);
  island_add_freq(p_lit, 1400.00, 0.281703834722222	,0.093324004385965);
  island_add_freq(p_lit, 1200.00, 0.201248805994152	,0.06539486622807 );
  island_add_freq(p_lit, 1000.00, 0.146737925657895	,0.047670885964912);
  island_add_freq(p_lit,  800.00, 0.102453253435672	,0.033128201754386);
  island_add_freq(p_lit,  600.00, 0.067276892690058	,0.021402447368421);
  island_add_freq(p_lit,  400.00, 0.042896272660819	,0.014739322368421);
  island_add_freq(p_lit,  200.00, 0.023366738377193	,0.009238804824561);
  
  return p_sys;	
}

// ODROID XU3 with all available frequencies
sys_t *build_biglittle(int n_big, int n_little, float cap_little) {
  sys_t *p_sys = malloc(sizeof(sys_t));
  assert(p_sys != NULL);
  sys_init(p_sys);
  island_t *p_big = sys_add_island(p_sys, 1.0, n_big, SYS_SCHED_EDF, CPU);
  // adopting MHz and W as standard units
  island_add_freq(p_big, 2000.00, 2.08265621783625	, 0.412970993421053);
  island_add_freq(p_big, 1900.00, 1.77142172266082	, 0.325980447368421);
  island_add_freq(p_big, 1800.00, 1.50253770423977	, 0.282202837719298);
  island_add_freq(p_big, 1700.00, 1.30678805233919	, 0.240915076754386);
  island_add_freq(p_big, 1600.00, 1.13424830635965	, 0.210462201754386);
  island_add_freq(p_big, 1500.00, 0.978278668055556	, 0.18216388377193 );
  island_add_freq(p_big, 1400.00, 0.868902175	    , 0.161186936403509);
  island_add_freq(p_big, 1300.00, 0.766021783625731	, 0.143356778508772);
  island_add_freq(p_big, 1200.00, 0.667705702997076	, 0.12385000877193 );
  island_add_freq(p_big, 1100.00, 0.582060522953216	, 0.110361934210526);
  island_add_freq(p_big, 1000.00, 0.500931594005848	, 0.095939989035088);
  island_add_freq(p_big,  900.00, 0.426750057236842	, 0.082052885964912);
  island_add_freq(p_big,  800.00, 0.360382067178363	, 0.070789282894737);
  island_add_freq(p_big,  700.00, 0.308482990862573	, 0.062486721491228);
  island_add_freq(p_big,  600.00, 0.266906553801169	, 0.055386111842105);
  island_add_freq(p_big,  500.00, 0.227691598099415	, 0.049942203947368);
  island_add_freq(p_big,  400.00, 0.18774763011696	, 0.04395965131579 );
  island_add_freq(p_big,  300.00, 0.145339782821638	, 0.036667206140351);
  island_add_freq(p_big,  200.00, 0.10236486498538	, 0.029714405701754);

  island_t *p_lit = sys_add_island(p_sys, cap_little, n_little, SYS_SCHED_EDF, CPU);
  island_add_freq(p_lit, 1500.00, 0.322891321710526	,0.109331984649123);
  island_add_freq(p_lit, 1400.00, 0.281703834722222	,0.093324004385965);
  island_add_freq(p_lit, 1300.00, 0.239441758991228	,0.079200675438597);
  island_add_freq(p_lit, 1200.00, 0.201248805994152	,0.06539486622807 );
  island_add_freq(p_lit, 1100.00, 0.172303840789473	,0.05617760745614 );
  island_add_freq(p_lit, 1000.00, 0.146737925657895	,0.047670885964912);
  island_add_freq(p_lit,  900.00, 0.122801795321637	,0.039642149122807);
  island_add_freq(p_lit,  800.00, 0.102453253435672	,0.033128201754386);
  island_add_freq(p_lit,  700.00, 0.084834951169591	,0.027034129385965);
  island_add_freq(p_lit,  600.00, 0.067276892690058	,0.021402447368421);
  island_add_freq(p_lit,  500.00, 0.052146664035088	,0.017003475877193);
  island_add_freq(p_lit,  400.00, 0.042896272660819	,0.014739322368421);
  island_add_freq(p_lit,  300.00, 0.032978425950292	,0.011662326754386);
  island_add_freq(p_lit,  200.00, 0.023366738377193	,0.009238804824561);
  
  return p_sys;
}

sys_t *build_dynamiq(int n_big, int n_middle, float cap_middle, int n_little, float cap_little) {
  sys_add_island(p_sys, 1.0, n_big, SYS_SCHED_EDF, CPU);
  sys_add_island(p_sys, cap_middle, n_middle, SYS_SCHED_EDF, CPU);
  sys_add_island(p_sys, cap_little, n_little, SYS_SCHED_EDF, CPU);
  return p_sys;
}

int parse_dag(int *argc, char **argv[]) {
  unsigned ret_code;
  (void) ret_code; // this varaible can be used when running in Release mode or undefine NDEBUG. This hack makes sure that wont be a unused variable warning
  if (strcmp(**argv, "-d") == 0) {
    float deadline, period;
    assert(*argc >= 2);
    (*argc)--;  (*argv)++;
    ret_code = sscanf(**argv, "%f,%f", &deadline, &period);
    assert(ret_code == 2);
    dag_t *p_dag = malloc(sizeof(dag_t));
    assert(p_dag != NULL);
    dag_init(p_dag, deadline, period);
    sys_add_dag(p_sys, p_dag);
    LOG(INFO,"Added new DAG with deadline %f, period %f\n", deadline, period);
  } else if (strcmp(**argv, "-t") == 0) {
    float wcet, wcet_ns;
    int e; 
    (void) e; // this variable can be unused depending on the LOG_LEVEL
    assert(*argc >= 2);
    (*argc)--;  (*argv)++;
    assert(p_sys->n_dags > 0);
    dag_t *p_dag = p_sys->dags[p_sys->n_dags - 1];
    if (sscanf(**argv, "%f,%f", &wcet, &wcet_ns) == 2)
      e = dag_add_elem(p_dag, wcet, wcet_ns, -1);
    else if (sscanf(**argv, "%f", &wcet) == 1)
      e = dag_add_elem(p_dag, wcet, 0, -1);
    else {
      fprintf(stderr, "Error: wrong format for -t argument (expecting wcet or wcet,wcet_ns)\n");
      exit(1);
    }
    LOG(INFO,"Added elem %d with WCET %f and WCET_ns %f\n", e, wcet, wcet_ns);
  } else if (strcmp(**argv, "-a") == 0) {
  	assert(*argc >= 1);
  	(*argc)--;  (*argv)++;
  	int acc_id;
  	ret_code = sscanf(**argv, "%d", &acc_id);
  	assert(ret_code == 1);
  	assert(p_sys->n_islands > acc_id);
  	dag_t *p_dag = p_sys->dags[p_sys->n_dags - 1];
  	dag_elem_t *p_elem = &(p_dag->elems[p_dag->num_elems - 1]);
  	p_elem->accelerators = (p_elem->accelerators | (1<<acc_id));
  	LOG(INFO,"Added implementation of task %d in dag %d for the accelerated island %d\n", p_dag->num_elems - 1, p_sys->n_dags - 1, acc_id);
  } else if (strcmp(**argv, "-p") == 0) {
    int e, p;
    assert(*argc >= 2);
    (*argc)--;  (*argv)++;
    ret_code = sscanf(**argv, "%d,%d", &e, &p);
    assert(ret_code == 2);
    dag_t *p_dag = p_sys->dags[p_sys->n_dags - 1];
    dag_add_prev(p_dag, e, p);
    LOG(INFO,"Added for elem %d prev %d\n", e, p);
  } else if (strcmp(**argv, "-aaf") == 0) {
    int t1, t2;
    assert(*argc >= 2);
    (*argc)--;  (*argv)++;
    ret_code = sscanf(**argv, "%d,%d", &t1, &t2);
    assert(ret_code == 2);
    dag_t *p_dag = p_sys->dags[p_sys->n_dags - 1];
    // the call on t2,t1 shouldn't be needed
    dag_set_anti_affinity(p_dag, t1, t2);
    LOG(INFO,"Elem %d and elem %d in dag %d won't be mapped to the same PU\n", t1, t2, p_sys->n_dags - 1);
  } else if (strcmp(**argv, "-n") == 0) {
    int e, n;
    assert(*argc >= 2);
    (*argc)--;  (*argv)++;
    ret_code = sscanf(**argv, "%d,%d", &e, &n);
    assert(ret_code == 2);
    dag_t *p_dag = p_sys->dags[p_sys->n_dags - 1];
    dag_add_next(p_dag, e, n);
    LOG(INFO,"Added for elem %d next %d\n", e, n);
  } else {
    return 0;
  }
  return 1;
}


int parse_sys(int *argc, char **argv[]) {
  unsigned ret_code;
  (void) ret_code;
  if (strcmp(**argv, "-s") == 0) {
    assert(*argc >= 2);
    (*argc)--;  (*argv)++;
    platform_name = **argv;
    if (strcmp(**argv, "odroid-xu3") == 0) {
      p_sys = build_biglittle(4, 4, 0.526);
      LOG(INFO,"Added new odroid-xu3 system\n");
    } else if (strcmp(**argv, "odroid-xu3-half") == 0) {
      p_sys = build_biglittle_half(4, 4, 0.526);
      LOG(INFO,"Added new odroid-xu3-half system\n");
    } else if (strcmp(**argv, "zcu102-1slot") == 0) {
      p_sys = build_zcu102_1slot();
      LOG(INFO,"Added new zcu102-1slot system\n");
    } else if (strcmp(**argv, "raspberry-pi4b") == 0) {
      p_sys = build_raspberry_pi4b();
      LOG(INFO,"Added new raspberry-pi4b system\n");
    } else {
      fprintf(stderr, "Unknown system %s\n", **argv);
      exit(1);
    }
  } else if (strcmp(**argv, "-i") == 0) {
    int n_cores;
    float capacity;
    assert(*argc >= 2);
    (*argc)--;  (*argv)++;
    ret_code = sscanf(**argv, "%d,%f", &n_cores, &capacity);
    assert(ret_code == 2);
    assert(p_sys != NULL);
    island_t *p_island = sys_add_island(p_sys, capacity, n_cores, SYS_SCHED_EDF, CPU);
    assert(p_island != NULL);
    LOG(INFO,"Added new island with n_cores %d, capacity %f\n", n_cores, capacity);
  } else if (strcmp(**argv, "-ai") == 0) {
    int n_cores;
    float capacity;
    assert(*argc >= 2);
    (*argc)--;  (*argv)++;
    if (sscanf(**argv, "%d,%f", &n_cores, &capacity) != 2) {
      fprintf(stderr, "Wrong syntax!\n");
      exit(1);
    }
    assert(p_sys != NULL);
    island_t *p_island = sys_add_island(p_sys, capacity, n_cores, SYS_SCHED_EDF, ACCELERATOR);
    assert(p_island != NULL);
    LOG(INFO,"Added new accelerated island with n_cores %d, capacity %f\n", n_cores, capacity);
  } else if (strcmp(**argv, "-f") == 0) {
    float freq, pow_busy, pow_idle;
    assert(*argc >= 2);
    (*argc)--;  (*argv)++;
    ret_code = sscanf(**argv, "%f,%f,%f", &freq, &pow_busy, &pow_idle);
    assert(ret_code == 3);
    island_add_freq(&p_sys->islands[p_sys->n_islands - 1], freq, pow_busy, pow_idle);
    LOG(INFO,"Added freq with freq %f, pow_busy %f, pow_idle %f\n", freq, pow_busy, pow_idle);
  } else {
    return 0;
  }
  return 1;
}

unsigned long micros(){
    struct timeval ts;
    gettimeofday(&ts,NULL);
    unsigned long us = 1000000 * ts.tv_sec + ts.tv_usec;
    return us;
}

int main(int argc, char *argv[]) {
  int call_solver_gurobi = 0;
  int call_solver_heur = 0;
  float power_budget = 0.0;
  unsigned ret_code;
  (void) ret_code;

  // this is set to make sure that the gurobi pid is killed when this process is killed.
  // this is a requiment to deal with timeout termination of dag
  printf("setsid() returned: %d\n", setsid());

  p_sys = malloc(sizeof(sys_t));
  assert(p_sys != NULL);
  sys_init(p_sys);

  // print the executed command
  printf("$> ");
  for (int i=0;i<argc;i++){
      printf("%s ", argv[i]);
  }
  printf("\n");

  argc--;  argv++;
  while (argc > 0) {
    if (strcmp(*argv, "-h") == 0 || strcmp(*argv, "--help") == 0) {
      printf("Usage: dag [options...]\n");
      printf("  Options:\n");
      printf("    -h|--help .................. Show this help message and exit\n");
      printf("    -s system .................. Use a named hardware platform (odroid-xu3), instead of adding islands/cores through -i and -f options\n");
      printf("    -i n_cores[,capacity] ...... Add an island with the specified number of cores and capacity (defaults to 1.0)\n");
      printf("    -ai n_cores,capacity.. ..... Add an accelerated island with the specified number of cores and capacity (tasks need to be explicitly mapped using -a)\n");
      printf("    -f freq,pow_busy,pow_idle .. Add an OPP to the current island (added last with -i), with the specified frequency and power values\n");
      printf("    -d deadline,period ......... Add an empty DAG with the specified end-to-end deadline and minimum inter-arrival period\n");
      printf("    -t WCET[,WCET_ns] .......... Add a task to the current DAG (added last with -d), with the specified WCET and non-scalable WCET (defaults to 0)\n");
      printf("    -a acc_island_id ........... Let the current task (added last with -t) run also on any accelerator of the island identified by the provided ID (can be used multiple times)\n");
      printf("    -p elem,prev ............... Add a precedence constraint from task prev to task elem (both must have already been added with -t)\n");
      printf("    -n elem,next ............... Add a precedence constraint from task elem to task next (both must have already been added with -t)\n");
      printf("    -aaf task_id_1,task_id_2 ... Specify that task_id_1 and task_id_2 of the current DAG (added last with -d) cannot be mapped to the same PU (anti-affinity constraint)\n");
      printf("    -olp filename.lp ........... Specify destination filename for the LP optimization problem\n");
      printf("    -lps|--lp-scale scale ...... Specify scaling factor for LP times (defaults to auto)\n");
      printf("    -u|--max-util bw ........... Specify the maximum per-core utilization (defaults to 0.95)\n");
      printf("    -r k,D,n,C,e[,seed] ........ Add k random DAGs to the model, with deadline up to D, up to n nodes, up to a WCET of C, up to e edges\n");
      printf("    --solve[=method] ........... Solve the generated LP problem using method, either gurobi (default) or heur\n");
      printf("    --create-bounds ............ Create explicit bounds in the LP formulation for the values non-binary variables can take (GUROBI ONLY)\n");
      printf("    -pb power_budget ........... Set a power budget so that the LP formulation maximizes the minimum slack within the power constraint\n");
      printf("    -osol filename.sol ......... Specify destination filename for the solution of the LP problem (requires --solve)\n");
      printf("    -osols pathname ............ Specify pathname/prefix for intermediate solutions (requires --solve)\n");
      printf("    -odot filename.dot ......... Specify destination filename for the DOT file containing the finally optimized system (requires --solve)\n");
      printf("    -oyml filename.yml ......... Dump current DAGs into the specified YAML file\n");
      printf("    -osh filename.sh ........... Dump current system (islands and DAGs) into the specified SH file\n");
      printf("    -oyml_rtsim_dags fname.yml . Dump placed DAGs into the specified YAML file for RTSim\n");
      printf("    -oyml_rtsim_isl fname.yml .. Dump optimized islands into the specified YAML file for RTSim\n");
      printf("    -oyaml filename.yaml ....... Specify destination filename for the the optimization output\n");
      printf("    -oyaml filename.yaml ....... Specify destination filename for the optimization output\n");
      printf("    -olog filename.log ......... Specify destination filename for the gurobi log\n");
      printf("    -max-threads ............... Set Gurobi to use the max number of threads. Otherwise it uses the number of threads defined in MAX_GUROBI_THREADS\n");
      printf("    -ip power .................. Specify the upper bound for power\n");
      exit(0);
    } else if (strcmp(*argv, "-olp") == 0) {
      argc--;  argv++;
      assert(argc > 0);
      out_lp_fname = *argv;
    } else if (strcmp(*argv, "-osol") == 0) {
      argc--;  argv++;
      assert(argc > 0);
      out_sol_fname = *argv;
    } else if (strcmp(*argv, "-osols") == 0) {
      argc--;  argv++;
      assert(argc > 0);
      out_sols_pname = *argv;
    } else if (strcmp(*argv, "-odot") == 0) {
      argc--;  argv++;
      assert(argc > 0);
      out_dot_fname = *argv;
    } else if (strcmp(*argv, "-oyml") == 0) {
      argc--;  argv++;
      assert(argc > 0);
      out_yml_fname = *argv;
    } else if (strcmp(*argv, "-oyml_rtsim_dags") == 0) {
      argc--;  argv++;
      assert(argc > 0);
      out_yml_rtsim_dags_fname = *argv;
    } else if (strcmp(*argv, "-oyml_rtsim_isl") == 0) {
      argc--;  argv++;
      assert(argc > 0);
      out_yml_rtsim_isl_fname = *argv;
    } else if (strcmp(*argv, "-osh") == 0) {
      argc--;  argv++;
      assert(argc > 0);
      out_sh_fname = *argv;
    } else if (strcmp(*argv, "-oyaml") == 0) {
      argc--;  argv++;
      assert(argc > 0);
      out_yaml_fname = *argv;
    } else if (strcmp(*argv, "-olog") == 0) {
      argc--;  argv++;
      assert(argc > 0);
      gurobi_log_fname = *argv;
    } else if (strcmp(*argv, "-max-threads") == 0) {
      gurobi_max_threads = 0;
    } else if (strcmp(*argv, "-ip") == 0) {
      argc--;  argv++;
      assert(argc > 0);
      int ret = sscanf(*argv,"%f", &initial_power);
      assert (ret == 1);
    } else if (strcmp(*argv, "-lps") == 0 || strcmp(*argv, "--lp-scale") == 0) {
      argc--;  argv++;
      assert(argc > 0);
      int ret = sscanf(*argv, "%f", &lp_scale);
      assert (ret == 1);
    } else if (strcmp(*argv, "-u") == 0 || strcmp(*argv, "--max-util") == 0) {
      argc--;  argv++;
      assert(argc > 0);
      int ret = sscanf(*argv, "%f", &max_util);
      assert (ret == 1);
    } else if (strcmp(*argv, "-r") == 0) {
      argc--;  argv++;
      assert(argc > 0);
      int seed;
      int n_dags, max_period, max_elems, wcet_max, max_edges;
      if (sscanf(*argv,"%d,%d,%d,%d,%d,%d", &n_dags, &max_period, &max_elems, &wcet_max, &max_edges, &seed) == 6)
        srand(seed);
      else if (sscanf(*argv,"%d,%d,%d,%d,%d", &n_dags, &max_period, &max_elems, &wcet_max, &max_edges) == 5)
        srand(time(NULL));
      else{
        fprintf(stderr, "Error: wrong format for -r argument (expecting 'n_dags,max_period,max_elems,wcet_max,max_edges,seed' or 'n_dags,max_period,max_elems,wcet_max,max_edges')\n");
        exit(1);
      }
      /* the adopted approach is to create a set of individual dot files (each dot file represents a single dag) and latter combine 
      1 or more dags into a 'dag set', both in yaml and .sh formats.
      This approach works best when considering both random dag generators because they will have a uniform
      approach to generate the dags.
      */
      assert(max_elems >= 3 && max_elems <= MAX_TASKS_PER_DAG);
      char *fname = strdup("rnd00.dot");
      int i=0;
      while(i < n_dags){
        dag_t *p_dag = build_dag_random(max_period, max_elems, wcet_max, max_edges);
        if (p_dag == NULL){
            continue;
        }
        // make sure that the critical path is < the deadline. Otherwise, discard the dag
        dag_path_t path;
        dag_path_init(&path);
        float C_sum = dag_longest_path(p_dag, p_sys, p_dag->first, p_dag->last, &path);
        if (((int)C_sum) > p_dag->deadline){
            LOG(DEBUG,"\n\nignoring dag with path=%f and deadline=%f\n\n", C_sum, p_dag->deadline);
            continue;
        }
        // now that we know that the dag is valid, print its spec
        #if LOG_LEVEL >= 3
            dag_dump(p_dag);
        #endif
        printf("deadline %f, period %f, n_elems %d\n", p_dag->deadline, p_dag->period, p_dag->num_elems);
        printf (" - critial path (C_sum=%f): [ ", C_sum);
        dag_path_dump(&path);
        printf(" ]\n");
        // TODO: is there any other check to make sure that the genreated dag is a valid one ?
        // compute this just to make sure that this is a valid dag
        // dag_comp_unrelated(p_dag);
        snprintf(fname, 10, "rnd%02d.dot", i);
        printf (" - filename: %s\n", fname);
        dag_dump_dot2(p_dag, C_sum, fname, &path);
        i++;
      }
      exit (0);
    } else if (strcmp(*argv, "--solve") == 0 || strcmp(*argv, "--solve=gurobi") == 0) {
      call_solver_gurobi = 1;
    } else if (strcmp(*argv, "--solve=heur") == 0) {
      call_solver_heur = 1;
    } else if (strcmp(*argv, "--create-bounds") == 0) {
      create_non_binary_bounds = 1;
    } else if (strcmp(*argv, "-pb") == 0) {
      argc--;  argv++;
      assert(argc > 0);
      int ret = sscanf(*argv,"%f", &power_budget);
      assert (ret == 1);
    } else if (!parse_dag(&argc, &argv) && !parse_sys(&argc, &argv)) {
      fprintf(stderr, "Unknown command-line option: %s\n", *argv);
      exit(1);
    }
    argc--;  argv++;
  }

  unsigned long start = micros();

  float max_period = 0;
  for (int d = 0; d < p_sys->n_dags; d++) {
    dag_t *p_dag = p_sys->dags[d];
    dag_update_first_last(p_dag);
    dag_comp_unrelated(p_dag);
    if (p_dag->period > max_period)
      max_period = p_dag->period;
  }

  if (lp_scale == 0.0) {
    lp_scale = 1.0;
    while (max_period / lp_scale > 1000)
      lp_scale *= 1000;
  }

  printf("lp_scale=%g, max_period=%g\n", lp_scale, max_period);

  sys_dump(p_sys);

  if (out_lp_fname != NULL)
    dump_lp_sys(p_sys, out_lp_fname, power_budget);

  if (out_yml_fname != NULL)
    sys_dump_dags_yaml(p_sys, out_yml_fname);

  if (out_sh_fname != NULL)
    sys_dump_args(p_sys, out_sh_fname);

  assert(!call_solver_gurobi || (out_lp_fname != NULL && out_sol_fname != NULL));

  printf("\n");

  int valid_solution=0; // 0 is invalid
  if (call_solver_gurobi) {
    // check if there is a island with capacity 1.0
    int has_capacity_1 = 0;
    for (int i = 0; i < p_sys->n_islands; i++) {
        if (p_sys->islands[i].capacity == 1.0) 
            has_capacity_1 = 1;
    }
    if (has_capacity_1 == 0){
      fprintf(stderr, "ERROR: make sure that the platform definition has one island with capacity 1.0\n");
      exit(1);
    }
    valid_solution = solve_gurobi(p_sys);
  }

  if (call_solver_heur) {
    sys_clear_deadlines(p_sys);
    sys_dump(p_sys);
    valid_solution = solve_heur(p_sys);
    sys_dump(p_sys);
    sys_set_deadlines(p_sys);
  }

  printf("\n");

  if (out_yml_rtsim_dags_fname != NULL)
    sys_dump_dags_yaml_rtsim(p_sys, out_yml_rtsim_dags_fname);

  if (out_yml_rtsim_isl_fname != NULL)
    sys_dump_isl_yaml_rtsim(p_sys, out_yml_rtsim_isl_fname);

  if (out_dot_fname != NULL)
    sys_dump_dot_placed(p_sys, out_dot_fname);

  sys_dump(p_sys);

  if (call_solver_gurobi) {
    // compact output format used to ease comparison with other optimization tools
    sys_dump_bbsearch(p_sys, out_sol_fname);

    if (valid_solution) {
        unsigned long end = micros();
        unsigned long duration = end - start;
        // keep it microseconds to ease the comparison among tools
        printf ("\n\nExecution time: %ld microseconds\n\n", duration);

        if (out_yaml_fname != NULL){
            // get the dag_name from the lp filename
            char * dag_name = strdup(out_lp_fname);
            // remove the extension '.lp'
            dag_name[strlen(dag_name)-3] = 0;
            sys_dump_output_yaml(p_sys, out_sol_fname, out_yaml_fname, dag_name, platform_name, duration);
        }
    }
  }
  if (valid_solution){
     printf ("Valid solution found !\n\n");
  }else{
     printf ("Unable to find a valid solution !\n\n");
  }
  sys_cleanup(p_sys);
  // return 0 if the solution is valid
  return valid_solution==1 ? 0 : 1;
}
