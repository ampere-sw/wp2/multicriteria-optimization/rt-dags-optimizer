#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <math.h>
#include <float.h>

#include "dag.h"
#include "log.h"

#define FEQUAL(N1,N2) ((fabs(N1-N2)<0.000001f)?(1):(0))

const char *colors[] = { "darkred", "darkgreen", "darkblue", "darkorange", "darkviolet", "darkbrown" };

void dag_init(dag_t *p_dag, float deadline, float period) {
  assert(period >= deadline);
  p_dag->first = p_dag->last = -1;
  p_dag->num_elems = 0;
  p_dag->relatives = NULL;
  p_dag->unrelated = NULL;
  p_dag->unrelated_size = 0;
  p_dag->deadline = deadline;
  p_dag->period = period;
}

void dag_cleanup(dag_t *p_dag) {
  if (p_dag->unrelated != NULL)
    free(p_dag->unrelated);
  p_dag->unrelated = 0;
  for (int i = 0; i < p_dag->num_elems; i++) {
      if ((p_dag->relatives[i] != NULL)){
        free(p_dag->relatives[i]);
      }
  }
  free(p_dag->relatives);
  free(p_dag);
  // p_dag->relatives = 0;
}

int dag_num_elems(dag_t *p_dag) {
  return p_dag->num_elems;
}

int dag_add_elem(dag_t *p_dag, float C, float C_ns, float d) {
  assert(p_dag->num_elems < MAX_TASKS_PER_DAG);
  assert(C > 0);
  assert(C_ns >= 0);
  int n = p_dag->num_elems++;
  if (n == 0)
    p_dag->first = n;
  p_dag->last = n;
  p_dag->elems[n].C = C;
  p_dag->elems[n].C_ns = C_ns;
  p_dag->elems[n].d = d;
  p_dag->elems[n].opt_island = -1;
  p_dag->elems[n].opt_punit = -1;
  p_dag->elems[n].n_prev = 0;
  p_dag->elems[n].n_next = 0;
  p_dag->elems[n].n_anti_affinity = 0;
  p_dag->elems[n].accelerators = 0;
  return n;
}

// TODO: for amalthea compatibility
// void dag_add_prev(dag_t *p_dag, int n, int prev, int size) {
void dag_add_prev(dag_t *p_dag, int n, int prev) {
  assert(prev >= 0 && prev < p_dag->num_elems);
  assert(n > 0 && n < p_dag->num_elems);
  dag_elem_t *p_elem = &p_dag->elems[n];
  foreach_elem_prev(p_dag, n)
    if (p == prev)
      return;
  assert(p_elem->n_prev < MAX_PREV);
  p_elem->prev[p_elem->n_prev++] = prev;
  p_elem = &p_dag->elems[prev];
  assert(p_elem->n_next < MAX_NEXT);
  p_elem->next[p_elem->n_next++] = n;
}

void dag_set_anti_affinity(dag_t *p_dag, int t1, int t2) {
  assert(t2 >= 0 && t2 < p_dag->num_elems);
  dag_elem_t *p_elem_t1 = &p_dag->elems[t1];
  for (int i = 0; i < p_elem_t1->n_anti_affinity; ++i) {
    if (p_elem_t1->anti_affinity[i] == t2) {
      return;
    }
  }

  assert(p_elem_t1->n_anti_affinity < MAX_TASKS_PER_DAG);
  p_elem_t1->anti_affinity[p_elem_t1->n_anti_affinity++] = t2;
}

//TODO: add msg size
void dag_add_next(dag_t *p_dag, int n, int next) {
  dag_add_prev(p_dag, next, n);
}

void dag_update_first_last(dag_t *p_dag) {
  p_dag->first = p_dag->last = -1;
  for (int i = 0; i < p_dag->num_elems; i++) {
    if (p_dag->elems[i].n_prev == 0) {
      assert(p_dag->first == -1);
      p_dag->first = i;
    } else if (p_dag->elems[i].n_next == 0) {
      assert(p_dag->last == -1);
      p_dag->last = i;
    }
  }
  assert(p_dag->first != -1 && p_dag->last != -1);
}

// Check if b is among a's next elems
int dag_is_successor(dag_t *p_dag, int a, int b) {
  foreach_elem_next(p_dag, a)
    if (n == b)
        return 1;
  return 0;
}

// Check if b reachable from a
int dag_is_reachable(dag_t *p_dag, int a, int b) {
  foreach_elem_next(p_dag, a)
    if (n == b)
        return 1;
  foreach_elem_next(p_dag, a)
    if (dag_is_reachable(p_dag, n, b))
        return 1;
  return 0;
}

void dag_comp_unrelated(dag_t *p_dag) {
  p_dag->relatives = malloc(sizeof(int*) * p_dag->num_elems);
  assert(p_dag->relatives != NULL);
  for (int i = 0; i < p_dag->num_elems; i++) {
    p_dag->relatives[i] = malloc(sizeof(int) * p_dag->num_elems);
    assert(p_dag->relatives[i] != NULL);
  }
  for (int a = 0; a < p_dag->num_elems-1; a++)
    for (int b = a+1; b < p_dag->num_elems; b++)
      if (dag_is_reachable(p_dag, a, b) || dag_is_reachable(p_dag, b, a))
        p_dag->relatives[a][b] = 1;
      else
        p_dag->relatives[a][b] = 0;
  // TODO: (Amory) is this correct ?!?! if you have MAX_ELEMS elements you allocated 4* 2^MAX_ELEMS bytes ?!?
  // it seeems that the correct would be:
  // p_dag->unrelated = malloc(sizeof(int)*(p_dag->num_elems));
  p_dag->unrelated = malloc(sizeof(int)*(1 << p_dag->num_elems));
  p_dag->unrelated_size = 0;
  // loop over all non-empty subsets of elems
  for (int s = 1; s < (1 << p_dag->num_elems); s++) {
    int found_relative = 0;
    for (int a = 0; a < p_dag->num_elems-1 && !found_relative; a++) {
      for (int b = a + 1; b < p_dag->num_elems && !found_relative; b++) {
        if ((s & (1<<a)) && (s & (1<<b)) && p_dag->relatives[a][b]) {
          found_relative = 1;
          break;
        }
      }
    }
    if (!found_relative)
      p_dag->unrelated[p_dag->unrelated_size++] = s;
  }
  for (int s1 = 0; s1 < p_dag->unrelated_size; s1++) {
    for (int s2 = s1+1; s2 < p_dag->unrelated_size; s2++) {
      if ((p_dag->unrelated[s1] & p_dag->unrelated[s2]) == p_dag->unrelated[s2]) {
        // s2 subset of s1 => delete s2
        p_dag->unrelated[s2] = p_dag->unrelated[--p_dag->unrelated_size];
        // repeat loop body with new replaced s2
        s2--;
      } else if ((p_dag->unrelated[s1] & p_dag->unrelated[s2]) == p_dag->unrelated[s1]) {
        // s1 subset of s2 => delete s1
        p_dag->unrelated[s1] = p_dag->unrelated[--p_dag->unrelated_size];
        // abort inner loop, and repeat outer loop body with new replaced s1
        s1--;
        break;
      }
    }
  }
}

void dag_dump_unrelated(dag_t *p_dag) {
  for (int i = 0; i < p_dag->unrelated_size; i++) {
    printf("{ ");
    int k = 0;
    for (int j = 0; j < p_dag->num_elems; j++)
      if (p_dag->unrelated[i] & (1<<j))
        printf("%s%d", k++ > 0 ? ", " : "", j);
    printf(" }%s", i < p_dag->unrelated_size - 1 ? ", " : "");
  }
  printf("\n");
}

float dag_elem_wcet_scaled(dag_t *p_dag, int i, island_t *p_isl, int m) {
  assert(m >= 0 && m < p_isl->n_freqs);
  return p_dag->elems[i].C_ns + (p_dag->elems[i].C - p_dag->elems[i].C_ns) * p_isl->freqs[0] / p_isl->freqs[m] / p_isl->capacity;
}

// min WCET among configs, realized at max frequency, i.e., first frequency in island
float dag_elem_wcet_scaled_min(sys_t *p_sys, dag_t *p_dag, int i) {
  float min_wcet = FLT_MAX;
  for (int s = 0; s < p_sys->n_islands; s++) {
    island_t *p_isl = &p_sys->islands[s];
    float wcet = dag_elem_wcet_scaled(p_dag, i, p_isl, 0);
    if (wcet < min_wcet)
      min_wcet = wcet;
  }
  return min_wcet;
}

// max WCET among configs, realized at min frequency, i.e., last frequency in island
float dag_elem_wcet_scaled_max(sys_t *p_sys, dag_t *p_dag, int i) {
  float max_wcet = 0;
  for (int s = 0; s < p_sys->n_islands; s++) {
    island_t *p_isl = &p_sys->islands[s];
    float wcet = dag_elem_wcet_scaled(p_dag, i, p_isl, p_isl->n_freqs - 1);
    if (wcet > max_wcet)
      max_wcet = wcet;
  }
  return max_wcet;
}

void dag_dump(dag_t *p_dag) {
  printf("DAG deadline: %f, period: %f\n", p_dag->deadline, p_dag->period);
  for (int e = 0; e < p_dag->num_elems; e++) {
    dag_elem_t *p_elem = &p_dag->elems[e];
    printf("elem %d: (%f, %f) / %f", e, p_elem->C, p_elem->C_ns, p_elem->d);
    // dont print hw platform data into a function for DAGs
    /*
    if (p_elem->opt_island != -1) {
      island_t *p_isl = &p_sys->islands[p_elem->opt_island];
      printf(" [%.2f] -> (%d,%d)",
             dag_elem_wcet_scaled(p_dag, e, p_isl, p_isl->opt_freq) / p_elem->d,
             p_elem->opt_island, p_elem->opt_punit);
    }
    */
    printf(", prev(");
    int k = 0;
    foreach_elem_prev(p_dag, e)
      printf("%s%d", k++ > 0 ? ", " : "", p);
    printf("), next(");
    k = 0;
    foreach_elem_next(p_dag, e)
      printf("%s%d", k++ > 0 ? ", " : "", n);
    printf(")\n");
  }

  printf("\nSuccessors:\n");
  printf("   ");
  for (int b = 0; b < dag_num_elems(p_dag); b++)
    printf("%2d ", b);
  printf("\n");
  for (int a = 0; a < dag_num_elems(p_dag); a++) {
    printf("%2d ", a);
    for (int b = 0; b < dag_num_elems(p_dag); b++) {
      if (dag_is_successor(p_dag, a, b))
        printf(" 1 ");
      else
        printf(" 0 ");
    }
    printf("\n");
  }

  printf("\nReachability:\n");
  printf("   ");
  for (int b = 0; b < dag_num_elems(p_dag); b++)
    printf("%2d ", b);
  printf("\n");
  for (int a = 0; a < dag_num_elems(p_dag); a++) {
    printf("%2d ", a);
    for (int b = 0; b < dag_num_elems(p_dag); b++) {
      if (dag_is_reachable(p_dag, a, b) || dag_is_reachable(p_dag, b, a))
        printf(" 1 ");
      else
        printf(" 0 ");
    }
    printf("\n");
  }
}

void dag_dump_dot(dag_t *p_dag, char *fname) {
  FILE *f = fopen(fname, "w");
  assert(f != NULL);
  fprintf(f, "digraph G {\n  rankdir=LR;\n");
  for (int e = 0; e < p_dag->num_elems; e++) {
    fprintf(f, "  n%d [ label=\"n%d (%f,%f) / %f -> (%d,%d)\" ];\n", e, e,
            p_dag->elems[e].C, p_dag->elems[e].C_ns, p_dag->elems[e].d,
            p_dag->elems[e].opt_island, p_dag->elems[e].opt_punit);
    foreach_elem_next(p_dag, e)
      //TODO: use dag_longest_path to get the dag critical path and mark the dot edge with 'color="red"' attribute
      fprintf(f, "  n%d -> n%d\n", e, n);
  }
  fprintf(f, "}\n");
  fclose(f);
}

void dag_dump_dot2(dag_t *p_dag, float C_sum, char *fname, dag_path_t *p_path) {
  FILE *f = fopen(fname, "w");
  assert(f != NULL);
  // fprintf(f, "digraph G {\n  rankdir=LR;\n");
  fprintf(f, "digraph G {\n");
  fprintf(f, "  graph [ critical_path_length=%d, deadline=%f, period=%f]\n", (unsigned)C_sum, p_dag->deadline, p_dag->period);
  for (int e = 0; e < p_dag->num_elems; e++) {
    fprintf(f, "  %d [ label=%d, C=%f, C_ns=%f ];\n", e, e,
            p_dag->elems[e].C, p_dag->elems[e].C_ns);
  }
  int critical_idx = 0;
  for (int e = 0; e < p_dag->num_elems; e++) {
    foreach_elem_next(p_dag, e){
      //TODO: when communication is mapped into this dot file, write down the # of bytes as 'label' attribute
      if (critical_idx <= p_path->n_elems-2 && // if it is not the end of the critical path
        p_path->elems[critical_idx] == e &&    // if the source node match w the one in the critical path
        n == p_path->elems[critical_idx+1]){   // if the target node match w the one in the critical path
        //fprintf(f, "  %d -> %d [color=black, label=%d];\n", e, n, bytes);
        //printf ("%d, %d, %d, %d, %d, %d, %d\n", p_path->n_elems, p_dag->num_elems,  critical_idx, p_path->elems[critical_idx], p_path->elems[critical_idx+1], e, n );
        fprintf(f, "  %d -> %d [color=red];\n", e, n);
        critical_idx ++;
      }else{
        fprintf(f, "  %d -> %d [color=black];\n", e, n);
      }

    }
  }
  fprintf(f, "}\n");
  fclose(f);
}

void sys_dump_dags_yaml(sys_t *p_sys, char *fname) {
  FILE *f = fopen(fname, "w");
  assert(f != NULL);
  dag_t *p_dag;
  fprintf(f, "dags:\n");
  for (int i = 0; i < p_sys->n_dags; i++) {
    p_dag = p_sys->dags[i];
    fprintf(f, "  - activation_period: %f\n", p_dag->period);
    fprintf(f, "    deadline: %f\n",p_dag->deadline);
    fprintf(f, "    tasks:\n");
    for (int e = 0; e < p_dag->num_elems; e++) {
        fprintf(f, "      - wcet_ref: %f\n", p_dag->elems[e].C);
        fprintf(f, "        wcet_ref_ns: %f\n", p_dag->elems[e].C_ns);
    }
    fprintf(f, "    edge_list:\n");
    fprintf(f, "     [\n");
    for (int e = 0; e < p_dag->num_elems; e++) {
        foreach_elem_next(p_dag, e)
        fprintf(f, "      [%d,%d],\n",e,n);
    }
    fprintf(f, "     ]\n");
  }
  fclose(f);
}

// Return the unique punit identifier given its island and punit p relative to the island
int sys_get_punit_id(sys_t *p_sys, island_t *p_island, int p) {
  int cpu_id = 0;
  for (int s = 0; s < p_sys->n_islands; s++) {
    island_t *p_isl = &p_sys->islands[s];
    if (p_island == p_isl)
      return cpu_id + p;
    cpu_id += p_isl->n_punits;
  }
  assert(0);
}

#define RTSIM_WL_BUSY "bzip2"
#define RTSIM_WL_IDLE "idle"

void sys_dump_dags_yaml_rtsim(sys_t *p_sys, char *fname) {
  printf("Dumping DAGs to RTSim YAML file: %s\n", fname);
  FILE *f = fopen(fname, "w");
  assert(f != NULL);
  dag_t *p_dag;
  fprintf(f, "taskset:\n");
  for (int d = 0; d < p_sys->n_dags; d++) {
    p_dag = p_sys->dags[d];
    for (int e = 0; e < p_dag->num_elems; e++) {
      fprintf(f, "  - name: task_%d_%d\n", d, e);
      // All tasks belonging to the same DAG have the same period == DAG period
      fprintf(f, "    iat: %f\n", p_dag->period);
      island_t *p_isl = &p_sys->islands[p_dag->elems[e].opt_island];
      float wcet = dag_elem_wcet_scaled(p_dag, e, p_isl, p_isl->opt_freq);
      if (p_isl->type == CPU) {
        fprintf(f, "    cbs_runtime: %f\n", wcet);
        fprintf(f, "    cbs_period: %f\n", p_dag->elems[e].d);
      }
      fprintf(f, "    rdl: %f\n", p_dag->deadline);
      fprintf(f, "    startcpu: %d\n", sys_get_punit_id(p_sys, p_isl, p_dag->elems[e].opt_punit));
      fprintf(f, "    code:\n");
      if (p_dag->elems[e].n_prev == 0)
        // first DAG in task: this prevents two concurrent activations in case of late completion
        fprintf(f, "      - lock(L_DAG_%d)\n", d);
      //
      foreach_elem_prev(p_dag, e) {
        fprintf(f, "      - lock(L_TSK_%d_%d_%d)\n", d, p, e);
      }
      fprintf(f, "      - fixed(%f," RTSIM_WL_BUSY ")\n", wcet);
      foreach_elem_next(p_dag, e) {
        fprintf(f, "      - unlock(L_TSK_%d_%d_%d)\n", d, e, n);
      }
      //
      if (p_dag->elems[e].n_next == 0)
        fprintf(f, "      - unlock(L_DAG_%d)\n", d);
    }
  }
  fprintf(f, "resources:\n");
  for (int d = 0; d < p_sys->n_dags; d++) {
    p_dag = p_sys->dags[d];
    fprintf(f, "  - name: L_DAG_%d\n", d);
    fprintf(f, "    initial_state: unlocked\n");
    for (int e = 0; e < p_dag->num_elems; e++) {
      foreach_elem_next(p_dag, e) {
        fprintf(f, "  - name: L_TSK_%d_%d_%d\n", d, e, n);
        fprintf(f, "    initial_state: locked\n");
      }
    }
  }
  fclose(f);
}

void sys_dump_isl_yaml_rtsim(sys_t *p_sys, char *fname) {
  printf("Dumping islands to RTSim YAML file: %s\n", fname);
  FILE *f = fopen(fname, "w");
  assert(f != NULL);
  fprintf(f, "cpu_islands:\n");
  for (int s = 0; s < p_sys->n_islands; s++) {
    island_t *p_isl = &p_sys->islands[s];
    fprintf(f, "  - name: island%d\n", s);
    fprintf(f, "    numcpus: %d\n", p_isl->n_punits);
    fprintf(f, "    kernel:\n");
    fprintf(f, "      scheduler: %s\n", p_isl->type == CPU ? "edf" : "fifo");
    fprintf(f, "      task_placement: partitioned\n");
    fprintf(f, "    volts:\n");
    fprintf(f, "      [");
    for (int m = p_isl->n_freqs - 1; m >= 0; --m)
      fprintf(f, "%f%s", 1.0, m > 0 ? "," : "");
    fprintf(f, "]\n");
    fprintf(f, "    freqs:\n");
    fprintf(f, "      [");
    for (int m = p_isl->n_freqs - 1; m >= 0; --m)
      fprintf(f, "%d%s", (int)p_isl->freqs[m], m > 0 ? "," : "");
    fprintf(f, "]\n");
    fprintf(f, "    base_freq: %d\n", (int)p_isl->freqs[p_isl->opt_freq]);
    fprintf(f, "    power_model: island%d_psm\n", s);
    fprintf(f, "    speed_model: island%d_psm\n", s);
  }
  static char psm_fname[256];
  fprintf(f, "power_models:\n");
  int num_elems = snprintf(psm_fname, sizeof(psm_fname), "%s_islands_psm.csv", fname);
  assert(num_elems < sizeof(psm_fname));
  for (int s = 0; s < p_sys->n_islands; s++) {
    fprintf(f, "  - name: island%d_psm\n", s);
    fprintf(f, "    type: table_based\n");
    fprintf(f, "    filename: %s\n", psm_fname);
  }
  fclose(f);
  printf("Dumping power-speed model to RTSim CSV file: %s\n", psm_fname);
  f = fopen(psm_fname, "w");
  assert(f != NULL);
  fprintf(f, "model,freq,workload,power,speed\n");
  for (int s = 0; s < p_sys->n_islands; s++) {
    island_t *p_isl = &p_sys->islands[s];
    for (int m = 0; m < p_isl->n_freqs; m++) {
      fprintf(f, "island%d_psm,%d," RTSIM_WL_BUSY ",%f,%f\n", s, (int)p_isl->freqs[m],
              p_isl->pows_busy[m],
              p_isl->freqs[m] * p_isl->capacity / p_isl->freqs[0]);
    }
    for (int m = 0; m < p_isl->n_freqs; m++) {
      fprintf(f, "island%d_psm,%d," RTSIM_WL_IDLE ",%f,%f\n", s, (int)p_isl->freqs[m],
              p_isl->pows_idle[m],
              p_isl->freqs[m] * p_isl->capacity / p_isl->freqs[0]);
    }
  }
  fclose(f);
}

/* example:
   -d 100,100 \
      -t 0,0 \
      -t 80,3 \
      -t 0,0 \
      -n 0,1 \
      -n 1,2 \
*/
void sys_dump_args(sys_t *p_sys, char *fname) {
  FILE *f = fopen(fname, "w");
  assert(f != NULL);
  dag_t *p_dag;
  fprintf(f, "#!/bin/bash\n\n");
  fprintf(f, "../dag/dag \\\n");
  for (int s = 0; s < p_sys->n_islands; s++) {
    island_t *p_isl = &p_sys->islands[s];
    if (p_isl->type == CPU) {
      fprintf(f, "    -i %d,%f \\\n", p_isl->n_punits, p_isl->capacity);
    } else if (p_isl->type == ACCELERATOR) {
      fprintf(f, "    -ai %d,%f \\\n", p_isl->n_punits, p_isl->capacity);
    } else {
      fprintf(stderr, "Error: unable to dump island type: %d!\n", p_isl->type);
      exit(1);
    }
  }
  for (int i = 0; i < p_sys->n_dags; i++) {
    p_dag = p_sys->dags[i];
    fprintf(f, "    -d %f,%f \\\n",p_dag->deadline, p_dag->period);
    for (int e = 0; e < p_dag->num_elems; e++) {
        fprintf(f,"      -t %f,%f \\\n", p_dag->elems[e].C, p_dag->elems[e].C_ns);
    }
    for (int e = 0; e < p_dag->num_elems; e++) {
        foreach_elem_next(p_dag, e)
        fprintf(f, "      -n %d,%d \\\n", e, n);
        fprintf(f, "    -d %f,%f \\\n",p_dag->deadline, p_dag->period);
    }
  }
  fprintf(f, "\"$@\"\n");
  fclose(f);
}

void sys_dump_dot_placed(sys_t *p_sys, char *fname) {
  FILE *f = fopen(fname, "w");
  assert(f != NULL);
  fprintf(f, "digraph G {\n  rankdir=LR;\n");

      for (int d = 0; d < p_sys->n_dags; d++) {
        dag_t *p_dag = p_sys->dags[d];
        for (int e = 0; e < p_dag->num_elems; e++) {
          dag_elem_t *p_elem = &p_dag->elems[e];
          if (p_elem->opt_island == -1) {
            fprintf(f, "    n_%d_%d [ label=\"n%d,%d (%f,%f)\", color=%s ];\n",
                    d, e, d, e,
                    p_dag->elems[e].C, p_dag->elems[e].C_ns,
                    colors[d]);
          }
        }
      }

  for (int s = 0; s < p_sys->n_islands; s++) {
    island_t *p_isl = &p_sys->islands[s];
    fprintf(f, "  subgraph cluster_island_%d {\n", s);
    fprintf(f, "    label=\"island %d -> %d [%.2f Hz]\";\n", s, p_isl->opt_freq, p_isl->freqs[p_isl->opt_freq]);
    for (int p = 0; p < p_isl->n_punits; p++) {
      fprintf(f, "    subgraph cluster_pu_%d {\n", p);
      fprintf(f, "      label=\"pu %d\";\n", p);
      for (int d = 0; d < p_sys->n_dags; d++) {
        dag_t *p_dag = p_sys->dags[d];
        for (int e = 0; e < p_dag->num_elems; e++) {
          dag_elem_t *p_elem = &p_dag->elems[e];
          if (p_elem->opt_island == s && p_elem->opt_punit == p) {
            island_t *p_isl = &p_sys->islands[p_elem->opt_island];
            fprintf(f, "      n_%d_%d [ label=\"n%d,%d (%f,%f) / %f [%.2f]\", color=%s ];\n",
                    d, e, d, e,
                    p_dag->elems[e].C, p_dag->elems[e].C_ns, p_dag->elems[e].d,
                    dag_elem_wcet_scaled(p_dag, e, p_isl, p_isl->opt_freq) / p_dag->elems[e].d,
                    colors[d]);
          }
        }
      }
      fprintf(f, "    }\n");
    }
    fprintf(f, "  }\n");
  }
  for (int d = 0; d < p_sys->n_dags; d++) {
    dag_t *p_dag = p_sys->dags[d];
    for (int e = 0; e < p_dag->num_elems; e++) {
      foreach_elem_next(p_dag, e)
        fprintf(f, "  n_%d_%d -> n_%d_%d\n", d, e, d, n);
    }
  }
  fprintf(f, "}\n");
  fclose(f);
}

void sys_dump_bbsearch(sys_t *p_sys, char *fname) {
    printf("\n\nCOMPACT OUTPUT FORMAT:\n");
    char line[256];
    size_t bufsize = 256;
    char *l_ptr = line;
    float power=0;

    // extract the resulting power. at this point i already know that a solution was generated
    FILE *f = fopen(fname, "r");
    while (!feof(f)) {
        line[0] = 0;
        int read_chars = getline(&l_ptr, &bufsize, f);
        if (read_chars == 0)
            continue;
        if (sscanf(line, "pow %f", &power) == 2) {
            break;
        }
    }
    assert(power!=0);
    printf("Power: %f W\n",power);
    fclose(f);

    // print tasks not placed !?!?
    for (int d = 0; d < p_sys->n_dags; d++) {
        dag_t *p_dag = p_sys->dags[d];
        for (int e = 0; e < p_dag->num_elems; e++) {
            dag_elem_t *p_elem = &p_dag->elems[e];
            if (p_elem->opt_island == -1) {
                printf("WARNING: task_%d_%d (%f,%f) was not placed!\n",
                    d, e, p_dag->elems[e].C, p_dag->elems[e].C_ns);
            }
        }
    }

    int island_mapping[MAX_TASKS_PER_DAG];
    int task_idx=0;
    // this is necessary because the elements are not ordered
    for (int d = 0; d < p_sys->n_dags; d++) {
        dag_t *p_dag = p_sys->dags[d];
        for (int e = 0; e < p_dag->num_elems; e++) {
            dag_elem_t *p_elem = &p_dag->elems[e];
            if (p_elem->C == 0)
              continue;
            island_mapping[task_idx] = p_elem->opt_island;
            ++task_idx;
        }
    }

    for (int s = 0; s < p_sys->n_islands; s++) {
        island_t *p_isl = &p_sys->islands[s];
        printf("Island %d (%.2f Hz): ", s, p_isl->freqs[p_isl->opt_freq]);
        for (int t = task_idx-1; t >= 0; --t) {
            if (s == island_mapping[t]){
                printf("1,");
            }else{
                printf("0,");
            }
        }
        printf("\n");
    }
}

// output binary variables for DAG d: x_dag_task_island_punit
void dump_lp_dag_bin(sys_t *p_sys, int d, FILE *f) {
  dag_t *p_dag = p_sys->dags[d];
  for (int i = 0; i < p_dag->num_elems; i++) {
    for (int s = 0; s < p_sys->n_islands; s++) {
      island_t *p_isl = &p_sys->islands[s];
      for (int p = 0; p < p_isl->n_punits; p++) {
        fprintf(f, " x_%d_%d_%d_%d\n", d, i, s, p);
        for (int m = 0; m < p_isl->n_freqs; m++) {
          fprintf(f, " z_%d_%d_%d_%d_%d\n", d, i, s, p, m);
        }
      }
    }
  }
}

// output constraints for DAG d
void dump_lp_dag_constr(sys_t *p_sys, int d, FILE *f) {
  dag_t *p_dag = p_sys->dags[d];
  for (int i = 0; i < p_dag->num_elems; i++)
    if (p_dag->elems[i].C > 0)
      fprintf(f, " [ d_%d_%d * di_%d_%d ] = 1\n", d, i, d, i);
    else
      fprintf(f, " d_%d_%d = 0\n", d, i);

  for (int i = 0; i < p_dag->num_elems; i++) {
    for (int s = 0; s < p_sys->n_islands; s++) {
      island_t *p_isl = &p_sys->islands[s];
      for (int p = 0; p < p_isl->n_punits; p++)
        fprintf(f, " + x_%d_%d_%d_%d", d, i, s, p);
    }
    fprintf(f, " = 1\n");
  }

  // generating the anti-affinity constraints
  for (int i = 0; i < p_dag->num_elems; i++) {
    dag_elem_t p_elem = p_dag->elems[i];
    for (int j = 0; j < p_elem.n_anti_affinity; ++j) {
      int replica_id = p_elem.anti_affinity[j];
      for (int s = 0; s < p_sys->n_islands; ++s) {
        island_t *p_isl = &p_sys->islands[s];
        for (int p = 0; p < p_isl->n_punits; ++p) {
          fprintf(f, " x_%d_%d_%d_%d + x_%d_%d_%d_%d <= 1\n", d, i, s, p, d, replica_id, s, p);
        }
      }
    }
  }

  fprintf(f, " f_%d_%d - d_%d_%d = 0\n", d, p_dag->first, d, p_dag->first);
  for (int i = 0; i < p_dag->num_elems; i++) {
    if (i == p_dag->first)
      continue;
    foreach_elem_prev(p_dag, i) {
      fprintf(f, " f_%d_%d - f_%d_%d - d_%d_%d >= 0\n", d, i, d, p, d, i);
    }
  }
  fprintf(f, " %g f_%d_%d <= 1\n", lp_scale / p_dag->deadline, d, p_dag->last);

  for (int i = 0; i < p_dag->num_elems; i++) {
    for (int s = 0; s < p_sys->n_islands; s++) {
      island_t *p_isl = &p_sys->islands[s];
      for (int m = 0; m < p_isl->n_freqs; m++) {
        for (int p = 0; p < p_isl->n_punits; p++) {
          fprintf(f, " z_%d_%d_%d_%d_%d - x_%d_%d_%d_%d <= 0\n", d, i, s, p, m, d, i, s, p);
          fprintf(f, " z_%d_%d_%d_%d_%d - y_%d_%d <= 0\n", d, i, s, p, m, s, m);
          fprintf(f, " z_%d_%d_%d_%d_%d - x_%d_%d_%d_%d - y_%d_%d >= -1\n", d, i, s, p, m, d, i, s, p, s, m);
//          fprintf(f, " z_%d_%d_%d_%d_%d - [ x_%d_%d_%d_%d * y_%d_%d ] = 0\n", d, i, s, p, m, d, i, s, p, s, m);
        }
      }
    }
  }
}

void dump_lp_dag_pow(FILE *f, sys_t *p_sys, island_t *p_isl, int s, int p, int m, int d, float pow_diff) {
  dag_t *p_dag = p_sys->dags[d];
  for (int i = 0; i < p_dag->num_elems; i++) {
    fprintf(f, " + %g z_%d_%d_%d_%d_%d", pow_diff * dag_elem_wcet_scaled(p_dag, i, p_isl, m) / p_dag->period, d, i, s, p, m);
  }
}

// easy but pessimistic: sum of bandwidths of all deployed tasks
void dump_lp_punit_constr_easy(FILE *f, sys_t *p_sys, int s, int p, int d) {
  island_t *p_isl = &p_sys->islands[s];
  dag_t *p_dag = p_sys->dags[d];
  fprintf(f, " - u_%d_%d_%d + [ ", d, s, p);
  for (int i = 0; i < p_dag->num_elems; i++) {
    if (p_dag->elems[i].C > 0) {
      for (int m = 0; m < p_isl->n_freqs; m++) {
        fprintf(f, " + %g z_%d_%d_%d_%d_%d * di_%d_%d", dag_elem_wcet_scaled(p_dag, i, p_isl, m) / lp_scale, d, i, s, p, m, d, i);
      }
    }
  }
  fprintf(f, " ] <= 0\n");
}

// less pessimistic: sum of bandwidths of all unrelated task subsets in each DAG
void dump_lp_punit_constr_unrelated(FILE *f, sys_t *p_sys, int s, int p, int d) {
  island_t *p_isl = &p_sys->islands[s];
  dag_t *p_dag = p_sys->dags[d];
  for (int ur = 0; ur < p_dag->unrelated_size; ur++) {
    fprintf(f, " - u_%d_%d_%d + [ ", d, s, p);
    for (int i = 0; i < p_dag->num_elems; i++) {
      if (p_dag->elems[i].C > 0 && (p_dag->unrelated[ur] & (1<<i))) {
        for (int m = 0; m < p_isl->n_freqs; m++) {
          fprintf(f, " + %g z_%d_%d_%d_%d_%d * di_%d_%d", dag_elem_wcet_scaled(p_dag, i, p_isl, m) / lp_scale, d, i, s, p, m, d, i);
        }
      }
    }
    fprintf(f, " ] <= 0\n");
  }
}

void dump_lp_island_constr(FILE *f, sys_t *p_sys, int s) {
  island_t *p_isl = &p_sys->islands[s];
  for (int m = 0; m < p_isl->n_freqs; m++) {
    fprintf(f, " + y_%d_%d", s, m);
  }
  fprintf(f, " = 1\n");

  if (p_isl->type != CPU) {
    return;
  }

  for (int p = 0; p < p_isl->n_punits; p++) {
    for (int d = 0; d < p_sys->n_dags; d++) {
      dag_t *p_dag = p_sys->dags[d];
      if (p_dag->unrelated_size == 0) {
        LOG(INFO,"Using easy but pessimistic utilization constraints for island %d, punit %d, dag %d\n", s, p, d);
        dump_lp_punit_constr_easy(f, p_sys, s, p, d);
      } else {
        LOG(INFO,"Using unrelated-based utilization constraints for island %d, punit %d, dag %d\n", s, p, d);
        dump_lp_punit_constr_unrelated(f, p_sys, s, p, d);
      }
    }
  }
  // with just 1 DAG this constraint is already included in the bounds for the u_* variables (if create_non_binary_bounds == 1)
  if (p_sys->n_dags > 1 || !create_non_binary_bounds) {
    for (int p = 0; p < p_isl->n_punits; p++) {
      for (int d = 0; d < p_sys->n_dags; d++) {
        fprintf(f, " + u_%d_%d_%d", d, s, p);
      }
      fprintf(f, " <= %g\n", max_util);
    }
  }
}

void dump_lp_non_acceleratable_constr(FILE *f, sys_t *p_sys, island_t* isl, int s) {
  for (int d = 0; d < p_sys->n_dags; ++d) {
    dag_t* p_dag = p_sys->dags[d];

    for (int i = 0; i < p_dag->num_elems; ++i) {
      dag_elem_t p_elem = p_dag->elems[i];

      if (!(p_elem.accelerators & (1<<s))) {
        // the task cannot be accelerated on the current accelerator
        // => forcing the corresponding allocation variable to 0
        for (int p = 0; p < isl->n_punits; ++p) {
          fprintf(f, " x_%d_%d_%d_%d = 0\n", d, i, s, p);
        }
      }
    }
  }
}

void dump_lp_acc_waiting_time_bound(FILE *f, sys_t *p_sys, island_t* isl, int s, int p) {
  // my task against all the others
  for (int d = 0; d < p_sys->n_dags; d++) {
    dag_t* p_dag = p_sys->dags[d];

    for (int i = 0; i < p_dag->num_elems; ++i) {
      // taking the unrelated sets of the current dag
      // in which the current task is present
      int non_empty_unrelated_sets_counter = 0;
      for (int j = 0; j < p_dag->unrelated_size; ++j) {
        if (p_dag->unrelated[j] & (1<<i)) {
          int unrelated_tasks_counter = 0;
          // the maximum is turned into a series of ">=" constraints
          // iterating over the tasks in the j-th unrelated set except the i-th one
          for (int k = 0; k < p_dag->num_elems; ++k) {
            if (k == i || !(p_dag->unrelated[j] & (1<<k))) {
              continue;
            }

            if (unrelated_tasks_counter == 0) {
              non_empty_unrelated_sets_counter++;
            }
            unrelated_tasks_counter++;

            if (unrelated_tasks_counter == 1) {
              fprintf(f, " bown_%d_%d_%d_%d", d, i, s, p);
            }

            for (int m = 0; m < isl->n_freqs; m++) {
              fprintf(f, " - %g z_%d_%d_%d_%d_%d", dag_elem_wcet_scaled(p_dag, k, isl, m) / lp_scale, d, k, s, p, m);
            }
          }

          if (unrelated_tasks_counter > 0) {
            fprintf(f, " >=  0\n");
          }
        }
      }

      // computing the contribution to the queuing time by the other DAGs
      int dags_counter = 0;
      for (int h = 0; h < p_sys->n_dags; h++) {
        if (h == d) {
          continue;
        }
        dag_t* p_dag_h = p_sys->dags[h];

        dags_counter++;

        for (int j = 0; j < p_dag_h->unrelated_size; ++j) {
          // the maximum is turned into a series of ">=" constraints
          fprintf(f, " both_%d_%d_%d_%d", d, i, s, p);
          // iterating over the tasks in the j-th unrelated set except the i-th one
          for (int k = 0; k < p_dag_h->num_elems; ++k) {
            if (!(p_dag_h->unrelated[j] & (1<<k))) {
              continue;
            }

            for (int m = 0; m < isl->n_freqs; m++) {
              fprintf(f, " - %g z_%d_%d_%d_%d_%d", dag_elem_wcet_scaled(p_dag_h, k, isl, m) / lp_scale, h, k, s, p, m);
            }
          }
          fprintf(f, " >= 0\n");
        }
      }

      // writing the final bound to the deadline
      fprintf(f, " x_%d_%d_%d_%d = 1 -> d_%d_%d", d, i, s, p, d, i);
      for (int m = 0; m < isl->n_freqs; m++) {
        fprintf(f, " - %g z_%d_%d_%d_%d_%d", dag_elem_wcet_scaled(p_dag, i, isl, m) / lp_scale, d, i, s, p, m);
      }

      if (non_empty_unrelated_sets_counter > 0) {
        fprintf(f, " - bown_%d_%d_%d_%d", d, i, s, p);
      }

      if (dags_counter > 0) {
        fprintf(f, " - both_%d_%d_%d_%d", d, i, s, p);
      }

      fprintf(f, " >= 0\n");
    }
  }
}

void dump_lp_accelerators_constr(FILE *f, sys_t *p_sys) {
  for (int s = 0; s < p_sys->n_islands; ++s) {
    island_t isl = p_sys->islands[s];
    if (isl.type != ACCELERATOR) {
      continue;
    }

    // preventing tasks not having an implementation for the
    // current accelerator to run on it
    dump_lp_non_acceleratable_constr(f, p_sys, &isl, s);

    for (int p = 0; p < isl.n_punits; ++p) {
      dump_lp_acc_waiting_time_bound(f, p_sys, &isl, s, p);
    }
  }
}

void dump_lp_variables_bounds(FILE *f, sys_t *p_sys) {
  for (int d = 0; d < p_sys->n_dags; d++) {
    dag_t *p_dag = p_sys->dags[d];
    for (int i = 0; i < p_dag->num_elems; i++) {
      float wcet_min = dag_elem_wcet_scaled_min(p_sys, p_dag, i);
      fprintf(f, " %g <= d_%d_%d <= %g\n", wcet_min / lp_scale, d, i, p_dag->deadline / lp_scale);
      fprintf(f, " %g <= di_%d_%d <= %g\n", lp_scale / p_dag->deadline, d, i, lp_scale / wcet_min);
      fprintf(f, " 0 <= f_%d_%d <= %g\n", d, i, p_dag->deadline / lp_scale);
    }

    for (int s = 0; s < p_sys->n_islands; s++) {
      island_t *p_isl = &p_sys->islands[s];

      for (int p = 0; p < p_isl->n_punits; p++) {
        if (p_isl->type == CPU) {
          fprintf(f, " 0 <= u_%d_%d_%d <= %g\n", d, s, p, max_util);
        } else if (p_isl->type == ACCELERATOR) {
          for (int i = 0; i < p_dag->num_elems; i++) {
            fprintf(f, " 0 <= bown_%d_%d_%d_%d <= %g\n", d, i, s, p, p_dag->deadline / lp_scale);
            fprintf(f, " 0 <= both_%d_%d_%d_%d <= %g\n", d, i, s, p, p_dag->deadline / lp_scale);
          }
        }
      }
    }
  }
}

void dump_lp_set_min_pow_obj_fun(sys_t *p_sys, FILE *f) {
  fprintf(f, "Minimize\n obj: pow\n");
  fprintf(f, "\nSubject To\n");
}

void dump_lp_set_max_slack_obj_fun(sys_t *p_sys, FILE *f, float power_budget) {
  fprintf(f, "Maximize\n obj: slack\n");
  fprintf(f, "\nSubject To\n");

  fprintf(f, " %g pow <= 1\n", 1.0 / power_budget);

  for (int d = 0; d < p_sys->n_dags; d++) {
    dag_t *p_dag = p_sys->dags[d];
    fprintf(f, " slack + %g f_%d_%d <= 1\n", lp_scale / p_dag->deadline, d, p_dag->last);
  }
}

void dump_lp_sys(sys_t *p_sys, const char *fname, float power_budget) {
  FILE *f = fopen(fname, "w");
  assert(f != NULL);
  assert(p_sys != NULL);

  if (power_budget > 0) {
    dump_lp_set_max_slack_obj_fun(p_sys, f, power_budget);
  } else {
    dump_lp_set_min_pow_obj_fun(p_sys, f);
  }

  fprintf(f, " - pow");

  // the idle power is per island, no mater the load, so this is summed up first
  for (int s = 0; s < p_sys->n_islands; s++) {
    island_t *p_isl = &p_sys->islands[s];
    for (int m = 0; m < p_isl->n_freqs; m++) {
      fprintf(f, " + %g y_%d_%d", p_isl->pows_idle[m], s, m);
    }
  }

  for (int s = 0; s < p_sys->n_islands; s++) {
    island_t *p_isl = &p_sys->islands[s];
    for (int p = 0; p < p_isl->n_punits; p++) {
      for (int m = 0; m < p_isl->n_freqs; m++) {
        // BUG: the idle power was being multiplied by the number of PUs
        //fprintf(f, " + %f y_%d_%d", p_isl->pows_idle[m], s, m);
        for (int d = 0; d < p_sys->n_dags; d++)
          //dump_lp_dag_pow(f, p_sys, p_isl, s, p, m, d, p_isl->pows_busy[m] - p_isl->pows_idle[m]);
          dump_lp_dag_pow(f, p_sys, p_isl, s, p, m, d, p_isl->pows_busy[m]);
      }
    }
  }
  fprintf(f, " = 0\n");

  for (int d = 0; d < p_sys->n_dags; d++) {
    dump_lp_dag_constr(p_sys, d, f);
  }
  for (int s = 0; s < p_sys->n_islands; s++) {
    dump_lp_island_constr(f, p_sys, s);
  }

  dump_lp_accelerators_constr(f, p_sys);

  if (create_non_binary_bounds == 1) {
    fprintf(f, "\nBounds\n");
    dump_lp_variables_bounds(f, p_sys);
  }

  fprintf(f, "\nBinary\n");
  for (int d = 0; d < p_sys->n_dags; d++) {
    dump_lp_dag_bin(p_sys, d, f);
  }

  // output binary variables for island power modes: y_island_mode
  for (int s = 0; s < p_sys->n_islands; s++)
    for (int m = 0; m < p_sys->islands[s].n_freqs; m++)
      fprintf(f, " y_%d_%d\n", s, m);

  fprintf(f, "\nEnd\n");
  fclose(f);
}

void dag_path_init(dag_path_t *p_dag_path) {
  p_dag_path->n_elems = 0;
}

// return scaled WCET if elem e of dag is placed on island with assigned opt_freq;
// return scaled WCET at max island freq if elem e is placed on some island;
// otherwise, return just elem's WCET
float dag_elem_wcet_or_scaled(dag_t *p_dag, sys_t *p_sys, int e) {
  int opt_s = p_dag->elems[e].opt_island;
  if (opt_s == -1)
    return p_dag->elems[e].C;
  else if (p_sys->islands[opt_s].opt_freq == -1)
    return dag_elem_wcet_scaled(p_dag, e, &p_sys->islands[opt_s], 0);
  else
    return dag_elem_wcet_scaled(p_dag, e, &p_sys->islands[opt_s], p_sys->islands[opt_s].opt_freq);
}

// return path length from s to e, both included, or -1 if e is not reachable from s
float dag_longest_path(dag_t *p_dag, sys_t *p_sys, int s, int e, dag_path_t *p_path) {
  float ret_code;
  (void) ret_code;
  LOG(DEBUG,"s=%d, e=%d\n", s, e);
  assert(p_path->n_elems < MAX_PATH_LEN);
  p_path->elems[p_path->n_elems++] = s;
  if (s == e) {
    float len = dag_elem_wcet_or_scaled(p_dag, p_sys, e);
    LOG(DEBUG,"returning node %d (opt_isl %d) len: %f\n", s, p_dag->elems[s].opt_island, len);
    return len;
  }
  float max_len = -1;
  int max_idx = -1;
  int old_n_elems = p_path->n_elems;
  foreach_elem_next(p_dag, s) {
    float len = dag_longest_path(p_dag, p_sys, n, e, p_path);
    p_path->n_elems = old_n_elems;
    if (len != -1 && len > max_len) {
      max_len = len;
      max_idx = n;
    }
  }
  if (max_len == -1)
    return -1;

  // 2nd visit only along max nodes, to actually gather the optimum path in *p_path
  // (it saves memory and time for the plenty of path copies needed otherwise)
  ret_code = dag_longest_path(p_dag, p_sys, max_idx, e, p_path);
  assert(ret_code == max_len);
  float len = dag_elem_wcet_or_scaled(p_dag, p_sys, s) + max_len;
  LOG(DEBUG,"returning %f\n", len);

  return len;
}

void dag_path_dump(dag_path_t *p_path) {
  for (int i = 0; i < p_path->n_elems; i++)
    printf("%d,", p_path->elems[i]);
}

void dag_clear_deadlines(dag_t *p_dag) {
  for (int e = 0; e < p_dag->num_elems; e++)
    p_dag->elems[e].d = -1;
}

void dag_set_deadlines(dag_t *p_dag, sys_t *p_sys, int s, int e, float D, int clear) {
  LOG(DEBUG,"s=%d, e=%d, D=%f, clear=%d\n", s, e, D, clear);
  if (clear)
    dag_clear_deadlines(p_dag);

  dag_path_t path;
  dag_path_init(&path);
  float C_sum = dag_longest_path(p_dag, p_sys, s, e, &path);
  LOG(DEBUG,"path (C_sum=%f): [ ", C_sum);
#if LOG_LEVEL >= 3
  dag_path_dump(&path);
  printf(" ]\n");
#endif

  if (C_sum == -1) // possible in recursive calls with e non-reachable from s
    return;
  float D_sum = D;
  for (int i = 0; i < path.n_elems; i++) {
    dag_elem_t *p_elem = &p_dag->elems[path.elems[i]];
    if (p_elem->d != -1) {
      LOG(DEBUG,"Discounting already assigned params for node %d: d=%f, C_sum=%f\n",
             path.elems[i], p_elem->d, p_elem->C);
      D_sum -= p_elem->d;
      C_sum -= p_elem->C;
    }
  }
  //printf("setting relative deadlines on longest path: D_sum=%f, C_sum=%f\n", D_sum, C_sum);
  for (int i = 0; i < path.n_elems; i++) {
    dag_elem_t *p_elem = &p_dag->elems[path.elems[i]];
    if (p_elem->d == -1) {
      if (p_elem->C > 0) {
        float wcet = dag_elem_wcet_or_scaled(p_dag, p_sys, path.elems[i]);
        assert(C_sum > 0);
        p_elem->d = D_sum * wcet / C_sum;
        C_sum -= wcet;
      } else {
        p_elem->d = 0;
      }
      LOG(DEBUG,"set elems[%d]->d = %f\n", path.elems[i], p_elem->d);
      D_sum -= p_elem->d;
    }
  }
  float D_tmp = D;
  for (int i = 0; i < path.n_elems - 1; i++) {
    D -= p_dag->elems[path.elems[i]].d;
    foreach_elem_next(p_dag, path.elems[i]) {
      LOG(DEBUG,"i=%d, n=%d\n", i, n);
      if (n != path.elems[i+1]) {
        D_tmp = D;
        for (int j = path.n_elems - 1; j > i; j--) {
          D_tmp -= p_dag->elems[path.elems[j]].d;
          foreach_elem_prev(p_dag, path.elems[j]) {
            LOG(DEBUG,"j=%d, p=%d\n", j, p);
            if (p != path.elems[j-1]) {
              dag_set_deadlines(p_dag, p_sys, n, p, D_tmp, 0);
            }
          }
        }
      }
    }
  }
}

void sys_set_deadlines(sys_t *p_sys) {
  for (int d = 0; d < p_sys->n_dags; d++) {
    dag_t *p_dag = p_sys->dags[d];
    if (p_dag->unrelated_size == 0)
      dag_comp_unrelated(p_dag);
    dag_set_deadlines(p_dag, p_sys, p_dag->first, p_dag->last, p_dag->deadline, 0);
  }
}

void sys_clear_deadlines(sys_t *p_sys) {
  for (int d = 0; d < p_sys->n_dags; d++)
    dag_clear_deadlines(p_sys->dags[d]);
}

void sys_init(sys_t *p_sys) {
  p_sys->n_islands = 0;
  p_sys->n_dags = 0;
  p_sys->hyperperiod = 0;
}

void island_dump(island_t *p_isl) {
  printf("punits %d, cap %f, sched %d, opt_freq: %d\n",
         p_isl->n_punits, p_isl->capacity, p_isl->sched, p_isl->opt_freq);
  assert(p_isl->table != NULL);
  print_table(p_isl->table);
  // this is a simpler printing format
  //print_csv(p_isl->table);
}

void sys_dump(sys_t *p_sys) {
  printf("System:\n");
  if (p_sys->n_islands == 0)
    printf("  no islands defined!\n");
  for (int i = 0; i < p_sys->n_islands; i++) {
    printf("  island %d: ", i);
    island_dump(&p_sys->islands[i]);
  }
  printf("DAGs with hyperperiod %ld\n", p_sys->hyperperiod);
  if (p_sys->n_dags == 0)
    printf("  no dags defined!\n");
  for (int d = 0; d < p_sys->n_dags; d++) {
    dag_t *p_dag = p_sys->dags[d];
    dag_dump(p_dag);
    dag_dump_unrelated(p_dag);
  }
}

void sys_cleanup(sys_t *p_sys){
  for (int d = 0; d < p_sys->n_dags; d++) {
      dag_cleanup(p_sys->dags[d]);
  }
  for (int s = 0; s < p_sys->n_islands; s++) {
    free_table(p_sys->islands[s].table);
  }
}

island_t *sys_add_island(sys_t *p_sys, float capacity, int n_punits, sys_sched_t sched, island_type_enum type) {
  assert(p_sys->n_islands < MAX_ISLANDS);
  assert(n_punits < MAX_PUNITS);
  island_t *p_island = &p_sys->islands[p_sys->n_islands++];
  p_island->n_punits = n_punits;
  p_island->sched = sched;
  p_island->capacity = capacity;
  p_island->opt_freq = -1;
  for (int i = 0; i < n_punits; i++) {
    p_island->punits[i].p_island = p_island;
  }
  p_island->type = type;
  char table_name[64];
  snprintf(table_name,64,"Power Table Island %d", p_sys->n_islands-1);
  p_island->table = table_init(stdout, (unsigned char*)table_name, 1);
  table_row(p_island->table, (unsigned char*)"%s%s%s", "Freq (Hz)", "Busy Power (W)", "Idle Power (W)");
  table_separator(p_island->table);
  return p_island;
}

unsigned long gcd(unsigned long a, unsigned long b) {
  while (a != b) {
    if (a > b) {
      a %= b;
      if (a == 0)
        return b;
    } else {
      b %= a;
      if (b == 0)
        return a;
    }
  }
  return a;
}

unsigned long mcm(unsigned long a, unsigned long b) {
  unsigned long d = gcd(a, b);
  return a / d * b;
}

void sys_add_dag(sys_t *p_sys, dag_t *p_dag) {
  assert(p_sys->n_dags < MAX_DAGS);
  p_sys->dags[p_sys->n_dags++] = p_dag;
  if (p_sys->n_dags == 1)
    p_sys->hyperperiod = p_dag->period;
  else
    p_sys->hyperperiod = mcm(p_sys->hyperperiod, p_dag->period);
  printf("Updated hyperperiod %ld\n", p_sys->hyperperiod);
}

void island_add_freq(island_t *p_sys, float f, float pow_busy, float pow_idle) {
  assert(p_sys->n_freqs < MAX_FREQS);
  p_sys->freqs[p_sys->n_freqs] = f;
  p_sys->pows_busy[p_sys->n_freqs] = pow_busy;
  p_sys->pows_idle[p_sys->n_freqs++] = pow_idle;
  // populate the table for nice priting
  table_row(p_sys->table, (unsigned char*)"%.2f%.8f%.8f", f, pow_busy, pow_idle);
}

int parse_gurobi_sol(sys_t *p_sys, const char *fname) {
  int rv = 0;
  size_t size = 256;
  char *line = malloc(size);
  float power=0;
  // the whether the file was generated w any content
  FILE *f = fopen(fname, "r");
  if(f == NULL){
      printf ("Unable to find a valid solution !\n\n");
      exit(1);
  }
  fseek(f, 0L, SEEK_END);
  if (ftell(f) == 0){
      printf ("Unable to find a valid solution !\n\n");
      exit(1);
  }
  fseek(f, 0L, SEEK_SET);
  // extract the resulting power to check whether any solution was generated
  while (!feof(f)) {
      size_t size;
      line[0] = 0;
      int read_chars = getline(&line, &size, f);
      if (read_chars == 0)
          continue;
      if (sscanf(line, "pow %f", &power) == 2) {
          break;
      }
  }
  fseek(f, 0L, SEEK_SET);

  while (!feof(f)) {
    line[0] = 0;
    int read_chars = getline(&line, &size, f);
    if (read_chars == 0)
      continue;
    int d, i, s, p, m;
    float ibool; // 99.9 % of the time gurobi prints 0 or 1 in the solution, but sometimes it prints 9.9999999999999967e-01 :/
    float dline;
    if (strncmp(line, "# Solution for model", 20) == 0) {
      rv = 1;
    } else if (sscanf(line, "d_%d_%d %f", &d, &i, &dline) == 3) {
      dline *= lp_scale;
      assert(d >= 0 && d < p_sys->n_dags);
      assert(i >= 0 && i < p_sys->dags[d]->num_elems);
      p_sys->dags[d]->elems[i].d = dline;
    } else if (sscanf(line, "x_%d_%d_%d_%d %f", &d, &i, &s, &p, &ibool) == 5 && FEQUAL(ibool,1.0f)) {
      assert(d >= 0 && d < p_sys->n_dags);
      assert(i >= 0 && i < p_sys->dags[d]->num_elems);
      assert(s >= 0 && s < p_sys->n_islands);
      assert(p >= 0 && p < p_sys->islands[s].n_punits);
      p_sys->dags[d]->elems[i].opt_island = s;
      p_sys->dags[d]->elems[i].opt_punit = p;
    } else if (sscanf(line, "y_%d_%d %f", &s, &m, &ibool) == 3 && FEQUAL(ibool,1.0f)) {
      assert(s >= 0 && s < p_sys->n_islands);
      assert(m >= 0 && m < p_sys->islands[s].n_freqs);
      p_sys->islands[s].opt_freq = m;
    }
  }
  free(line);
  fclose(f);
  return rv;
}

static char cmd[1024];
int solve_gurobi(sys_t *p_sys) {
  printf("Solving with gurobi...\n");
  int ret = snprintf(cmd, sizeof(cmd),
    "rm -f %s_log.txt && gurobi_cl Threads=%d NonConvex=2 ResultFile=%s LogFile=%s_log.txt SolFiles=%s %s\n",
    out_sols_pname, gurobi_max_threads, out_sol_fname, out_sols_pname, out_sols_pname, out_lp_fname);
  assert(ret < sizeof(cmd));
  printf("Running cmd: %s\n", cmd);
  int rv = system(cmd);
  if (rv == -1) {
    fprintf(stderr, "Error: %s\n", strerror(errno));
    exit(1);
  }
  ret = snprintf(cmd, sizeof(cmd),
           "grep 'Optimal solution found' %s_log.txt > /dev/null 2>&1 "
           "&& ! grep 'model may be infeasible' %s_log.txt > /dev/null 2>&1"
           "&& ! grep 'max constraint violation .* exceeds tolerance' %s_log.txt > /dev/null 2>&1",
           out_sols_pname, out_sols_pname, out_sols_pname);
  assert(ret < sizeof(cmd));
  printf("Running cmd: %s\n", cmd);
  rv = system(cmd);
  if (rv != 0) {
    fprintf(stderr, "Error: Gurobi could not find an optimal feasible solution!\n");
    exit(1);
  }
  return parse_gurobi_sol(p_sys, out_sol_fname);
}

float punit_tot_util(sys_t *p_sys, int s, int p) {
  island_t *p_isl = &p_sys->islands[s];
  int m = p_isl->opt_freq;
  float u_punit_tot = 0.0;
  for (int d = 0; d < p_sys->n_dags; d++) {
    dag_t *p_dag = p_sys->dags[d];
    float u_dag_max = 0.0; // max U occupied by p_dag on punit (s, p)
    for (int ur = 0; ur < p_dag->unrelated_size; ur++) {
      float u_dag_sum = 0.0;
      for (int e = 0; e < p_dag->num_elems; e++) {
        dag_elem_t *p_elem = &p_dag->elems[e];
        if ((p_dag->unrelated[ur] & (1<<e)) && p_elem->C > 0 && p_elem->opt_island == s && p_elem->opt_punit == p) {
          float u = dag_elem_wcet_scaled(p_dag, e, p_isl, m)
            / (p_elem->d != -1 ? p_elem->d : p_dag->deadline);
          u_dag_sum += u;
        }
      }
      if (u_dag_sum > u_dag_max)
        u_dag_max = u_dag_sum;
    }
    u_punit_tot += u_dag_max;
  }
  return u_punit_tot;
}

/*
 * worst-fit: search for (island, core) minimizing the max occupied bandwidth after addition of p_elem
 *   bandwidths obtained dividing by either the relative deadlines if present, or the dag deadlines
 *   occupied bandwidth computed according to the unrelated[] sets, that must have been pre-computed
 */
void place_wf_elem(sys_t *p_sys, dag_elem_t *p_elem) {
  int opt_s = -1, opt_p = -1;
  float opt_u = 1.0;
  for (int s = 0; s < p_sys->n_islands; s++) {
    island_t *p_isl = &p_sys->islands[s];
    for (int p = 0; p < p_isl->n_punits; p++) {
      p_elem->opt_island = s;
      p_elem->opt_punit = p;
      float u_new = punit_tot_util(p_sys, s, p);
      if (opt_s == -1 || u_new < opt_u) {
        opt_s = s;
        opt_p = p;
        opt_u = u_new;
      }
    }
  }
  p_elem->opt_island = opt_s;
  p_elem->opt_punit = opt_p;
}

void place_wf(sys_t *p_sys) {
  for (int d = 0; d < p_sys->n_dags; d++) {
    dag_t *p_dag = p_sys->dags[d];
    for (int e = 0; e < p_dag->num_elems; e++) {
      dag_elem_t *p_elem = &p_dag->elems[e];
      if (p_elem->opt_island == -1) {
        place_wf_elem(p_sys, p_elem);
      }
    }
  }
}

int solve_heur(sys_t *p_sys) {
  printf("Solving with greedy heuristics based on worst-fit...\n");

  if (out_sols_pname == NULL) {
    out_sols_pname = strdup("/tmp/dag-XXXXXX");
    out_sols_pname = mkdtemp(out_sols_pname);
  }
  // check assumption: islands ordered from max to min capacity
  for (int s = 0; s < p_sys->n_islands - 1; s++) {
    assert(p_sys->islands[s].capacity >= p_sys->islands[s+1].capacity);
  }
  for (int s = 0; s < p_sys->n_islands; s++) {
    island_t *p_isl = &p_sys->islands[s];
    // check assumption: OPPs ordered from max to min frequency
    for (int m = 0; m < p_isl->n_freqs - 1; m++) {
      assert(p_isl->freqs[m] >= p_isl->freqs[m+1]);
    }
    p_isl->opt_freq = 0;
  }
  place_wf(p_sys);
  // TODO: check whether the obtained solution satisfies all constraints
  // for now, all solutions are valid
  return 1;
}

dag_t *build_dag_random(int max_period, int max_elems, int wcet_max, int max_edges) {
  dag_t *p_dag = malloc(sizeof(dag_t));
  assert(p_dag != NULL);
  p_dag->unrelated = NULL;
  p_dag->relatives = NULL;
  int period = max_period/4 + 3*max_period/4 * (long)rand() / (RAND_MAX + 1l);
  int deadline = period/4 + 3*period/4 * (long)rand() / (RAND_MAX + 1l);
  // sched deadline does not accept a period/deadline < 1024 (time unit is ns)
  assert(period >= 1024);
  assert(deadline >= 1024);
  dag_init(p_dag, deadline, period);
  // 3+ means that the dag must have at least one source/target node + another node
  int n_elems = 3 + (max_elems - 2) * (long)rand() / (RAND_MAX + 1l);
  // printf("deadline %d, period %d, n_elems %d\n", deadline, period, n_elems);
  for (int i = 0; i < n_elems; i++) {
    // sched deadline does not accept a task runtime < 1024
    int wcet = wcet_max/4 + 3*wcet_max/4 * (long)rand() / (RAND_MAX + 1l);
    assert(wcet >= 1024);
    int wcet_ns = wcet_max/4 * (long)rand() / (RAND_MAX + 1l);
    dag_add_elem(p_dag, wcet, wcet_ns, -1);
  }
  int n_edges = max_edges/4 + 3*max_edges/4 * (long)rand() / (RAND_MAX + 1l);
  do {
    int i = (n_elems-1) * (long)rand() / (RAND_MAX + 1l);
    int j = i+1 + (n_elems-1-i) * (long)rand() / (RAND_MAX + 1l);
    LOG(DEBUG,"i=%d,j=%d\n", i, j);
    assert(i >= 0 && i < n_elems-1 && j > i && j < n_elems);
    // does not allow a starting node to connect directly to the last node
    if (i==0 &&  j==(n_elems-1))
        continue;
    if (p_dag->elems[i].n_next < MAX_NEXT && p_dag->elems[j].n_prev < MAX_PREV){
      dag_add_prev(p_dag, j, i);
    }
  } while (--n_edges > 0);
  // this part makes sure that the 1st and last tasks are the single input/output tasks
  int p = 0;
  for (int i = 1; i < n_elems; i++){
    if (!dag_is_reachable(p_dag, 0, i)){
        if (p_dag->elems[p].n_next < MAX_NEXT && p_dag->elems[i].n_prev < MAX_PREV){
          dag_add_prev(p_dag, i, p);
        }else{
          free(p_dag);
          return NULL;
        }
    }else{
      p = i;
    }
  }
  int n = n_elems-1;
  for (int i = n_elems-2; i >=0; i--){
    if (!dag_is_reachable(p_dag, i, n_elems-1)){
        if (p_dag->elems[i].n_next < MAX_NEXT && p_dag->elems[n].n_prev < MAX_PREV){
          dag_add_prev(p_dag, n, i);
        }else{
          free(p_dag);
          return NULL;
        }
    }
  }

  return p_dag;
}

void sys_dump_output_yaml(sys_t *p_sys, char *sol_fname,  char *fname,
    char *dag_name, char *platform_name, unsigned long exec_time_us){
    FILE *f = fopen(fname, "w");
    assert(f != NULL);
    assert(platform_name != NULL);
    assert(dag_name != NULL);

    // complement the output yaml with more information from the main() context:
    fprintf(f, "tool_name: \"dag.c\"\n");
    fprintf(f, "platform_filename: \"\"\n"); // dag.c has not file describing the hw platform
    fprintf(f, "platform_name: \"%s\"\n", platform_name);
    fprintf(f, "dag_filename: \"%s\"\n", dag_name); // in this case, its the lp filename wo extension
    fprintf(f, "execution_time_us: %ld\n", exec_time_us);

    // TODO: add yaml attribute ... need to parse the gurobi log file
    // - gap, to say whehter the solution is optimal
    fprintf(f, "n_dags: %d\n",p_sys->n_dags);

    // TODO: this code to read the power is dupliated w sys_dump_bbsearch. a better solution is
    // to add p_sys->power field and parse this only once
    char line[256];
    char *l_ptr = line;
    size_t line_size = 256;
    float power=0;
    // extract the resulting power. at this point i already know that a solution was generated
    FILE *fp = fopen(sol_fname, "r");
    while (!feof(fp)) {
        line[0] = 0;
        int read_chars = getline(&l_ptr, &line_size, fp);
        if (read_chars == 0)
            continue;
        if (sscanf(line, "pow %f", &power) == 2) {
            break;
        }
    }
    assert(power!=0);
    fclose(fp);
    fprintf(f, "power: %f\n",power);

    fprintf(f, "islands:\n");
    for (int s = 0; s < p_sys->n_islands; s++) {
        island_t *p_isl = &p_sys->islands[s];
        fprintf(f, "    - capacity: %f\n", p_isl->capacity);
        fprintf(f, "      frequency: %d\n", (int)p_isl->freqs[p_isl->opt_freq]);
        fprintf(f, "      tasks: [\n");
        // get all tasks that are mapped to this island
        for (int d = 0; d < p_sys->n_dags; d++) {
            dag_t *p_dag = p_sys->dags[d];
            for (int e = 0; e < p_dag->num_elems; e++) {
                dag_elem_t *p_elem = &p_dag->elems[e];
                if (p_elem->opt_island == s){
                    fprintf(f, "                [%d,%d],\n", d, e);
                }
            }
        }
        fprintf(f, "             ]\n");
        fprintf(f, "      pus: \n");
        for (int p = 0; p < p_isl->n_punits; p++) {
            fprintf(f, "            - [\n");
            // get all tasks that are mapped onto unit p and island s
            for (int d = 0; d < p_sys->n_dags; d++) {
                dag_t *p_dag = p_sys->dags[d];
                for (int e = 0; e < p_dag->num_elems; e++) {
                    dag_elem_t *p_elem = &p_dag->elems[e];
                    if (p_elem->opt_island == s && p_elem->opt_punit == p){
                        // wcet must be truncated, otherwise it created situations where wcet/del > 1
                        float wcet = dag_elem_wcet_scaled(p_dag, e, p_isl, p_isl->opt_freq);
                        float util = wcet / p_dag->elems[e].d;
                        assert(util <= 1.0);
                        fprintf(f, "                 [%f,%d,%d],\n", util, d, e);
                    }
                }
            }
            fprintf(f, "              ]\n");
        }
    }

    fprintf(f, "tasks:\n");
    for (int d = 0; d < p_sys->n_dags; d++) {
        dag_t *p_dag = p_sys->dags[d];
        for (int e = 0; e < p_dag->num_elems; e++) {
            dag_elem_t *p_elem = &p_dag->elems[e];
            fprintf(f, "    - id: [%d,%d]\n", d, e);
            float dline = p_dag->elems[e].d;
            fprintf(f, "      deadline: %f\n", dline);
            island_t *p_isl = &p_sys->islands[p_elem->opt_island];
            float wcet = dag_elem_wcet_scaled(p_dag, e, p_isl, p_isl->opt_freq);
            fprintf(f, "      wcet: %f\n", wcet);
            assert (wcet <= dline);
        }
    }

    fclose(f);
}
