all:
	cd dag && make
	cd tif && mkdir -p build && cd build && cmake .. && make
	cd bb-search && mkdir -p build && cd build && cmake .. && make

clean:
	rm -f *~ *.bak
	cd dag && make clean
	cd tif && rm -rf build/*
	cd bb-search && rm -rf build/*
