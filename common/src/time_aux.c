#include <time.h>
#include "time_aux.h"

uint64_t micros(){
    struct timespec ts;
    clock_gettime(CLOCK_MONOTONIC, &ts);
    uint64_t us = SEC_TO_US((uint64_t)ts.tv_sec) + NS_TO_US((uint64_t)ts.tv_nsec);
    return us;
}

// using the wall-clock time micros() here is a bad error: a task may be scheduled along with others
// here we need to enforce a given execution time on the CPU, so we're using THREAD_CPUTIME_ID
void Count_Time(uint64_t us){
    struct timespec ts_beg, ts_end;
    clock_gettime(CLOCK_THREAD_CPUTIME_ID, &ts_beg);
    unsigned long elapsed_us;
    do {
      clock_gettime(CLOCK_THREAD_CPUTIME_ID, &ts_end);
      elapsed_us = (ts_end.tv_sec - ts_beg.tv_sec) * 1000000 + (ts_end.tv_nsec - ts_beg.tv_nsec) / 1000;
    } while (elapsed_us < us);
}

// Different from Count_Time, Count_Ticks actually executes a fixed amont of work and, for this reason,
// the execution time will change when you change the cpu frequency
// Note that this busy-wait could be simpler:
//    for(int i=0;i<ticks;++i);
// However, this simple implementation translates to memory accesses to increment 'i'
// The proposed one doesnt add memory access, thus, it's more into accordance with amalthea/autosar model of a runnable
/////
// Check the generated assembly in https://godbolt.org/
/////
uint64_t Count_Ticks(uint64_t ticks){
   // 256 is the number of ops inside the 1st while loop
   // left_over is the remaining ticks, but 4 with ops per iteration
   uint64_t left_over = (ticks & !(256-1))/4;
   uint64_t numLoops = ticks / 256; 
   uint32_t nop=0;
   uint64_t i=0;
   for (i = 0; i < numLoops; i++) {
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
   }
   for (i = 0; i < left_over; i++) {
      nop ^=1;
      nop ^=1;
      nop ^=1;
      nop ^=1;
   }   
   return nop;
}
