#ifndef  COMMON_H_
#define COMMON_H_

#define MAX_ISLANDS 4
#define MAX_DAGS 4
#define MAX_TASKS_PER_DAG 32
#define MAX_FREQS 20
#define MAX_PUNITS 6
#define DEF_MAX_UTIL 0.95

#endif // COMMON_H_