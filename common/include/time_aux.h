#ifndef TIME_AUX_H_
#define TIME_AUX_H_

#include <stdint.h>

#ifdef  __cplusplus
extern "C" {
#endif

/// Convert seconds to milliseconds
#define SEC_TO_MS(sec) ((sec)*1000)
/// Convert seconds to microseconds
#define SEC_TO_US(sec) ((sec)*1000000)
/// Convert seconds to nanoseconds
#define SEC_TO_NS(sec) ((sec)*1000000000)

/// Convert nanoseconds to seconds
#define NS_TO_SEC(ns)   ((ns)/1000000000)
/// Convert nanoseconds to milliseconds
#define NS_TO_MS(ns)    ((ns)/1000000)
/// Convert nanoseconds to microseconds
#define NS_TO_US(ns)    ((ns)/1000)

/// Convert microseconds to seconds
#define US_TO_SEC(us)   ((us)/1000000)
/// Convert microseconds to miliseconds
#define US_TO_MSEC(us)   ((us)/1000)
/// Convert microseconds to nanoseconds
#define US_TO_NSEC(us)   ((us)*1000)

uint64_t micros();


// it runs in busy waiting (no task suspension) for time specified in microsecs
// currently, count ticks is not used 
#ifdef USE_COUNT_TICK
    #if defined(__GNUC__) && (__GNUC__ > 4 || (__GNUC__ == 4 && __GNUC_MINOR__ >= 7))
        uint64_t Count_Ticks(uint64_t ticks) __attribute__ ((optimize("0")));
    #elif defined(__clang__) && (__clang_major__ > 3 || (__clang_major__ == 3 && __clang_minor__ >= 3))
        uint64_t Count_Ticks(uint64_t ticks) __attribute__ ((optnone));
    #else
        #error "Unsupported compiler. Expecting gcc 7.0 or newer, or clang 10 or newer"
    #endif
#endif

/*
The use of Count_Time has been selected over Count_Ticks to be compatible with the current power model which is 
time-based. In addition, sched_deadline parameters are also time based. Finally, Count_Time does not rely on 
compiler specific pragmas to turn optimization off.

Hooooooever, Count_Time complicates DVFS analysis because, whatever the frequency you use to run the program, 
the task execution time will always be the same. Count_Ticks does not have this issue since it's inputs are 
ticks, not time. Tick is a kind of 'workload unit' where a tick correspond to a simple logic operation.

To solve this issue, we selected Count_Time approach but the time is scaled up/or down by the converters that generate the 
dag.h file. This ways, rt_dag remains simple, with no platform related information rather than the cpu affinity.
All the scale up/down complecity and platform awareness is pushed to the converters.
*/
void Count_Time(uint64_t ticks);


#ifdef  __cplusplus
}
#endif

#endif // TIME_AUX_H_
