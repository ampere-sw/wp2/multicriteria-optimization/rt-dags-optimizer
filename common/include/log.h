#ifndef  LOG_H_
#define LOG_H_

#include <stdio.h>
/*
Log lib following the ideas described in 
http://dev.kotula.net.pl/2016/02/08/c-logger-basics/

Authors: 
    * Alexandre Amory (June 2022), ReTiS Lab, Scuola Sant'Anna, Pisa, Italy.

How to use: 
    * set -DLOG_LEVEL=X in the Makefile to select verbosity level.

LOG_LEVEL 0: All logs are off
LOG_LEVEL 1: Error logs or higher
LOG_LEVEL 2: Warning logs or higher
LOG_LEVEL 3: Info logs or higher
LOG_LEVEL 4: Debug logs or higher

Usage Example :
    int main(int argc) {

        LOG(ERROR, "Deu Pau !!!\n");
        LOG(WARNING, "Mega bug !!!\n");
        LOG(INFO, "Annoying useless messages !!!\n");
        LOG(DEBUG, "Hello world %d\n", argc);
        LOG(DEBUG, "Hello world\n");
        return 0;
    }

Output:
    ERROR   log.c:main(68): Deu Pau !!!
    WARNING log.c:main(69): Mega bug !!!
    INFO    log.c:main(70): Annoying useless messages !!!
    DEBUG   log.c:main(71): Hello world 1
    DEBUG   log.c:main(72): Hello world

Tip: 
    *try this code in https://godbolt.org/ ;)
*/

#define LOG(level, ...)  _LOG_ ## level(__VA_ARGS__)

// LOG_LEVEL == 0 is the default
#ifndef LOG_LEVEL
#define LOG_LEVEL 0
#endif

#if LOG_LEVEL == 0
#define _LOG_ERROR(...)
#define _LOG_DEBUG(...)
#endif

#if LOG_LEVEL >= 1
#define _LOG_ERROR(...)  __LOG(ERROR, __VA_ARGS__)
#else
#define _LOG_ERROR(...)
#endif

#if LOG_LEVEL >= 2
#define _LOG_WARNING(...)  __LOG(WARNING, __VA_ARGS__)
#else
#define _LOG_WARNING(...)
#endif

#if LOG_LEVEL >= 3
#define _LOG_INFO(...)  __LOG(INFO, __VA_ARGS__)
#else
#define _LOG_INFO(...)
#endif

#if LOG_LEVEL >= 4
#define _LOG_DEBUG(...)  __LOG(DEBUG, __VA_ARGS__)
#else
#define _LOG_DEBUG(...)
#endif


#define _LOG_SIZEOF(...)  __LOG_SIZEOF(__VA_ARGS__,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,1,0)
#define __LOG_SIZEOF(a1,a2,a3,a4,a5,a6,a7,a8,a9,a10,a11,a12,a13,a14,a15,a16,a17,a18,a19,a20,N,...)  N
#define _LOG_EVAL(x) x
#define __LOG(level, ...)  ___LOG(_LOG_SIZEOF(__VA_ARGS__), level, __VA_ARGS__)
#define ___LOG(n, ...)  ____LOG(n, __VA_ARGS__)
#define ____LOG(n, ...)  ____LOG ## n(__VA_ARGS__)
#define ____LOG1(level, format)  fprintf(stderr, #level "\t%s:%s(%d): " format , __builtin_strrchr(__FILE__, '/') ? __builtin_strrchr(__FILE__, '/')+1 : __FILE__, __func__, __LINE__)
#define ____LOG2(level, format, ...)  fprintf(stderr, #level "\t%s:%s(%d): " format ,  __builtin_strrchr(__FILE__, '/') ? __builtin_strrchr(__FILE__, '/')+1 : __FILE__, __func__, __LINE__, __VA_ARGS__)

#endif // LOG_H_
