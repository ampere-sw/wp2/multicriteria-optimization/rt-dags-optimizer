
# How to generate platform YAML files for another board

The existing platform YAML files were generated from a previous power profiling methodology described in the following paper

```bibtex
@article{mascitti2021dynamic,
  title={Dynamic partitioned scheduling of real-time tasks on ARM big. LITTLE architectures},
  author={Mascitti, Agostino and Cucinotta, Tommaso and Marinoni, Mauro and Abeni, Luca},
  journal={Journal of Systems and Software},
  volume={173},
  pages={110886},
  year={2021},
  publisher={Elsevier}
}
```

The following [link](https://gitlab.retis.sssup.it/parts/partsim/-/blob/master/simconf/models/tb/) provides a list of power profiling data for different boards. Each file represents different methods to perform the statistics. The discussion of the advantages and disadvantages of each method for speed and power is above mentioned paper. We are using the *maximum* for the speed, because it’s the “safest” option for real time tasks, similar to a WCET. We are also using *fixed regression* for the power since it has the lowest 90th percentile error over the entire dataset. 

Finally, these power profile CSV files used for each platform:
 - [Odroid-xu3/xu4](https://gitlab.retis.sssup.it/parts/partsim/-/blob/master/simconf/models/tb/odroid-xu3-maximum-fixed_regression.csv);
 - [Zcu102](https://gitlab.retis.sssup.it/parts/partsim/-/blob/master/simconf/models/tb/zcu102-maximum-fixed_regression.csv);
 - [Raspberry Pi 4 Model B](https://gitlab.retis.sssup.it/parts/partsim/-/blob/master/simconf/models/tb/raspberry-maximum-fixed_regression.csv);

Each CSV also describes the profiling of different tasks. For the optimization process we have chosen the *encrypt* task because it is considered a common/representative workload. The *idle power* is encoded in the CSV file as another application called *idle*. It is in the last lines of the CSV file. The *busy power* is the sum of the *power* column of the selected application and the idle power. 

For each platform, there are excel files performing some basic consistency check and plots with the power data. 
