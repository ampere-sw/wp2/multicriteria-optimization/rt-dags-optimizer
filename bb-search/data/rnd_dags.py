import os
import shutil

MAX_DAGS = 4
MAX_PERIOD = 1000
MAX_WCET = 100
MAX_TASKS = 10
MAX_EDGES = MAX_TASKS*2

n_task_set=10

for d in range(MAX_DAGS):
    #  #dags, max_period, int max_elems, int wcet_max, int max_edges
    os.system("../../dag/dag -r %d,%d,%d,%d,%d" % (d,MAX_PERIOD,MAX_TASKS,MAX_WCET,MAX_EDGES))
    dest_dir = "./rnd_dags/%2d" % d
    if not os.path.exists(dest_dir):
        os.makedirs(dest_dir)
    files = os.listdir("rnd*.dot")
    files.append("rnd.sh")
    files.append("rnd.yaml")
    for f in files:
        shutil.move(f, dest_dir)
