#ifndef UNREL_SEQ_H
#define UNREL_SEQ_H

#include <vector>
#include <string>
#include <cstdint>

using namespace std;

class SwModelMat;

/*
  This class hides the logic to implement the sequence of unrelated tasks among multiple DAGs,
  which is used in the utilization constraint check. 
*/
class UnrelSeq {
public:

    UnrelSeq(const SwModelMat* _sw);
    ~UnrelSeq() {  
        unrelated_idx.clear();
    }

    // run this to restart the sequence, e.g., when evaluating a new task partition
    void reset_unrelated_idx();
    // write back a vector of uint32_t that represents the current unrelated set of each dag (bin encoded!)
    // return false when reaching the end of the sequence of unrelated sets
    // TODO: instead of making the cartesian product among the unrelated of each DAG, 
    // take the unrelated with max utilization of each DAG and sum these utilizations.
    // This is reduce the total number of checks.
    bool get_next_unrelated(uint32_t * next_unrelated);
    // return the number of unrelated sets considering all DAGs
    uint32_t get_n_sets() const { return n_sets;}
    // return the longest unrelated sets (i.e. the set with more tasks) considering all DAGs
    static uint8_t get_longest_set() { return UnrelSeq::s_longest_set;}
    // get the unrelated set in string format to be used in the minizinc model
    static string get_minizinc_str() {return UnrelSeq::s_minizinc_str;}
    // call it once to set the static string with the unrelated sets
    static void set_minizinc_str(const SwModelMat* sw);

private:
    // pointer to the sw model. it should be read-only, but DagCpp needs to write wcet into dag.c to get back the rel_deadline :/
    const SwModelMat* sw;
    // point to the current unrelated set
    vector<uint32_t> unrelated_idx;
    // when > 0, it means the sequencer reached the end of the seq
    uint8_t last_unrelated;
    // The first time the unrelated loop is run, it fills this variable with the number of 
    // sets considering all DAGs. This is used during minizinc generation.
    uint32_t n_sets;
    
    // Making these attributes STATIC because it is not trivial to set them. Repeating this computation
    // for every instance of this class would have a penalty. With STATIC, this is done only once and all 
    // instances can read from it.

    // The longest set of tasks in a unrelated set, which is equivalent to the # of 1 bits in the set.
    // It is also the sum of the longest unrelated of each dag.
    static uint8_t s_longest_set;
    // This attribute is set the first time the unrelated loop is run and read multiple times.
    // So, this methods returns the unrelated set in string format to be used in the minizinc 
    // model, run during the utilization check.
    // Make it a static variable to be shared among all instances of this class, which will occur in multithread
    static string s_minizinc_str;
};

#endif // UNREL_SEQ_H
