#ifndef FREQ_SEQ_H
#define FREQ_SEQ_H

#include <vector>
#include <cstdint>


#include <common.h>

using namespace std;

/*
   FreqSeq implements a logic to reduce the number of frequencies configurations
   evaluated by exploiting a so called 'dominance' aspect of the freq set.
   
   Let's assume that the frequencies of all islands are sorted in ascending order.
   Then, assuming an island with 3 different frequencies, it's safe to say that
   if a configuration using the 2nd freq is not feasible or does not provide the lowest 
   power so far, then the same configuration using the 1st freq will also not be feasible.

   Now, generalizing this to multiple islands where each island can have different number
   of frequencies, it's possible to model this 'dominance' relationship using a DAG. 
   For example, the freq set dominance for 3 islands and freq sizes of 1, 2, 3 frequencies is:
    012  <== the highest freq for all islands
   /    \ 
  002   011 
    \   /  \
     001   010
       \   /
        000  <=== the lowest freq for all islands

   Meaning, if configuration 011 if not viable, then the configurations 001, 010, and 000
   are also not viable. This frequency set modeling can potentially reduce the computation
   time, specially for hardware models with large number of frequency sets, i.e. several
   islands where each islands has several possible frequencies.
*/
class FreqSeq {
public:
    // TODO: skip and seqs are public only for testing purposes. 
    // mark which freq seq should be skipped
    // TODO: use bit_vector instead to use one bit per position
    // https://github.com/electronicarts/EASTL/blob/master/doc/BestPractices.md#consider-fixed-size-containers
    vector< bool > skip;
    // holds the generated freq sequence
    vector< vector<uint8_t> > seqs;

    FreqSeq(uint8_t _n_islands, uint8_t* n_freqs_per_island);

    ~FreqSeq() {
        seqs.clear();
        skip.clear();
        n_freqs_per_island.clear();
    }

    // generates the next frequency sequence and return false when the previously 
    // generated was the last
    bool next();
    // returns the current frequency sequence and the value of this ref is read-only
    const vector<uint8_t>* get() const;
    // must call it when starting to check a new placement, all skip attrib return to false
    // the placement [island] is encoded such that there is no distiction of DAG and each bit corresponds to a task
    // task 0 of dag 0 is the LSB. 
    void initiate(vector < uint32_t >* placement);
    // set the current frequency set, and all its lower freq sets, as not viable solution
    // by marking their 'skip' attribute. This means that the search algo can safely
    // avoid evaluating lower freq sets.
    // TODO: this function could be more efficient for larger freq sets 
    // by using a DAG instead of this linear search to represent the dominance 
    // aspect of the freq sets
    void not_viable();
    void dump() const;

private:
    // points to the current task mapping
    // TODO: use 'bit_vector' to encode the placement with ullimited number of tasks
    // https://github.com/electronicarts/EASTL/blob/master/doc/Modules.md
    vector < uint32_t >* curr_placement;
    // each position has the number of different frequencies per island
    vector < uint8_t > n_freqs_per_island;
    // points to the current frequency sequence
    uint32_t curr_freq;
    uint8_t n_islands;

    // populate seqs by sorting the frequency sets with deacreasing frequency
    void generate_sequence(uint32_t total_freqs);
    // set curr_freq to the 1st highest valid freq sequence. Since islands can 
    // be empty, the highest valid freq sequence is not always the 1 seq of the set.
    void highest_freqset();
};

#endif // FREQ_SEQ_H
