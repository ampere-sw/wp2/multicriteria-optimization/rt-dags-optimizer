#ifndef HW_MODEL_H
#define HW_MODEL_H

#include <vector>
#include <string>
#include <cstdint>

#include <yaml-cpp/yaml.h>

#include <common.h>

using namespace std;

class HwModel {
public:
    float capacity;
    uint32_t n_pus;
    // TODO: decide to use float or double at expense of performance loss in the vector unit
    // all these three vectors are sorted in ascending order of frequency
    vector<float> busy_power;
    vector<float> idle_power;
    vector<uint32_t> freqs;

    HwModel(){n_pus = 0; capacity = 0.0f;}
    ~HwModel() {  
        busy_power.clear();
        idle_power.clear();
        freqs.clear();
    }

    // node a YAML node representing an island
    void load(const string abs_path_dir, const YAML::Node node);
    void dump() const;

private:
    /* load busy_power, idle_power, freqs from the power CSV file*/
    void load_csv(const std::string *filename);

};

#endif // HW_MODEL_H