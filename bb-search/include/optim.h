#ifndef OPTIM_H
#define OPTIM_H

#include <cstdint>

#include <common.h>
#include <freq_seq.h>
#include <eval_cfg.h>

using namespace std;

class SwModelMat;
class HwModelMat;

/*
Implements the main optimization loops that evaluates the range of possible task placements 
(i.e. from _start_mapping until  _start_mapping+_n_mappings) against all frequency sets.
The 'best_*' attributes saves the configuration with the lowest power.
*/
class Optim {
public:

    Optim(const SwModelMat* _sw, const HwModelMat* _hw, const uint32_t _start_mapping, const uint32_t _n_mappings, const float _heuristic_power, const float _max_power);
    ~Optim() {  
        delete fSeq;
        delete evalCfg;
    }

    // evaluate the range of possible task placements (i.e. from _start_mapping until  _start_mapping+_n_mappings) against all frequency sets
    bool evaluate();
    // dump for programmers where task maping is printed in hex
    void dump();
    // a dump for human beings, not programmers :)
    void verbose_dump();
    // this is the output format used to compare the results from bb-search and dag.c
    string dump_summary();
    // the detailed optimization output
    string dump_yaml();
    float get_best_power() const { return best_power;}

private:
    // pointer to the sw model. read-only
    const SwModelMat* sw;
    // pointer to the hw model. read-only
    const HwModelMat* hw;
    // frequency set sequencer
    FreqSeq* fSeq;
    // main constraint checker 
    EvalCfg* evalCfg;
    // the current task to island mapping being evaluated
    uint32_t current_mapping;
    // number of mappings to be evaluated
    uint32_t n_mappings;

    // Lowest known power among all threads
    float best_power;
    vector <uint8_t> best_freq;
    uint32_t best_mapping;
    vector<uint32_t> best_mapping_per_island; // equivalent to best_mapping, but divided by island
    vector < vector<uint32_t> >  best_mapping_for_eval; // equivalent to best_mapping, but divided by island and by dag

    // the highest power assuming all tasks placed on high capacity island at the highest freq
    // this is used to check if this can be used as a upper bound
    float max_power;

    // aux attributes initialized once and used to convert uint32_t current_mapping into vector of mapping divided per island
    vector<uint32_t> mapping_mask;
    vector<uint32_t> starting_task_idx;

    // generates different representations of a mapping of tasks onto islands.
    // The input is this->current_mapping and it generates 2 representations:
    //  - mapping_per_island: the mapping is split among the islands. Used bu FreqSeq
    //  - mapping_per_island: the mapping is split among the islands/dags. Used be EvalCfg.
    // TODO: unify the representation once figure out which is the best
    void decode_mapping(vector<uint32_t>* mapping_per_island, vector < vector<uint32_t> >* mapping_for_eval);

    void print_mapping_freq(float power, vector<uint32_t>* mapping, vector <uint8_t>* freq);
};

#endif // OPTIM_H