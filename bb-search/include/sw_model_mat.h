#ifndef SW_MODEL_MAT_H
#define SW_MODEL_MAT_H

/*
* A vectorization and cache friendly version of the software model
TODO: not actually usefull since i gave up vectorization. in the future it might be better to merge this class with SwModel.
*/

#include <cstdint>

using namespace std;

#include <common.h>
#include <sw_model.h>

class SwModelMat {
public:
    // data from the software model are duplicated such that dummy tasks are removed and the 
    // memory has no gaps for vectorization
    //float * task_wcet_ref_ns;
    // task_wcet_delta = abs(wcet_ref-wcet_ref_ns)
    //float * task_wcet_delta;

    //uint8_t n_islands;

    // intermediate data structure which is not actually used in the main optimization loop
    // TODO: make it a base class instead of an attribute
    SwModel sw_model;

    SwModelMat(int islands, const char* filename);
    ~SwModelMat() {  
        //delete[] task_wcet_ref_ns;
        //delete[] task_wcet_delta;
    }

    //void dump() const;

private:

};

#endif // SW_MODEL_MAT_H