#ifndef SW_MODEL_H
#define SW_MODEL_H

#include <vector>
#include <string>
#include <cstdint>

#include <yaml-cpp/yaml.h>

#include <dag_cpp.h>
#include <common.h>

using namespace std;

typedef struct{
    // wcet considering the reference frequency. this part scales with frequency changes
    float wcet_ref;
    // wcet considering the reference frequency. this part DOES NOT scale with frequency changes.
    // For example, program with lots of access to the main memory
    // 'reference frequency' is the highest freq of the current island
    float wcet_ref_ns;
} Task;

typedef struct {
    float activation_period;
    float deadline;
    vector< Task >  tasks;
    // list duplication because it is populated only once and read multiple times.
    // i want to make sure that i can change/optimize this list without breaking/changing the original dag.c
    // There are also other small optimizations that could be done, like ordering the set as deacresing utilization.
    vector <uint32_t> unrelated;
    // The graph is only used to build the minizinc model and to build the unrelated list
    vector<tuple<uint8_t,uint8_t>>  edge_list;
} Dag;

class SwModel {
public:
    vector<Dag> dags;

    SwModel(){}
    ~SwModel() {  
        tom_dag.clear();
        dags.clear();
    }

    // load the YAML file representing the list of tasks and list of dags
    void load(const char* filename);
    void dump() const;

    // the sum of all tasks from all dags
    uint32_t get_n_actual_tasks() const;

    // returns true if the dag deadline is obeyed and write back the relative deadline of each task in rel_deadline
    // When ignore_bounds is true, it wont stop when a violation is found
    bool get_rel_deadline(const vector < vector <float> >* wcet, vector < vector <float> >* rel_deadline, bool ignore_bounds=false) const;

private:
    // Tommaso's DAG, used only to calculate the unrelated tasks and relative deadlines
    vector<DagCpp> tom_dag;
};
#endif // SW_MODEL_H
