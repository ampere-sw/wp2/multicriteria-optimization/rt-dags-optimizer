#ifndef HW_MODEL_MAT_H
#define HW_MODEL_MAT_H

/*
* A vectorization and cache friendly version of the hardware model
TODO: in the future it might be better to merge this class with HwModel.
*/

#include <vector>
#include <cstdint>

#include <yaml-cpp/yaml.h>

using namespace std;

#include <common.h>
#include <hw_model.h>

class HwModelMat {
public:
    // TODO: are these attributes required ?
    uint8_t n_islands;

    // intermediate data structure which is not actually used in the main optimization loop
    vector<HwModel> islands;


    HwModelMat(const char* filename);
    ~HwModelMat() {  
        islands.clear();
    }

    void dump() const;

private:

};

#endif // HW_MODEL_MAT_H