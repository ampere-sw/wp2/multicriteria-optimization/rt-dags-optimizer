#ifndef CHECK_OVERFLOW_H
#define CHECK_OVERFLOW_H


class SwModelMat;
class HwModelMat;

/*
It performs two tests:
 1) Assume all tasks are placed onto the lowest capacity island at its lowest frequency
 to cause the highest possible wcet. If this configuration does not cause a wcet or a path deadline overflow, than this configuration is safe 
 2) Assume all tasks are placed onto the highest capacity island at its highest frequency
 to cause the highest possible power. If this configuration does not cause a power overflow, than this configuration is safe. 

Return true if error occured.
Since it calculates the min/max power anyways, return this value to be used upper bound.
*/
bool check_overflow(const SwModelMat* _sw, const HwModelMat* _hw, float* min_power, float* max_power);

#endif // CHECK_OVERFLOW_H
