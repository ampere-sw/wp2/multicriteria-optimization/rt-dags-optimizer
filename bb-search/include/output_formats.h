#ifndef OUTPUT_FORMATS_H
#define OUTPUT_FORMATS_H

#include <vector>
#include <cstdint>

#include "hw_model_mat.h"
#include "sw_model_mat.h"

using namespace std;

/* Here is an example of the expected format
COMPACT OUTPUT FORMAT:
Power: 1.933354 W
Island 0 (1400 Hz): 0,1,0,1,0,0,1,1,1,
Island 1 (1600 Hz): 1,0,1,0,1,1,0,0,0,
*/
string compact_output_format(const HwModelMat* hw, const SwModelMat* sw, 
    const vector < vector < uint32_t > >  *placement,
    vector <uint8_t> *freqs, const float power);

/* This part of the yaml file has to be filled in by the optimization tool main function
tool_name: "heur1"
platform_name: "odroid-xu3"
platform_filename: ".../odroid-xu3.yaml"
dag_filename: ".../ex1-sw.yaml"
*/
/* Here is an example of the expected format
n_dags: 2
power: 1.23
islands:
    - capacity: 0.3
      freq: 1200
      tasks: [(0,0),(0,2),(1,0)] // tuple format (dag,task)
      pus:
        - []
        - [(0.4,0,0),(0.2,0,2] // tuple format (utilization, dag, task)
        - []
        - [(0.5,1,0)]
    - capacity: 1.0
      freq: 500
      tasks: [(0,1),(1,1)]
      pus:
        - [(0.3,0,1)]
        - [(0.5,1,1)]
        - []
        - []
tasks:
    - id: (0,0)
      wcet: 1234
      deadline: 1500
    - id: (0,1)
      wcet: 1000
      deadline: 1500
    - id: (0,2)
      wcet: 1000
      deadline: 1200
    - id: (1,0)
      wcet: 500
      deadline: 1000
    - id: (1,1)
      wcet: 500
      deadline: 1000
*/
void detailed_yaml_output_format(const string & detailed_output,
    const string & tool_name,
    const string & plat_name, const string & plat_filename,
    const string & dag_filename, unsigned long exec_time_us,
    const string & output_yaml);
#endif
