#ifndef EVAL_CFG_H
#define EVAL_CFG_H

#include <cstdint>

#include <unrelated_seq.h>
#include <common.h>

using namespace std;

class SwModelMat;
class HwModelMat;

/*
Given a task placement onto islands, a frequency set for each island, and the current best power, 
return: 
    - the power of this configuration or 
    - true/false if this configuration is feasible
*/
class EvalCfg {
public:

    // if ignore_bounds == true it will continue the evaluation even if bounds are violated
    EvalCfg(const SwModelMat* _sw, const HwModelMat* _hw, bool _ignore_bounds = false);
    ~EvalCfg() {
        wcet.clear();
        rel_deadline.clear();
        delete uSeq;
    }

    // return false if not feasible or the power of this configuation if not better
    // _placement is a matrix [islands][dags] where each bit of the 32bit word corresponds to a task
    // _freqs is a vector [islands] with the index value of the vector of frequencies to each island
    // _curr_best_power is the initial bound in power
    // _calculated_power is an output of the power found in this configuration
    bool evaluate(const vector < vector < uint32_t > >* _placement, const vector < uint8_t>* _freqs, float _curr_best_power, float* _calculated_power);
    // print out the results of this configuration, which are: 
    // - wcet and power;
    // - relative deadline;
    // - task placement onto PUs and task/pu utilization
    void dump();
    // dump all internal results in a yaml format comonly used by all the optimization tools
    string dump_yaml();


    // configuration dumping assuming the 2 kinds of config encoding
    void dump_freq_placement2(float power, const vector < vector < uint32_t > >* _placement, const vector < uint8_t>* _freqs);
    void dump_freq_placement(float power, const vector < uint32_t >* _placement, const vector < uint8_t>* _freqs);

    // used by the heuristics to get access to the internally generated wcet and deadline. read only
    const vector < vector <float> > * get_wcet_ptr(){return &wcet;}
    const vector < vector <float> > * get_deadline_ptr(){return &rel_deadline;}

private:
    // pointer to the sw model. it should be read-only, but DagCpp needs to write wcet into dag.c to get back the rel_deadline :/
    const SwModelMat* sw;
    // pointer to the hw model. read-only
    const HwModelMat* hw;

    // references to the current configuration under evaluation
    const vector < vector < uint32_t > >* placement;
    const vector < uint8_t >* freqs;
    float curr_best_power;

    // set it to true to continue the checks even if some bound is violated 
    bool ignore_bounds;

    // those next attribute are storage area for the intermediate results, such that 'sw' is not written

    // wcet for each task of eack dag for this current configuration
    vector < vector <float> > wcet;
    // relative deadline for each task of eack dag for this current configuration
    vector < vector <float> > rel_deadline;
    // the power obtained in this evaluation
    float current_power;
    // pu X tuple <utilization,dag,task> mapped to this pu. used only for debug
    // indexes are [island][pu][task_tuple]
    // This is filled by pu_task_placement()
    vector < vector < vector < tuple<float, uint32_t, uint32_t > > > > task_placement;
    // return false is the current configuration is not better than the 'curr_best_power'
    // the configuration power is written in _calculated_power
    // conventional implementation - no vectorization
    bool eval_wcet_power(float * _calculated_power);
    // return false is the current configuration does not respect the deadline of one of the DAGs
    bool eval_rel_deadline();
    // return false is the current configuration does not respect the PU utilization constraint.
    // it does not save task placement onto PUs to make it simpler and faster  
    bool eval_utilization();

    // place the tasks onto PUs, not only onto islands
    // this function is only called to generate the final output. It is not called in the opimization loop.
    // this procedure does not take into accound dag precedence or unrelated sets.
    void pu_task_placement();
    
    // aux methods for checking the utilization

    // Generates the datafile required to run the function 'optimal_placement'
    // The generated model is such that all nodes are included into the model 
    // instead of only the ones assigned to the island. This way, it's easier 
    // to check the precedence constraints. However, the utilization is zero
    // when the task is not mapped to the target island.
    // Return 1 when the generation is ok, 0 to abort the minizinc execution
    int build_minizinc_model(uint8_t island, FILE* fp);
    bool run_minizinc(uint8_t island);
    // generates the sequence of unrelated sets among all DAGs
    UnrelSeq* uSeq;

    void dump_freq(const vector < uint8_t>* _freqs, char *str_out, const unsigned str_out_size);

    void placement_to_str(uint32_t placement, char *out_str, const uint32_t out_str_size, uint32_t *offset);

};

#endif // EVAL_CFG_H
