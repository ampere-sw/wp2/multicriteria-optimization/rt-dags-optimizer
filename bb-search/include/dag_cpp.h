#ifndef DAG_CPP_H
#define DAG_CPP_H

#include <vector>
#include <cstdint>
#include <tuple>

#include <yaml-cpp/yaml.h>

// Tommaso's graph lib
extern "C" {
    #include <dag.h>
}

using namespace std;

/*
   a C++ wrapper around Tommaso's C code to build DAGs for the software model.
*/
class DagCpp {
public:

    DagCpp(const YAML::Node dag_node);
    ~DagCpp(){
        dag_cleanup(p_dag);
        //free(p_dag);
    }

    void dump() const { dag_dump(p_dag); }
    void dump_dot(char *fname) const { dag_dump_dot(p_dag,fname); }
    void dump_unrelated() const { dag_dump_unrelated(p_dag); }
    void copy_unrelated_tasks( vector <uint32_t> & unrelated );
    // compute relative deadline and its aux member functions
    void set_rel_deadline() const {dag_set_deadlines(p_dag, nullptr, p_dag->first, p_dag->last, p_dag->deadline, 1); }
    void set_wcet(uint8_t task, const float value) const {
        // we skip the 1st dummy tasks
        p_dag->elems[task].C = value;
        }
    float get_deadline(uint8_t task) const { return p_dag->elems[task].d; }
    float get_longest_path() const;
    void print_longest_path() const;

private:
    dag_t *p_dag;
    void compute_unrelated(dag_t *pdag) { dag_comp_unrelated(pdag); }

}; 

#endif // DAG_CPP_H