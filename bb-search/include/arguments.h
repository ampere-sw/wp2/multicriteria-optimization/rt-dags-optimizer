#ifndef ARGUMENTS_H
#define ARGUMENTS_H

#include <vector>
#include <string>

using namespace std;

// common argument parsing for all heuristics
void parse_arguments(int argc, char* argv[], string &dag_filename,
    string &plat_name, string &plat_filename, string &output_yaml, float* initial_power);

#endif