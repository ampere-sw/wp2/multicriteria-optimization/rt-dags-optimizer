#ifndef CHECK_YAML_H
#define CHECK_YAML_H

#include <vector>
#include <tuple>
#include <cstdlib>

using namespace std;

// data structure to load the hw definition in memory for sorting. later, this is not used anymore
typedef struct{
    float capacity;
    uint32_t n_pus;
    vector < tuple <float,float,float> > power_data; // freq, busy_power, idle_power
} Hw_t;


bool check_yaml(const char* sw_filename, const char* hw_filename);

// return in hw_def the entire islands definition sorted by ascending capacity
void sort_islands(const string abs_path, vector < Hw_t >* hw_def, YAML::Node hw, bool ascending);

#endif // CHECK_YAML_H
