
# BB-SEARCH

This is a optimization tool, based on branch-and-bound approach, that searches for a feasible placement of real-time tasks onto processing islands of a heterogeneous computing platform (.e.g like ARM's big-Little architecture) while minimizing its power consumption. The *main motivation* to build this tool is that it would not provide an optimal solution as the LP-based *../../dag*, but it would be faster and more scalable to larger problem instances. 

Given a set of real-time tasks modeled as a DAG and end-to-end DAG deadlines, this tool assigns each task onto an island and sets the minimum frequency to each island such that :
  - The DAG deadlines are obeyed;
  - Each processing unit of all islands has a utilization lower than DEF_MAX_UTIL, which is 0.95;
  - power consumption is minimized.

## Building the tool and requirements

The required dependencies:
  - yaml-cpp: install it with *sudo apt install libyaml-cpp-dev*;
  - Ubuntu 18.04 or 20.04 with their default GCC compilers;
  - CMake: version 3.23.2 was used, but it probably works with older versions too .

The optional dependencies:
  - [minizinc](https://www.minizinc.org/software.html) version 2.6.1 (disabled by default) for a sub-problem optimization mentioned next (Currently not tested!!!).

The optional dependencies for developers only:
  - Sanitizers and static checkers like clang, cpp-checker, PVS;
  - ccmake: install it with *sudo apt install cmake-curses-gui*

Compiling with the default settings:

```
$ cd bb-search; mkdir build; cd build
$ cmake ..
$ make -j 8
```

Use *ccmake ..* in the build to change the default compilation settings.

## Usage

```
bb-search, rev 8bdc9b3, 2021, ReTiS Laboratory, Scuola Sant'Anna, Pisa, Italy
Last commit time: Wed Aug 3 21:33:46 2022 +0200
Usage: bb-search <SW YAML file> <HW YAML file> [options...]
  Options:
    -h|--help ................ Show this help message and exit
    -oyaml filename.yaml .... Specify destination filename for the the optimization output
    -ip power ............... Specify the upper bound for power
```

It takes *two mandatory YAML input files*: 
 - The software workload describing the DAGs deadline and the tasks WCET;
 - The hardware platform, including the characteristics of each island, and a pre-processed platform power profile. It a YAML file describing a custom platfform or it can be one of the built-in supported plataform names: odroid-xu3 or odroid-xu3-half.

The optional arguments are:
 - ip: is an initial power used as a lower bound, potentially reducing the search space and execution time. Typically, the user can run faster, but less optimized, heuristics to obtain this initial solution;
 - oyaml: filename of the output YAML file. 

### Software workload file

This YAML file describes a set of DAGs where each dag has two attributes (activation_period, deadline), a set of tasks, and an edge list connecting the tasks. Each task has two attributes (wcet_ref, wcet_ref_ns). *wcet_ref_ns* represents the WCET of the non-scalable part of the task, which is typically the task computation time spent that does not scale with the frequency. *wcet_ref*, on the other hand, represents the WCET scalable part of the task.
For example, memory-bounded tasks spend a large proportion of their execution time in memory accesses, but this part does not scale with the island frequency. This time proportion is assinged to wcet_ref_ns. Each task must be profiled on the target platform running on the platform highest capacity island at its highest frequency. 

Here is a simplified example describing a workload of a single DAG:

```yaml
dags:
  - activation_period: 100
    deadline: 100
    tasks:
      - wcet_ref: 0 
        wcet_ref_ns: 0
      - wcet_ref: 80 
        wcet_ref_ns: 3
      ...
    edge_list:
     [
      [0,1],
      [1,2],
      ...
     ]
```

### Hardware platform file

This file described the target hardware platform, its processing islands and power profiling.
Here is an example for the ODROID-XU3 board, with two islands of 4 processing units each:

```yaml
hw:
  - capacity: 1.0
    n_pus: 4
    power_file: "odroid-xu3-big.csv"
  - capacity: 0.526
    n_pus: 4
    power_file: "odroid-xu3-little.csv"
```

Each island has a capacity, as specified in the kernel device tree. For exynos5422 devices, the capacity information comes from [here](https://patchwork.kernel.org/project/linux-renesas-soc/patch/20170830144120.9312-4-dietmar.eggemann@arm.com/#20933539). Each island has a CSV file with three columns: frequency (Hz), busy power (W), and idle power (W). Please refer to the following paper for more information about how the power profiling of a platform is done. This other [document](./data/readme.md) describes how the platform YAML, with power profiling data, was generated. 

```bibtex
@article{mascitti2021dynamic,
  title={Dynamic partitioned scheduling of real-time tasks on ARM big. LITTLE architectures},
  author={Mascitti, Agostino and Cucinotta, Tommaso and Marinoni, Mauro and Abeni, Luca},
  journal={Journal of Systems and Software},
  volume={173},
  pages={110886},
  year={2021},
  publisher={Elsevier}
}
```

### Output YAML file format

The tool generates tha output also in YAML format. Here is an example:

```
tool_name: "bb-search"
platform_name: "odroid-xu3"
platform_filename: ".../power-aware-optimization/heuristics/bb-search/data/odroid-xu3.yaml"
dag_filename: ".../power-aware-optimization/dag-examples/small-dags/small000.yaml"
execution_time_us: 5649
n_dags: 1
power: 1.700337
islands:
    - capacity: 0.526000
      frequency: 1200
      tasks: [
                [0,1],
             ]
      pus:
              - [
                 [0.947121,0,1],
                ]
              - [
                ]
              - [
                ]
              - [
                ]
    - capacity: 1.000000
      frequency: 1900
      tasks: [
                [0,0],
                [0,2],
             ]
      pus:
              - [
                 [0.947121,0,0],
                ]
              - [
                 [0.947121,0,2],
                ]
              - [
                ]
              - [
                ]
tasks:
    - id: [0,0]
      wcet: 38806440.000000
      deadline: 40973040.000000
    - id: [0,1]
      wcet: 3668136.000000
      deadline: 3872931.500000
    - id: [0,2]
      wcet: 33295126.000000
      deadline: 35154024.000000
```

## Examples

```
$ cd bb-search/data 
$ bb-search ex1.sw.yaml odroid-xu3-hw.yaml
```

This example runs for a software workload of two DAGs, summing up 9 tasks, onto ODROIID-XU3.
The result is :

```
Power: 1.901540 W
Island 0 (1300.00 Hz): 0,1,1,1,1,1,0,0,0,
Island 1 (1600.00 Hz): 1,0,0,0,0,0,1,1,1,
```

It informs the minimal power obtained, the frequency each island must be set, and on which island each task must be placed. 

## Main assumptions

 - It follows the modeling definitions found in *../model* directory;
 - It uses worst-fit heuristic (not optimal) to check the processing unit utilization constraint of tasks placed on an island. Optionally, it is possible to enable minizinc (see *ENABLE_MINIZINC* at [CMakeLists.txt](./CMakeLists.txt)) to solve the same problem, leading to optimal results but at a significant increase in execution time;
 - It uses a deadline splitting strategy to split the DAG deadline into task deadline where each task receives a deadline proportional to its WCET. Also not an optimal solution for deadline splitting;

## Main compilation parameters

The compilation parameters are located in two places:

 * [CMakeLists.txt](./CMakeLists.txt): parameters starting with *ENABLE_\**;
 * [common.h](../common/include/common.h): parameters related to the size of internal data structures. You will only need to change it if you have some odd sized problem to compute.
 * [log.h](../common/include/log.h): log macros used to control the verbosity via *LOG_LEVEL* define. When compiled with the max verbosity, it runs additional runtime checks that affect the performance. 

## Profiling

There are two compilation parameters that can be enabled with CMake that give a better insight of internals performance: 
 
 - *ENABLE_PROFILING*: Enable visual profiling for chrome tracing;
 - *ENABLE_TIMMING_MEASUREMENTS*: Enable timing measurements of key parts of the code.

### To be used in the future - developers only

 - https://developers.redhat.com/blog/2021/05/05memory-error-checking-in-c-and-c-comparing-sanitizers-and-valgrind
 - https://stackoverflow.com/questions/7664788/freopen-stdout-and-console
 - https://stackoverflow.com/questions/7400418/writing-a-log-file-in-c-c
 - trace: https://stackoverflow.com/questions/3379193/trace-system-function-call-in-c
 - memory tracing per class - custom new, delete: https://medium.com/swlh/tracing-code-in-c-fd9470e3bf5
 - perf: https://www.brendangregg.com/linuxperf.html
 - https://www.brendangregg.com/ebpf.html#bpftrace

## TO be done

The source code is tagged with *TODO* to indicate where optimization could be applied. In addition, there are these key enhancements to be done:

 - [] implement multi-thread version. The search optimization is *embarrassingly parallel* with tiny shared data (just the best power found so far) among these threads;
 - [] enable the user to specify how the power is calculated because this typically changes from platform to platform;
 - [] more software tests;

## Contributions

  Did you find a bug ? Do you have a new example to add ? Do you have some extensions or updates to add ? Please send me a Pull Request.

## Authors

 - Alexandre Amory (June 2022), [Real-Time Systems Laboratory (ReTiS Lab)](https://retis.santannapisa.it/), [Scuola Superiore Sant'Anna (SSSA)](https://www.santannapisa.it/), Pisa, Italy.

## Funding
 
This software package has been developed in the context of the [AMPERE project](https://ampere-euproject.eu/). This project has received funding from the European Union’s Horizon 2020 research and innovation programme under grant agreement No 871669.
