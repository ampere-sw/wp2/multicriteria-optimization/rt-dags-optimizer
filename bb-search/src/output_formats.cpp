
#include <cstdio>
#include <cassert>
#include <fstream>
#include <iostream>

#include "output_formats.h"

string compact_output_format(const HwModelMat* hw, const SwModelMat* sw,
    const vector < vector < uint32_t > >  *placement,
    vector <uint8_t> *freqs, const float power){

    string output_str;
    output_str +="COMPACT OUTPUT FORMAT:\n";
    output_str +="Power: "+to_string(power)+" W\n";
    uint8_t i,d;
    int t;

    // combine the placement per island/dag into placement per island only
    vector < uint32_t > placement_per_island(hw->n_islands);
    uint32_t initial_task;
    for (i=0;i<hw->n_islands;++i){
        initial_task = 0;
        for(d=0;d<sw->sw_model.dags.size();++d){
            placement_per_island[i] |= (*placement)[i][d] << initial_task;
            initial_task += sw->sw_model.dags[d].tasks.size();
        }
    }

    const unsigned n_tasks = sw->sw_model.get_n_actual_tasks();
    for (i=0;i<hw->n_islands;++i){
        output_str +="Island "+to_string(i)+" ("+to_string(hw->islands[i].freqs[(*freqs)[i]])+" Hz): ";
        for(t=n_tasks-1;t>=0;--t){
            if (placement_per_island[i] & (1<<t)){
                output_str +="1,";
            }else{
                output_str +="0,";
            }
        }
        output_str +="\n";
    }
    return output_str;
}

void detailed_yaml_output_format(const string & detailed_output,
    const string & tool_name,
    const string & plat_name, const string & plat_filename,
    const string & dag_filename, unsigned long exec_time_us,
    const string & output_yaml){

    string complete_output;
    /* complement the output yaml with more information from the main() context
    tool_name: "dag.c"
    platform_name: "odroid-xu3"    
    platform_filename: xxxxxx
    dag_filename: xxxxxx
    */
    complete_output += "tool_name: \"" + tool_name + "\"\n";
    complete_output += "platform_name: \"" + plat_name + "\"\n";
    complete_output += "platform_filename: \"" + plat_filename + "\"\n";
    complete_output += "dag_filename: \"" + dag_filename + "\"\n";
    complete_output += "execution_time_us: " + to_string(exec_time_us) + "\n";
    complete_output += detailed_output;

    cout << "DETAILED OUTPUT FORMAT:\n";
    cout << complete_output << endl;
    if (output_yaml != ""){
        fstream f;
        f.open(output_yaml,ios::out);
        if (!f.is_open()) { 
            cerr << "ERROR: File '" << output_yaml << "' did not open";
            exit(1);             
        }
        f << complete_output << endl;
        f.close();
    }
}

