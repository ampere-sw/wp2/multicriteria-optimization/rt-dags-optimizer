#include <iostream>
#include <vector>
#include <bits/stdc++.h> // reverse

#include <freq_seq.h>
#include <Instrumentor.h>

FreqSeq::FreqSeq(uint8_t _n_islands, uint8_t* _n_freqs_per_island){
    assert (_n_islands>0);
    n_islands = _n_islands;
    n_freqs_per_island.resize(n_islands);
    for(uint8_t i=0;i<n_freqs_per_island.size();i++){
        n_freqs_per_island[i] = *_n_freqs_per_island;
        _n_freqs_per_island++;
    }
    uint32_t total_freqs = n_freqs_per_island[0];
    for(uint8_t i=1;i<n_freqs_per_island.size();i++){
        total_freqs *= n_freqs_per_island[i];
    }
    // to force the API user to call 'initiate' before using it
    curr_placement = NULL;
    // populate seqs with deacresing frequency sets
    generate_sequence(total_freqs);
    cout << endl << "Number of Frequency sets: " << total_freqs << endl << endl;
}

bool FreqSeq::next(){
    //TIMER_SCOPE("FreqSeq::next()");
    PROFILE_FUNCTION();
    uint8_t j;
    uint32_t i;
    bool skip_candidate;
    bool found = false;
    // must call 'initiate'  before using it
    assert (curr_placement!=NULL);
    // Find the next valid freq set or reach the end of the search
    // There are 2 situations where a freq set should be discarded: 
    for(i=curr_freq+1;i<seqs.size();i++){
        // 1) check whether this node should be skiped by dominance rules
        if (skip[i]){
            continue;
        }
        // 2) skip candidate solutions where an island placement is empty but the frequency is not the minimal.
        // It means that this candidate is "obviously" not an optimal one
        skip_candidate = false;
        for(j=0;j<n_islands;j++){
            if ((*curr_placement)[j] == 0 && seqs[i][j] != 0){
                skip_candidate = true;
                break;
            }
        }
        if (skip_candidate)
            continue;
        // if it reaches to this point, then this is a good freq set
        found = true;
        curr_freq = i;
        break;
    }    
    // reached the end of the sequence. reinitialization is mandatory
    if (!found){
        curr_placement=NULL;
    }
    return found;
}

const vector<uint8_t>* FreqSeq::get() const {
    // must call 'initiate'  before using it
    assert (curr_placement!=NULL);
    return &(seqs[curr_freq]);
}

void FreqSeq::initiate(vector < uint32_t >* placement){
    //TIMER_SCOPE("FreqSeq::initiate");
    PROFILE_FUNCTION();
    uint32_t i;
    for(i=0;i<skip.size();i++){
        skip[i] = false;
    }
    curr_placement = placement;
    // set curr_freq to the first valid sequence, which might not be at zero.
    // this happens when the placement has an empty island
    highest_freqset();
}

void FreqSeq::not_viable(){
    PROFILE_FUNCTION();
    // must call 'initiate'  before using it
    assert (curr_placement!=NULL);
    uint8_t j;
    uint32_t i;
    bool nviable;
    skip[curr_freq] = true;
    for(i=curr_freq+1;i<seqs.size();i++){
        nviable = true;
        // all values must be <= to be able to safely assume that this freq set can be skipped
        for(j=0;j<n_islands;j++){
            if (seqs[i][j] > seqs[curr_freq][j]){
                nviable = false;
                break;
            }
        }
        if (nviable)
            skip[i] = true;
    }
}

void FreqSeq::dump() const{
    // must call 'initiate'  before using it
    assert (curr_placement!=NULL);

    cout << endl << "FreqSeq:" << endl;
    for(uint32_t j=0;j<seqs.size();j++){
        for (auto const& value : seqs[j]){
            cout << (uint32_t)value << ", ";
        }
        cout << endl;
    }
}

/*
 TODO: the freqset order is not the ideal one. See test # 5 in freq_seq_test.cpp for more information.
Assume 3 islands w 3, 2, and 1 freqs:
The expected order is {{2,1,0},{1,1,0},{0,1,0},{2,0,0},{1,0,0},{0,0,0}}
but got  {{2,1,0},{1,1,0},{2,0,0},{0,1,0},{1,0,0},{0,0,0}}

Not a big issue. It will, probably, cause some slowdown in hw models with large freqsets
*/
void FreqSeq::generate_sequence(uint32_t total_freqs){
    uint8_t i;
    uint32_t j;
    // uint32_t infinite_loop_cnt=0;
    bool stop=false;
    // points to the last incremented island
    uint8_t inced = 0;
    // start with all islands using their respective minimal frequencies
    vector<uint8_t> freqs_per_island_idx(n_islands,0);
    seqs.push_back(freqs_per_island_idx);

    do{
        inced = 0;
        for(i=0;i<n_islands;i++){
            // if island idx i is not pointing to its max freq, then point to the next higher freq of this island
            if (freqs_per_island_idx[i] < n_freqs_per_island[i]-1){
                freqs_per_island_idx[i] ++;
                inced = i;
                break;
            }else{
                //  if this is not the last island, then go to the next island to increment its freq
                if (i >= n_islands-1){
                    stop = true;
                }
            }
        }
        // if all freqs are at their maximal value, then stop
        if (stop)
            break;
        // all island before the last incremented island must start over at their lowest freq
        for(i=0;i<inced;i++){
            freqs_per_island_idx[i] = 0;
        }
        seqs.push_back(freqs_per_island_idx);
        total_freqs--;
    } while (total_freqs>0);
    // want to get have the maximal freq for each island 1st, to prune the search space faster
    reverse(seqs.begin(), seqs.end());
    // temporaly set this to zero, but the highest valid freqset can only be known when the placement is set.
    curr_freq = 0;
    skip.resize(seqs.size());
    for(j=0;j<skip.size();j++){
        skip[j] = false;
    }
}

void FreqSeq::highest_freqset(){
    bool skip_candidate;
    uint8_t j;
    uint32_t i;
    // must call 'initiate'  before using it
    assert (curr_placement!=NULL);

    // start the seach from the 1st seq
    for(i=0;i<seqs.size();i++){
        // skip candidate solutions where an island placement is empty but the frequency is not the minimal.
        // It means that this candidate is "obviously" not an optimal one
        skip_candidate = false;
        for(j=0;j<n_islands;j++){
            if ((*curr_placement)[j] == 0 && seqs[i][j] != 0){
                skip_candidate = true;
                break;
            }
        }
        if (skip_candidate)
            continue;
        // if it reaches to this point, then this is a good freq set
        curr_freq = i;
        break;
    }    
}
