#include <iostream>
#include <string>
#include <cassert>
#include <algorithm>
#include <experimental/filesystem>

#ifdef __cpp_lib_filesystem
    #include <filesystem>
    using fs = std::filesystem;
#elif __cpp_lib_experimental_filesystem
    #include <experimental/filesystem>
    namespace fs = std::experimental::filesystem;
#else
    #error "no filesystem support ='("
#endif

#include <yaml-cpp/yaml.h>

#include <hw_model_mat.h>

HwModelMat::HwModelMat(const char* filename){
    uint32_t i;
    YAML::Node hw_config = YAML::LoadFile(filename);
    // extract the absolut path from the hw yaml file. This is used later to load the CSV files
    auto abs_path = fs::absolute(filename);
    abs_path = abs_path.parent_path();
    this->islands.resize(hw_config["hw"].size());

    for (i=0;i<hw_config["hw"].size();i++){
        YAML::Node node = hw_config["hw"][i];
        this->islands[i].load(abs_path.u8string(), node);
    }

    // sort the Islands by capacity
    sort(islands.begin(), islands.end(),
          [](HwModel const & a, HwModel const & b) -> bool
          { return a.capacity < b.capacity; } 
    );

    for (i=0;i<hw_config["hw"].size();i++){
        cout << "Island " << i << endl;
        islands[i].dump();
    }
    // both islands and freqs are sorted in ascending order
    this->n_islands = (uint8_t)islands.size();
}

void HwModelMat::dump() const{
    cout << "HwMat:" << endl;

}
