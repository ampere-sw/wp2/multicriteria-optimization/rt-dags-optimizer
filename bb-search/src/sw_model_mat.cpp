#include <iostream>
#include <vector>
#include <string>
#include <set>

#include <sw_model_mat.h>

SwModelMat::SwModelMat(int islands, const char* filename){
    // load the YAML file
    sw_model.load(filename);
    sw_model.dump();

}

