#include <experimental/filesystem>
#include <iostream>
#include <fstream>
#include <string>
#include <algorithm>

#include <hw_model.h>

#ifdef __cpp_lib_filesystem
    #include <filesystem>
    using fs = std::filesystem;
#elif __cpp_lib_experimental_filesystem
    #include <experimental/filesystem>
    namespace fs = std::experimental::filesystem;
#else
    #error "no filesystem support ='("
#endif

#include <yaml-cpp/yaml.h>


void HwModel::load(const string abs_path_dir, const YAML::Node node){
    string fname;

    this->capacity = node["capacity"].as<float>();
    this->n_pus = node["n_pus"].as<uint32_t>();
    fname = node["power_file"].as<string>();
    // the CSV is supposed to be in the same directory as the yaml file
    fname = abs_path_dir +  string("/") + fname;
    load_csv(&fname);
}

void HwModel::dump() const{
    cout << " - capacity: " << this->capacity << endl;
    cout << " - n_pus: " << this->n_pus << endl;
    cout << " - power:" << endl;
    for(uint32_t i=0;i<this->freqs.size();i++){
        cout << "   - " << this->freqs[i] << ", " << this->busy_power[i]
            << ", " << this->idle_power[i] << endl;
    }
}

/* 
https://stackoverflow.com/questions/18155487/reading-a-particular-line-in-a-csv-file-in-c
*/
void HwModel::load_csv(const std::string *filename){
    vector<tuple<uint32_t, float, float> > v;
    uint32_t i,freq;
    string line, item;
    float busy, idle;

    // load the CSV into v vector. Next, it is sorted
    ifstream myfile (*filename);
    if (myfile.is_open()) {
        // ignore the 1st line
        getline(myfile,line);
        while (getline(myfile,line)) {
                istringstream myline(line);
                getline(myline, item, ',');
                freq = stoi(item);
                getline(myline, item, ',');
                busy = stof(item);
                getline(myline, item, ',');
                idle = stof(item);
                v.push_back(make_tuple(freq,busy,idle));
        }
        myfile.close();
    }

    // resize the vectors according to the number of lines of the CSV 
    this->freqs.resize(v.size());
    this->busy_power.resize(v.size());
    this->idle_power.resize(v.size());
    // sort by the first element of the tuple, which is the frequency
    sort(v.begin(), v.end());
    // copy data back to the class
    for(i=0;i<v.size();i++){
        this->freqs[i]      = get<0>(v[i]);
        this->busy_power[i] = get<1>(v[i]);
        this->idle_power[i] = get<2>(v[i]);
    }
}
