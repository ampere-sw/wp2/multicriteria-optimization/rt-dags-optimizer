#include <string>
#include <iostream>
#include <fstream>
#include <limits> // max uint32
#include <cmath>
#include <algorithm>
#include <experimental/filesystem>

#include <yaml-cpp/yaml.h>


#ifdef __cpp_lib_filesystem
    #include <filesystem>
    using fs = std::filesystem;
#elif __cpp_lib_experimental_filesystem
    #include <experimental/filesystem>
    namespace fs = std::experimental::filesystem;
#else
    #error "no filesystem support ='("
#endif

#include <check_yaml.h>
#include <common.h>

using namespace std;

// check whether the yaml has the expected format
// TODO: in the future we could use a YAML schema validator, like this one https://github.com/psranga/yavl-cpp
uint8_t check_sw_yaml(const char* sw_filename){
    uint32_t i,e,t;
    uint32_t aux_uint;
    // the sum of all actual tasks from all dags
    uint32_t total_tasks=0;
    string key_name, node_msg, dag_msg;

    fs::path sw(sw_filename);
    YAML::Node sw_config;

    // load the sw YAML
    if (!fs::exists(sw)){
        cerr << "ERROR: YAML file '" << sw.string() << "' not found\n\n";
        return false;
    }
    try{
        sw_config = YAML::LoadFile(sw.string().c_str());
    }
    catch (YAML::ParserException & e)
    {
        cerr << "Exception :: " << e.what() << endl;
        cerr << "ERROR: Syntax error in the SW YAML " << sw << endl;
        exit(EXIT_FAILURE);
    }

    if (!sw_config["dags"]){
        cerr << "ERROR: a list of DAGs is mandatory\n\n";
        exit(EXIT_FAILURE);
    }
    // checking for unsupported keys under the root
    for(YAML::const_iterator it=sw_config.begin();it!=sw_config.end();++it) {
        key_name = it->first.as<std::string>();
        if (key_name != "dags" ){
            cerr << "ERROR: unexpected key '" << key_name << "' under the YAML root" << endl;
            exit(EXIT_FAILURE);
        }
    }
 
    if (sw_config["dags"].size() < 1){
        cerr << "ERROR: at least one dag is requried" << endl;
        exit(EXIT_FAILURE);
    }
    if (sw_config["dags"].size() > MAX_DAGS){
        cerr << "ERROR: the input file exceed the maximum number of dags " << MAX_DAGS << endl;
        cerr << "It is possible to change the definition MAX_DAGS and recompile the program." << endl;
        exit(EXIT_FAILURE);
    }

    for (i=0;i<sw_config["dags"].size();i++){
        YAML::Node dag = sw_config["dags"][i];
        dag_msg = "DAG '" + std::to_string(i) + "'";
        // checking for unsupported keys for a task
        for(YAML::const_iterator it=dag.begin();it!=dag.end();++it) {
            key_name = it->first.as<std::string>();
            if (key_name != "activation_period" && key_name != "deadline" && key_name != "edge_list"  && key_name != "tasks")
            {
                cerr << "ERROR: unexpected key '" << key_name << "' in " << dag_msg << endl;
                exit(EXIT_FAILURE);
            }
        }
        if (!dag["activation_period"]){
            cerr << "ERROR: key 'activation_period' not found in " << dag_msg << endl;
            exit(EXIT_FAILURE);
        }
        if (!dag["deadline"]){
            cerr << "ERROR: key 'deadline' not found in " << dag_msg << endl;
            exit(EXIT_FAILURE);
        }
        try{
            // all values must be unsigned int
            aux_uint = dag["activation_period"].as<uint32_t>();
        } catch (const std::exception &e) {
            cerr << "ERROR: key 'activation_period' expects unsigned int values. See " << dag_msg << endl;
            exit(EXIT_FAILURE);
        } catch (...) {
            cerr << "ERROR: key 'activation_period' expects unsigned int values. See " << dag_msg << endl;
            exit(EXIT_FAILURE);
        }
        try{
            // all values must be unsigned int
            aux_uint = dag["deadline"].as<uint32_t>();
        } catch (const std::exception &e) {
            cerr << "ERROR: key 'deadline' expects unsigned int values. See " << dag_msg << endl;
            exit(EXIT_FAILURE);
        } catch (...) {
            cerr << "ERROR: key 'deadline' expects unsigned int values. See " << dag_msg << endl;
            exit(EXIT_FAILURE);
        }
        if (dag["deadline"].as<uint32_t>() > dag["activation_period"].as<uint32_t>()){
            cerr << "ERROR: key 'deadline' must be <= 'activation_period'. See " << dag_msg << endl;
            exit(EXIT_FAILURE);
        }

        // get the tasks
        uint32_t n_tasks = dag["tasks"].size();
        if (n_tasks < 3){
            cerr << "ERROR: at least three tasks are required" << endl;
            exit(EXIT_FAILURE);
        }
        // TODO: the current implementation uses 32bits to encode tasks mapping onto island
        // when we replace int by bit_vector, task number wont be limitated
        // https://github.com/electronicarts/EASTL/blob/master/doc/Modules.md        
        if (n_tasks >= MAX_TASKS_PER_DAG){
            cerr << "ERROR: the max number of tasks per DAG is 32 " << endl;
            exit(EXIT_FAILURE);
        }
        for (t=0;t<n_tasks;t++){
            YAML::Node task = dag["tasks"][t];
            node_msg = "Task '" + std::to_string(t) + "'";
            // checking for unsupported keys for a task
            for(YAML::const_iterator it=task.begin();it!=task.end();++it) {
                key_name = it->first.as<std::string>();
                // TODO: add name attribute as optional. it will easier debugging huge amalthea models
                if (key_name != "wcet_ref" && key_name != "wcet_ref_ns")
                {
                    cerr << "ERROR: unexpected key '" << key_name << "' in " << node_msg << endl;
                    exit(EXIT_FAILURE);
                }
            }
            if (!task["wcet_ref"]){
                cerr << "ERROR: key 'wcet_ref' not found in " << node_msg << endl;
                exit(EXIT_FAILURE);
            }
            if (!task["wcet_ref_ns"]){
                cerr << "ERROR: key 'wcet_ref_ns' not found in " << node_msg << endl;
                exit(EXIT_FAILURE);
            }
            try{
                // all values must be unsigned int
                aux_uint = task["wcet_ref"].as<uint32_t>();
            } catch (const std::exception &e) {
                cerr << "ERROR: key 'wcet_ref' expects unsigned int values. See " << node_msg << endl;
                exit(EXIT_FAILURE);
            } catch (...) {
                cerr << "ERROR: key 'wcet_ref' expects unsigned int values. See " << node_msg << endl;
                exit(EXIT_FAILURE);
            }
            try{
                // all values must be unsigned int
                aux_uint = task["wcet_ref_ns"].as<uint32_t>();
            } catch (const std::exception &e) {
                cerr << "ERROR: key 'wcet_ref_ns' expects unsigned int values. See " << node_msg << endl;
                exit(EXIT_FAILURE);
            } catch (...) {
                cerr << "ERROR: key 'wcet_ref_ns' expects unsigned int values. See " << node_msg << endl;
                exit(EXIT_FAILURE);
            }

            // since wcet_ref_ns represents a part of wcet_ref, wcet_ref < wcet_ref_ns must always be false
            if (task["wcet_ref"].as<uint32_t>() < task["wcet_ref_ns"].as<uint32_t>()){
                cerr << "ERROR: 'wcet_ref' < 'wcet_ref_ns'. See " << node_msg << endl;
                exit(EXIT_FAILURE);
            }
            // sched_deadline does not accept tasks shorter than 1024 ns
            if (task["wcet_ref"].as<uint32_t>() < 1024){
                cerr << "ERROR: wcet_ref < 1024 ns, which is not supported by sched_deadline. See " << node_msg << endl;
                exit(EXIT_FAILURE);
            }
        }
        total_tasks += n_tasks;

        // check the edge list
        for (e=0;e<sw_config["dags"][i]["edge_list"].size();e++){
            // TODO: add edge name and size attributes as optional. 
            // it would easier debugging huge amalthea models and validation in the actual platform
            YAML::Node edge_node = sw_config["dags"][i]["edge_list"][e];
            node_msg = "DAG '" + std::to_string(i) + "' edge '" + std::to_string(e) + "'" ;
            try{
                // all values must be unsigned int
                aux_uint = edge_node[0].as<uint32_t>();
                if (aux_uint > n_tasks-1){
                    cerr << "ERROR: wrong edge index. See " << node_msg << endl;
                    exit(EXIT_FAILURE);
                }
                // make sure that the final task is the last one defined in the yaml file
                if (aux_uint == n_tasks-1){
                    cerr << "ERROR: the final task should not have output edges. See " << node_msg << endl;
                    exit(EXIT_FAILURE);
                }
                aux_uint = edge_node[1].as<uint32_t>();
                if (aux_uint > n_tasks-1){
                    cerr << "ERROR: wrong edge index. See " << node_msg << endl;
                    exit(EXIT_FAILURE);
                }
                // make sure that the initial task is the 1st one defined in the yaml file
                if (aux_uint == 0){
                    cerr << "ERROR: the initial task should not have input edges. See " << node_msg << endl;
                    exit(EXIT_FAILURE);
                }

            } catch (const std::exception &e) {
                cerr << "ERROR: key 'edge' expects unsigned int values. See " << node_msg << endl;
                exit(EXIT_FAILURE);
            } catch (...) {
                cerr << "ERROR: key 'edge' expects unsigned int values. See " << node_msg << endl;
                exit(EXIT_FAILURE);
            }
            
        }
    }
    // there is a current limitation that the placement is encoded into a uint32_t
    if (total_tasks > 32){
        cerr << "ERROR: the total number of tasks among all DAGs cannot exceed 32.\n";
        exit(EXIT_FAILURE);
    }
    return (uint8_t)total_tasks;
}

// source: https://stackoverflow.com/questions/52689845/split-comma-separated-string
// load the power data in CSV format
std::vector<float> split_line(const std::string& s, char delimiter=',')                                                                                                                          
{                                                                                                                                                                                             
   std::vector<float> splits;                                                                                                                                                           
   std::string split;                                                                                                                                                                         
   std::istringstream ss(s);                                                                                                                                                                  
   while (std::getline(ss, split, delimiter))                                                                                                                                                 
   {                                                                                                                                                                                          
      splits.push_back(std::stof(split));                                                                                                                                                                
   }                                                                                                                                                                                          
   return splits;                                                                                                                                                                             
}

void sort_islands(const string abs_path, vector < Hw_t >* hw_def, YAML::Node hw, bool ascending=true){
    string str_line;
    uint32_t i;

    for (i=0;i<hw["hw"].size();++i){
        YAML::Node island = hw["hw"][i];
        // island properties
        (*hw_def)[i].capacity = island["capacity"].as<float>();
        (*hw_def)[i].n_pus = island["n_pus"].as<uint32_t>();
        // read the CSV filename
        fs::path csv_filename = abs_path;
        csv_filename /= island["power_file"].as<string>();
        ifstream power_file(csv_filename.string());
        // ignore the 1st line
        std::getline(power_file, str_line);
        while (std::getline(power_file, str_line)){
            vector<float> freq_data = split_line(str_line);
            (*hw_def)[i].power_data.push_back(make_tuple(freq_data[0], freq_data[1], freq_data[2]));
        }
        // power data is sorted by frequency
        sort((*hw_def)[i].power_data.begin(),(*hw_def)[i].power_data.end());
    }
    // sort islands by capacity. bb-search uses ascending order but dag.c uses descending
    if (ascending){
        sort(hw_def->begin(),hw_def->end(),
                [] (const auto& lhs, const auto& rhs) {
                return lhs.capacity < rhs.capacity;
            });
    }else{
        sort(hw_def->begin(),hw_def->end(),
                [] (const auto& lhs, const auto& rhs) {
                return lhs.capacity > rhs.capacity;
            });
    }
}

// the power defition has to be in 'ascending order' from the lowest freq of the lowest capacity island 
// to the highest freq of the highest capacity island
void check_power_data(vector < Hw_t >* hw_def){
    /*
    The idle power should be ascending, otherwise, when comparing the power of two empty islands
    we might have the situation of f1 < f2 but power1 > power2
    */
    vector <float> idle_power;
    /*
    make sure that the linear function that defines every island-freq pair is such that 
    the resulting power is always ascending from the lowest freq of the lowest capacity island 
    to the highest freq of the highest capacity island.
    it means that, for every island_i_freq_j < island_x_freq_y,
    power(island_i_freq_j) is always < power(island_x_freq_y)
    */    
    vector <float> power_linear_coeff;
    float freq,idle,busy;
    // ref_freq is the highest freq of the highest capacity island
    const float ref_freq = get<0> ( (*hw_def)[hw_def->size()-1].power_data[ (*hw_def)[hw_def->size()-1].power_data.size()-1 ] );
    uint32_t i,j;
    for(i=0;i<hw_def->size();++i){
        for(j=0;j<(*hw_def)[i].power_data.size();++j){
            idle = get<2>((*hw_def)[i].power_data[j]);
            busy = get<1>((*hw_def)[i].power_data[j]);
            freq = get<0>((*hw_def)[i].power_data[j]);
            idle_power.push_back(idle);
            power_linear_coeff.push_back(idle+(busy-idle)*ref_freq/freq);
        }
        if(!is_sorted(idle_power.begin(),idle_power.end())){
            cerr << "WARNINIG: idle power is not in ascending order.\n";
            //exit(EXIT_FAILURE);  
        }
        if(!is_sorted(power_linear_coeff.begin(),power_linear_coeff.end())){
            cerr << "WARNINIG: the power linear coeff is not in ascending order.\n";
            //exit(EXIT_FAILURE);  
        }
        idle_power.clear();
        power_linear_coeff.clear();
    }
}

uint8_t check_hw_yaml(const char* hw_filename){
    uint32_t i;
    uint32_t aux_uint;
    (void)aux_uint;
    float aux_float;
    string key_name, node_msg;

    fs::path hw(hw_filename);
    YAML::Node hw_config;

    // load the hw YAML
    if (!fs::exists(hw)){
        cerr << "ERROR: YAML file '" << hw.string() << "' not found\n\n";
        exit (1);
    }
    try{
        hw_config = YAML::LoadFile(hw.string().c_str());
    }
    catch (YAML::ParserException & e)
    {
        cerr << "Exception :: " << e.what() << endl;
        cerr << "ERROR: Syntax error in the HW YAML " << hw << endl;
        exit(EXIT_FAILURE);
    }

    if (!hw_config["hw"]){
        cerr << "ERROR: a list of islands is mandatory\n\n";
        exit(EXIT_FAILURE);
    }
   
    // }
    if (hw_config["hw"].size() < 1){
        cerr << "ERROR: at least one island is requried" << endl;
        exit(EXIT_FAILURE);
    }
    if (hw_config["hw"].size() > MAX_ISLANDS){
        cerr << "ERROR: the input file exceed the maximum number of islands " << MAX_ISLANDS << endl;
        cerr << "It is possible to change the definition MAX_ISLANDS and recompile the program." << endl;
        exit(EXIT_FAILURE);
    }
    
    for (i=0;i<hw_config["hw"].size();i++){
        YAML::Node ip_node = hw_config["hw"][i];
        node_msg = "Island '" + std::to_string(i) + "'";
        // checking for unsupported keys for an IP
        for(YAML::const_iterator it=ip_node.begin();it!=ip_node.end();++it) {
            key_name = it->first.as<std::string>();
            // if (key_name != "capacity" && key_name != "n_pus" &&
            //     key_name != "ref_freq" && key_name != "power_file")
            if (key_name != "capacity" && key_name != "n_pus" && key_name != "power_file")
            {
                cerr << "ERROR: unexpected key '" << key_name << "' in " << node_msg << endl;
                exit(EXIT_FAILURE);
            }
        }
        if (!ip_node["capacity"]){
            cerr << "ERROR: key 'capacity' not found in " << node_msg << endl;
            exit(EXIT_FAILURE);
        }
        if (!ip_node["n_pus"]){
            cerr << "ERROR: key 'n_pus' not found in " << node_msg << endl;
            exit(EXIT_FAILURE);
        }
        if (!ip_node["power_file"]){
            cerr << "ERROR: key 'power_file' not found in " << node_msg << endl;
            exit(EXIT_FAILURE);
        }
        try{
            // all values must be unsigned int
            aux_uint = ip_node["n_pus"].as<uint32_t>();
        } catch (const std::exception &e) {
            cerr << "ERROR: key 'n_pus' expects unsigned int values. See " << node_msg << endl;
            exit(EXIT_FAILURE);
        } catch (...) {
            cerr << "ERROR: key 'n_pus' expects unsigned int values. See " << node_msg << endl;
            exit(EXIT_FAILURE);
        }

        try{
            aux_float = ip_node["capacity"].as<float>();
            if (aux_float <=0.0 || aux_float>1.0){
                cerr << "ERROR: key 'capacity' must be between (0,1]). See " << node_msg << endl;
                exit(EXIT_FAILURE);
            }
        } catch (const std::exception &e) {
            cerr << "ERROR: key 'capacity' expects unsigned float values. See " << node_msg << endl;
            exit(EXIT_FAILURE);
        } catch (...) {
            cerr << "ERROR: key 'capacity' expects unsigned float values. See " << node_msg << endl;
            exit(EXIT_FAILURE);
        }
        // ref_freq key is only expected in the high capacity island
        // if (aux_float == 1.0){
        //     if (!ip_node["ref_freq"]){
        //         cerr << "ERROR: 'ref_freq' key is mandatory for the high capacity island. See in " << node_msg << endl;
        //         exit(EXIT_FAILURE);
        //     }
        //     try{
        //         // all values must be unsigned int
        //         aux_uint = ip_node["ref_freq"].as<uint32_t>();
        //     } catch (const std::exception &e) {
        //         cerr << "ERROR: key 'ref_freq' expects unsigned int values. See " << node_msg << endl;
        //         exit(EXIT_FAILURE);
        //     } catch (...) {
        //         cerr << "ERROR: key 'ref_freq' expects unsigned int values. See " << node_msg << endl;
        //         exit(EXIT_FAILURE);
        //     }            
        // }else{
        //     if (ip_node["ref_freq"]){
        //         cerr << "ERROR: 'ref_freq' key is only expected in the high capacity island. See in " << node_msg << endl;
        //         exit(EXIT_FAILURE);
        //     }
        // }

        // the CSV is supposed to be in the same directory as the yaml file
        // extract the absolut path from the hw yaml file. This is used to load the CSV files
        fs::path abs_path = fs::absolute(hw_filename);
        fs::path csv_filename = abs_path.parent_path();
        // CSV file with power data
        try{
            if (ip_node["power_file"]){
                csv_filename /= ip_node["power_file"].as<string>();
                if (!fs::exists(csv_filename)){
                    cerr << "ERROR: power CSV file not found\n\n";
                    exit(EXIT_FAILURE);
                }
            }
        } catch (const std::exception &e) {
            cerr << "ERROR: key 'power_file' must point to a CSV file" << endl;
            exit(EXIT_FAILURE);
        } catch (...) {
            cerr << "ERROR: key 'power_file' must point to a CSV file" << endl;
            exit(EXIT_FAILURE);
        }
        // check the number of lines of the CSV file
        int number_of_lines = 0;
        std::string line;
        std::ifstream myfile(csv_filename.string());
        while (std::getline(myfile, line))
            ++number_of_lines;
        myfile.close();
        if (number_of_lines > MAX_FREQS+1){
            cerr << "ERROR: the input file exceed the maximum number of lines in the power CSV file of " << MAX_FREQS << endl;
            cerr << "It is possible to change the definition MAX_FREQS and recompile the program." << endl;
            exit(EXIT_FAILURE);
        }
    }
    // there must be at least one island with capacity 1.0
    bool found=false;
    for (i=0;i<hw_config["hw"].size();i++){
        YAML::Node ip_node = hw_config["hw"][i];
        node_msg = "Island '" + std::to_string(i) + "'";

        aux_float = ip_node["capacity"].as<float>();
        if (aux_float == 1.0){    
            found = true;
            break;
        }
    }
    if (!found){
        cerr << "ERROR: there must be at least one island with capacity 1.0" << endl;
        exit(EXIT_FAILURE);
    }

    // it is not allowed to have two islands with the same capacity
    {
        float capacity1, capacity2;
        for (i=0;i<hw_config["hw"].size()-1;i++){
            YAML::Node island_node = hw_config["hw"][i];
            capacity1 = island_node["capacity"].as<float>();
            for (uint32_t ii=i+1;ii<hw_config["hw"].size();ii++){
                YAML::Node island_node2 = hw_config["hw"][ii];
                capacity2 = island_node2["capacity"].as<float>();
                if (capacity1 == capacity2){
                    cerr << "ERROR: it is not allowed to have two islands with the same capacity\n";
                    exit(EXIT_FAILURE);
                }
            }
        }
    }
    // the CSV is supposed to be in the same directory as the yaml file
    // extract the absolut path from the hw yaml file. This is used to load the CSV files
    fs::path abs_path = fs::absolute(hw_filename);
    abs_path = abs_path.parent_path();
    // load the CSV again, now to test whether the frequencies are in descending order
    // store the hw data before printing it because we need to sort the islands by ascending capacity
    vector < Hw_t > hw_def(hw_config["hw"].size());
    sort_islands(abs_path.string(), &hw_def, hw_config);
    check_power_data(&hw_def);

    return hw_config["hw"].size();
}

bool check_yaml(const char* sw_filename, const char* hw_filename){
    // check the mandatory keys
    float total_tasks = (float)check_sw_yaml(sw_filename);
    float n_islands = (float)check_hw_yaml(hw_filename);

    //the adopted encoding of task mapping onto islands is limited to a max of 2^32 
    // possible mapping to be searched.
    uint64_t max_task_encoding =  (uint64_t)pow(n_islands,total_tasks);
    if (max_task_encoding > std::numeric_limits<uint32_t>::max()){
        cerr << "ERROR: total number of islands and tasks is exceeded. It must be n_islands ^ total_tasks < 2^32\n";
        exit(EXIT_FAILURE);
    }

    return true;
}
