#include <iostream>
#include <cstring>
#include <cassert>

#include <experimental/filesystem>

#ifdef __cpp_lib_filesystem
    #include <filesystem>
    using fs = std::filesystem;
#elif __cpp_lib_experimental_filesystem
    #include <experimental/filesystem>
    namespace fs = std::experimental::filesystem;
#else
    #error "no filesystem support ='("
#endif

#include "arguments.h"

// to be defined during linking time
void usage();

// The output_yaml file is optional. when this is not specified, 
//   it prints the results in the stdout
// The initial_power is used to prune the search space. It is optional. 
//   when specified, partial solutions with higher power are ignored
void parse_arguments(int argc, char* argv[], 
    string &dag_filename, string &plat_name,
    string &plat_filename, string &output_yaml,
    float *initial_power){

    // saving the mandatory positional arguments
    string temp_dag_filename;
    string temp_plat_filename;
    if (argc >= 3){
        // 'argv[1]' is the yaml filename defining the dags
        temp_dag_filename = argv[1];
        // 'argv[2]' can either be a hw yaml file or the name of a known hardware platform. to be compatible with dag.c
        temp_plat_filename = argv[2];
    }else{
        usage();
        exit(EXIT_FAILURE);
    }

    // print the executed command
    cout << "$> ";
    for (int i=0;i<argc;i++){
        cout << argv[i] << " ";
    }
    cout << "\n";   

    if (fs::exists(temp_dag_filename)){
        fs::path dag_path = temp_dag_filename;
        dag_filename = fs::canonical(dag_path).string();        
    }else{
        cerr << "ERROR: Sw YAML file '" << temp_dag_filename << "' not found\n\n";
        exit(EXIT_FAILURE);
    } 
    
    // gather the platform filename
    if (temp_plat_filename == "odroid-xu3" || temp_plat_filename == "odroid-xu3-half"){
        char *base_repo_path;
        if ((base_repo_path = getenv("POWER_OPTIM_HOME")) == NULL) {                                                                 
            cerr << "ERROR: when using a platform name instead of a yaml file, the environment variable POWER_OPTIM_HOME is mandatory\n";
            exit(EXIT_FAILURE);
        }
        auto plat_path = fs::path(base_repo_path);
        plat_path /= fs::path("heuristics"); 
        plat_path /= fs::path("bb-search"); 
        plat_path /= fs::path("data");
        plat_path /= fs::path(temp_plat_filename + string(".yaml"));
        plat_filename = plat_path.string();
        plat_name = temp_plat_filename;
    }else{
        auto plat_path = fs::path(temp_plat_filename);
        if (!fs::exists(plat_path)){
            cerr << "ERROR: Hw YAML file '" << plat_path.string() << "' not found\n\n";
            exit(EXIT_FAILURE);
        }
        plat_filename = fs::canonical(plat_path).string();
        // Returns the filename wo its extension. 
        plat_name = plat_path.stem().string();
    }
    if (!fs::exists(plat_filename)){
        cerr << "ERROR: Hw YAML file '" << plat_filename << "' not found\n\n";
        exit(EXIT_FAILURE);
    }        
 

    // skip the 3 first arguments
    argc -=3;  argv +=3;
    // process the optional arguments
    while (argc > 0) {
        cout << *argv << endl;
        if (strcmp(*argv, "-h") == 0 || strcmp(*argv, "--help") == 0) {
            usage();
            exit(0);
        }else if (strcmp(*argv, "-oyaml") == 0) {
            argc--;  argv++;
            assert(argc > 0);
            output_yaml = *argv;
        }else if (strcmp(*argv, "-ip") == 0) {
            argc--;  argv++;
            assert(argc > 0);
            if (sscanf(*argv,"%f", initial_power) != 1){
                cerr << "ERROR: expecting a float for initial power\n\n";
                exit(EXIT_FAILURE);
            }
        }else{
            cerr << "ERROR: unkown argument " << *argv << "\n\n";
            usage();
            exit(EXIT_FAILURE);
        }
        argc--;  argv++;
    }
}
