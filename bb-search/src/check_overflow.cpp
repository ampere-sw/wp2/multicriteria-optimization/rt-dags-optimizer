#include <limits> // max float
#include <stdexcept> // overflow exepctions
#include <vector>
#include <iostream>
#include <cmath> // pow

#include <eval_cfg.h>
#include <check_overflow.h>
#include <hw_model_mat.h>
#include <sw_model_mat.h>
#include <common.h>

using namespace std;

bool check_power_overflow(const SwModelMat* _sw, const HwModelMat* _hw, float* max_power);
bool check_wcet_overflow(const SwModelMat* _sw, const HwModelMat* _hw, float* min_power);

bool check_overflow(const SwModelMat* _sw, const HwModelMat* _hw, float* min_power, float* max_power){
    if (check_power_overflow(_sw,_hw,max_power)){
        return true;
    }
    if (check_wcet_overflow(_sw,_hw,min_power)){
        return true;
    }
    // no error found
    return false;    
}

bool check_power_overflow(const SwModelMat* _sw, const HwModelMat* _hw, float* max_power){
    uint8_t i;
    uint32_t j;
    vector <uint8_t> freq_idx;
    vector < vector < uint32_t > >  placement;
    freq_idx.resize(_hw->islands.size());
    placement.resize(_hw->islands.size());
    for(i=0;i<_hw->n_islands;++i){
        placement[i].resize(_sw->sw_model.dags.size());
    }

    cout << "\nChecking overflow in the power calculation:\n";
    // assign the lowest freq to the lowest capacity islands
    for (i=0;i<_hw->n_islands-1;++i){
        freq_idx[i] = 0;
    }
    // assign the highest freq to the highest capacity island
    freq_idx[i] = _hw->islands[i].freqs.size()-1;

/*  example of semantics
    vector < vector < uint32_t > >  placement = {
        {1,1},   // island 0 task task 0 of dag0 and task 0 of dag1
        {2,0},   // island 1 task task 1 of dag0 and no task of dag1
        {4,0}    // island 2 task task 1 of dag0 and no task of dag1
    };
*/
    // no task is assigned to the lowest capacity islands
    for(i=0;i<_hw->n_islands-1;++i){
        for(j=0;j<_sw->sw_model.dags.size();++j){
            placement[i][j] = 0;
        }
    }
    // all tasks are assigned to the highest capacity island
    for(j=0;j<_sw->sw_model.dags.size();++j){
        // set all bits to 1
        placement[_hw->n_islands-1][j] = (uint32_t)(pow(2.0,(double)_sw->sw_model.dags[j].tasks.size())-1.0);
    }

    // 3rd parameter is to ignore bound violations
    EvalCfg cfg(_sw,_hw,true);
    float curr_best_power = std::numeric_limits<float>::infinity();
    try
    {
        cfg.evaluate(&placement, &freq_idx, curr_best_power, max_power);
    }
    catch(const std::overflow_error& e)
    {
        std::cerr << e.what() << '\n';
        exit(EXIT_FAILURE);
    }
    catch(const std::exception& e)
    {
        std::cerr << e.what() << '\n';
        exit(EXIT_FAILURE);
    }
    // no error found
    return false;    
}

bool check_wcet_overflow(const SwModelMat* _sw, const HwModelMat* _hw, float* min_power){
    uint8_t i;
    uint32_t j;
    vector <uint8_t> freq_idx;
    vector < vector < uint32_t > >  placement;
    freq_idx.resize(_hw->islands.size());
    placement.resize(_hw->islands.size());
    for(i=0;i<_hw->n_islands;++i){
        placement[i].resize(_sw->sw_model.dags.size());
    }

    cout << "\nChecking overflow in the wcet calculation:\n\n";
    // assign all the islands to their lowest freq
    for (i=0;i<_hw->n_islands;++i){
        freq_idx[i] = 0;
    }

    // no task is assigned to the 2nd lowest capacity islands until the highest one
    for(i=1;i<_hw->n_islands;++i){
        for(j=0;j<_sw->sw_model.dags.size();++j){
            placement[i][j] = 0;
        }
    }
    // all tasks are assigned to the lowest capacity island
    for(j=0;j<_sw->sw_model.dags.size();++j){
        // set all bits to 1
        placement[0][j] = (uint32_t)(pow(2.0,(double)_sw->sw_model.dags[j].tasks.size())-1.0);
    }
    EvalCfg cfg(_sw,_hw,true);
    float curr_best_power = std::numeric_limits<float>::infinity();
    try
    {
        cfg.evaluate(&placement, &freq_idx, curr_best_power, min_power);
    }
    catch(const std::overflow_error& e)
    {
        std::cerr << e.what() << '\n';
        exit(EXIT_FAILURE);
    }
    catch(const std::exception& e)
    {
        std::cerr << e.what() << '\n';
        exit(EXIT_FAILURE);
    }
    // no error found
    return false;    
}
