/*
Multi DAG task placement for heterogeneous architectures with multiple islands and support to frequency scaling.

Authors: 
    * Alexandre Amory (May 2022), ReTiS Lab, Scuola Sant'Anna, Pisa, Italy.

Usage: 
$> bb-search <sw.yaml> <hw.yaml> [initial power]
The YAML files are mandatory and the heuristic initial power is optional
*/

// std c++ libs
#include <algorithm>
#include <vector>
#include <iostream>
#include <string>
#include <cassert>
#include <cmath>
#include <chrono>
#include <experimental/filesystem>
#include <limits> // max float

#ifdef __cpp_lib_filesystem
    #include <filesystem>
    using fs = std::filesystem;
#elif __cpp_lib_experimental_filesystem
    #include <experimental/filesystem>
    namespace fs = std::experimental::filesystem;
#else
    #error "no filesystem support ='("
#endif

// external c++ libs
#include <yaml-cpp/yaml.h>
// TODO: replace minizinc by gurobi to try get some performance improvement
// extern "C" {
// #include "gurobi_c.h"
// }

// internal c++ libs
#include <check_yaml.h>
#include <check_overflow.h>
#include <hw_model_mat.h>
#include <sw_model_mat.h>
#include <arguments.h>
#include <output_formats.h>
#include <optim.h>
#include <unrelated_seq.h>

using namespace std;

// static class attribute related to the tranaformation of the unrelated set into string, 
// used in the minizinc model run during the utilization check
string UnrelSeq::s_minizinc_str;
uint8_t UnrelSeq::s_longest_set;

// counts how many times minizinc was executed
uint32_t g_minizinc_counter=0;
uint32_t g_minizinc_ok_counter=0;
uint32_t g_worst_fit_counter=0;

void usage(){
    cout << "bb-search, rev "<< GIT_REV << ", 2021, ReTiS Laboratory, Scuola Sant'Anna, Pisa, Italy\n";
    cout << "Last commit time: " << GIT_DATE <<endl;
    cout << "Usage: bb-search <SW YAML file> <HW YAML file> [options...]\n";
    cout << "  Options:\n";
    cout << "    -h|--help ................ Show this help message and exit\n";
    cout << "    -oyaml filename.yaml .... Specify destination filename for the the optimization output\n";
    cout << "    -ip power ............... Specify the upper bound for power\n";
}

std::string getEnvVar( std::string const & key )
{
    char * val = getenv( key.c_str() );
    return val == NULL ? std::string("") : std::string(val);
}

// check whether the environment are corretly set up
void checking_requirements(void){
    vector<string> tokens ;

// GUROBI is not currently used
// checking GUROBI_HOME enrironment variables
/*
    string gurobi_path = getEnvVar ("GUROBI_HOME");
    if (gurobi_path.empty()){
        cerr << "ERROR: GUROBI_HOME environment variable is not defined\n\n";
        exit(EXIT_FAILURE);
    }
    if (!fs::exists(gurobi_path) || !fs::is_directory(gurobi_path)){
        cerr << "ERROR: GUROBI_HOME points to an invalid directory '" << gurobi_path << "'\n";
        exit(EXIT_FAILURE);
    }  
    gurobi_path = getEnvVar ("GRB_LICENSE_FILE");
    if (gurobi_path.empty()){
        cerr << "ERROR: GRB_LICENSE_FILE environment variable is not defined\n\n";
        exit(EXIT_FAILURE);
    }
    if (!fs::exists(gurobi_path) || !fs::is_regular_file(gurobi_path)){
        cerr << "ERROR: GRB_LICENSE_FILE points to an invalid file '" << gurobi_path << "'\n";
        exit(EXIT_FAILURE);
    }  
    int GRB_major,GRB_minor,GRB_tech;
    GRBversion(&GRB_major,&GRB_minor,&GRB_tech);
    if (GRB_major != 8 || GRB_minor != 1 || GRB_tech != 1){
        cerr << "ERROR: The expected gurobi version is 811, but found '" << GRB_major << GRB_minor << GRB_tech << "'\n";
        exit(EXIT_FAILURE);
    }
*/
    // TODO: check if minizinc is in the path
}

// print 2d matrix
std::ostream& operator<<(std::ostream &out, std::vector<std::vector<uint32_t>> const&v) {
  for(auto &&i : v) {
    for(auto &&j : i) out << j << " ";
    out << std::endl;
  }
  return out;
}

// print vector
std::ostream& operator<<(std::ostream &out, std::vector<uint8_t> const&v) {
  for(auto &&i : v) {
    out << (uint32_t)i << " ";
  }
  out << std::endl;
  return out;
}

std::ostream& operator<<(std::ostream &out, std::vector<float> const&v) {
  for(auto &&i : v) {
    out << i << " ";
  }
  out << std::endl;
  return out;
}

int main(int argc, char* argv[])
{
    string plat_name;
    string plat_filename;
    string dag_filename;
    string output_yaml;
    float starting_power = std::numeric_limits<float>::infinity();

    auto start = chrono::high_resolution_clock::now();
    parse_arguments(argc, argv, dag_filename, plat_name, plat_filename, 
        output_yaml, &starting_power);

    // check whether the yaml has the expected format
    if (!check_yaml(dag_filename.c_str(),plat_filename.c_str())){
        usage();
        exit(EXIT_FAILURE);
    }

    HwModelMat hw(plat_filename.c_str());
    hw.dump();

    SwModelMat sw(hw.n_islands,dag_filename.c_str());

    // called once to set the string related to the unrelated seq used by minizinc
    // it sets a static class member called UnrelSeq::minizinc_str
    //UnrelSeq::set_minizinc_str(&sw); 

    // antecipate a possible overflow error and get the bounds on power
    float max_power=0.0f,min_power=0.0f;
    if (check_overflow(&sw, &hw, &min_power, &max_power)){
        cout << "Overflow error" << endl;
        exit(EXIT_FAILURE);
    }
    cout << "Max power: " << max_power << endl;
    cout << "Min power: " << min_power << endl;

    const uint64_t total_placements = (uint32_t)pow(hw.n_islands,sw.sw_model.get_n_actual_tasks());
    /* This is a weird and suspicious code. why there is this limitation, casting 64bits to 32 bits ?!?!
       This code is being planned to be run in a multithread environment. 
       The total solution space could require more than 32bits. However, 
       each thread will check no more than 2^32 mappings.
    */
    assert(total_placements < std::numeric_limits<uint32_t>::max());
    const uint64_t placements_per_thread = (uint32_t)total_placements;
    Optim opt(&sw, &hw,0,placements_per_thread, starting_power, max_power);
    bool valid_solution = opt.evaluate();
    cout << "\n";
    // the best result
    opt.dump();
    opt.verbose_dump();

    // Get the execution time 
    auto duration = chrono::duration_cast<chrono::microseconds>(chrono::high_resolution_clock::now() - start);
    cout << endl << "Execution time : " << duration.count() << " microseconds\n\n";

    // compact output format used to ease comparison with other optimization tools
    cout << opt.dump_summary() << endl;

    if (valid_solution){
        // detailed output format used to gather statistics
        string detailed_output = opt.dump_yaml();
        detailed_yaml_output_format(detailed_output,
            "bb-search", plat_name, plat_filename, dag_filename, 
            duration.count(), output_yaml);
        cout << "Valid solution found !\n\n";
    }else{
        cout << "Unable to find a valid solution !\n\n";
    }
    // the return code is 0 only if a valid solution was found
    return valid_solution ? 0 : 1;    
}

