#include <iostream>
#include <string>
#include <vector>
#include <cmath>

#include <optim.h>
#include <sw_model_mat.h>
#include <hw_model_mat.h>
#include <output_formats.h>
#include <Instrumentor.h>
#include <log.h>

Optim::Optim(const SwModelMat* _sw, const HwModelMat* _hw, const uint32_t _start_mapping, const uint32_t _n_mappings, const float _heuristic_power, const float _max_power){
    this->sw = _sw;
    this->hw = _hw;
    this->current_mapping = _start_mapping;
    this->n_mappings = _n_mappings;

    best_freq.resize(hw->n_islands);
    best_mapping_per_island.resize(hw->n_islands);
    best_mapping_for_eval.resize(hw->n_islands);
    for (uint32_t i=0;i<best_mapping_for_eval.size();++i){
        best_mapping_for_eval[i].resize(sw->sw_model.dags.size());
    }
    best_mapping = 0;
    best_power = _heuristic_power;

    max_power  = _max_power;

    // freqseq related
    uint8_t power_data_per_island[MAX_ISLANDS];
    for(uint32_t i=0;i<hw->n_islands;i++){
        power_data_per_island[i] = (uint8_t)hw->islands[i].freqs.size();
    }    
    fSeq = new FreqSeq(hw->n_islands,power_data_per_island);

    evalCfg = new EvalCfg(sw,hw);
    // initialize attributes used to ease mapping convertion
    mapping_mask.resize(sw->sw_model.dags.size());
    uint32_t base_task_idx=0,i;
    starting_task_idx.resize(sw->sw_model.dags.size());
    for (i=0;i<mapping_mask.size();++i){
        // EvalCFG uses a mapping encoding such that mapping[islands][dags] and each bit of the 32bits word corresponds to one task
        mapping_mask[i] = (uint32_t)(pow(2.0,(double)sw->sw_model.dags[i].tasks.size())-1.0);
    }
    for (i=0;i<starting_task_idx.size();++i){
        starting_task_idx[i] = base_task_idx;
        base_task_idx += sw->sw_model.dags[i].tasks.size();
    }
}

bool Optim::evaluate( ){
    float curr_power;
    bool valid_cfg;
    const vector<uint8_t>* curr_freq;

    // TODO: both mapping_per_island and mapping_for_eval are representations of the task mapping.
    // Keep only one of them.
    vector<uint32_t> mapping_per_island(hw->n_islands);
    vector < vector<uint32_t> > mapping_for_eval(hw->n_islands);
    for (uint32_t i=0;i<mapping_for_eval.size();++i){
        mapping_for_eval[i].resize(sw->sw_model.dags.size());
    }
    Instrumentor::Instance().beginSession("Profile");
    while (n_mappings > 0){
        {
            /* Currently, when compiled in Release mode, it takes roughly 1us to evaluate a single mapping.
            Thus, in one second it evaluated 1M mappings, 16min to evaluate 1B mappings.
            The current max number of tasks is 32, limited by the used of 32bits variable to mapping encoding.
            The number of total mapping is #islands ^ #tasks. Assuming 2 islands, it results in 4B mappings.
            Thus, aprox 1h to evaluate the max instance size.
            Note that this estimate is over pessimistic because, as the the search converge to a lower power, only the power 
            calculation is executed, which takes less than 0.1us. Thus, 10x faster than the average execution time.

            This also means that, if we get a good heusristic, the avg execution time can get as low as 0.01us.
            Thus, the max instance size would be about 6 min.
            */
            TIMER_SCOPE("Time to evaluate a mapping");
            // TODO: put here any prunning logic to ignore usesless task mappings
            // if (is_bad_mapping())
            //    continue;
            // convert uint32_t current_mapping into a vector of mappings divided per island
            decode_mapping(&mapping_per_island, &mapping_for_eval);
            // check the current mapping against all frequency sequences to find the min power that respects the constraints
            fSeq->initiate(&mapping_per_island);
            do{
                curr_freq = fSeq->get();
                // main function that performs all the constraint checks
                valid_cfg = evalCfg->evaluate(&mapping_for_eval, curr_freq, best_power, &curr_power);
                #if LOG_LEVEL >=4 // DEBUG level
                // to check whether max_power is a true upper bound
                // assert ((valid_cfg && (curr_power <= max_power)) || !valid_cfg );
                if (valid_cfg && (curr_power <= max_power)){
                    cout << "WARNINIG: current power " << curr_power << " > max power " << max_power << endl;
                }
                evalCfg->dump_freq_placement(curr_power,&mapping_per_island,const_cast<vector<uint8_t>*>(curr_freq));
                #endif

                if (valid_cfg){
                    // found a new best solution
                    if (curr_power < best_power){
                        best_power = curr_power;
                        best_freq = *curr_freq;
                        best_mapping = current_mapping;
                        best_mapping_per_island = mapping_per_island;
                        best_mapping_for_eval = mapping_for_eval;
                        this->dump();
                    }
                }else{
                    // bound: mark this solution (and all the freq lower than this one) as not viable/feasible to 
                    // reduce the search for frequency sets
                    fSeq->not_viable();
                }
            }while(fSeq->next());
            ++current_mapping;
            --n_mappings;
        }
    }
    Instrumentor::Instance().endSession();
    return best_mapping == 0 ? false : true;
}

void Optim::dump(){
    printf("Lowest ");
    print_mapping_freq(best_power, &best_mapping_per_island, &best_freq);
}

void Optim::print_mapping_freq(float power, vector<uint32_t>* mapping, vector <uint8_t>* freq){
    char freq_buf[64];
    char map_buf[64];
    uint32_t offset;
    uint32_t i;

    offset = snprintf(freq_buf,sizeof(freq_buf),"%d",(*freq)[0]);
    for (i=1;i<hw->islands.size();++i){
        offset += snprintf(freq_buf + offset,sizeof(freq_buf)-offset,",%d",(*freq)[i]);
    }
    assert(offset < 63);
    offset = snprintf(map_buf,sizeof(freq_buf),"%08X",(*mapping)[0]);
    for (i=1;i<hw->islands.size();++i){
        offset += snprintf(map_buf + offset,sizeof(freq_buf)-offset,",%08X",(*mapping)[i]);
    }
    assert(offset < 63);
    printf("power: %f mapping [%s] at freq [%s]\n", power, map_buf, freq_buf);
}

void Optim::verbose_dump(){
    evalCfg->dump_freq_placement(best_power, &best_mapping_per_island, &best_freq);
}

string Optim::dump_summary(){
    // common function used by al heuristics. 
    return compact_output_format (hw, sw, &best_mapping_for_eval,&best_freq, best_power);
}

string Optim::dump_yaml(){
    // common function used by al heuristics. 
    float curr_power;
    bool valid_cfg;
    (void)valid_cfg;// this can be used when running in Release mode or undefine NDEBUG
    valid_cfg = evalCfg->evaluate(&best_mapping_for_eval, &best_freq, best_power, &curr_power);
    assert (valid_cfg);
    return evalCfg->dump_yaml();
}

/*
# It converts an integer into a list of list representing the actual task mapping onto islands.
# Limitation: supports up to 32 tasks assuming a 32-bit int encodes the task mapping.
# It is based on a numerical system of base 'n_islands' which encodes a task mapping onto islands.
# For example:
# - Assuming 2 islands, i.e., a binary numerical system, the numbers:
#    - 00000:  says that all 5 tasks are mapped onto island 0
#    - 00001:  says that the 1st task is mapped onto island 1 and the rest onto island 0
#    - 00101:  says that the 1st and 3rd tasks are mapped onto island 1 and the rest onto island 0
# - Assuming 3 islands, i.e., a ternary numerical system, the numbers:
#    - 00000:  says that all 5 tasks are mapped onto island 0
#    - 00001:  says that 1st task is mapped onto island 1 and the rest onto island 0
#    - 00002:  says that 1st task is mapped onto island 2 and the rest onto island 0
#    - 00102:  says that 1st task is mapped onto island 2, the 3rd task onto island 1, and the rest onto island 0
*/
void Optim::decode_mapping(vector<uint32_t>* mapping_per_island, vector < vector<uint32_t> >* mapping_for_eval){
    //TIMER_SCOPE("Optim::decode_mapping");
    PROFILE_FUNCTION();
    uint8_t i,t,d;
    uint8_t island;
    const float f_n_island = (float) hw->n_islands;
    const uint32_t n_actual_tasks = sw->sw_model.get_n_actual_tasks();
    for (i=0;i<hw->n_islands;++i){
        (*mapping_per_island)[i] = 0;
        for (d=0;d<(*mapping_for_eval)[i].size();++d){
            (*mapping_for_eval)[i][d] = 0;
        }
    }

/**Assuming an arch with 3 islands and 9 tasks. The max decimal value is 3 ^ 9 = 19683.
 * Then, the max value to be represented is 19682. Using the following eq in Excel
 * "=$A$2/($B$2^C2)", it is possible to see that indeed the max value points all tasks 
 * to the last island, island #2
 * 
 * See file: bb-search/tools/placement_enc-dec.xlsx
 * 
 * encoded uin32_t	n_islands	bit position	“=$A$2/($B$2^C2)”	"=MOD(INT(D2),$B$2)"
            19682	        3	            0	    19682.0000	        2
                                            1	     6560.6667	        2
                                            2	     2186.8889	        2
                                            3	      728.9630	        2
                                            4	      242.9877	        2
                                            5	       80.9959	        2
                                            6	       26.9986	        2
                                            7	        8.9995	        2
                                            8	        2.9998	        2

The encoded value 2 represents that only task 0 is assigned to island 2 and all the others are in island 0
                2	        3	            0	        2.0000	        2
                                            1	        0.6667	        0
                                            2	        0.2222	        0
                                            3	        0.0741	        0
                                            4	        0.0247	        0
                                            5	        0.0082	        0
                                            6	        0.0027	        0
                                            7	        0.0009	        0
                                            8	        0.0003	        0

The encoded value 3 represents that only task 1 is assigned to island 1 and all the others are in island 0
                3	        3	            0	        3.0000	        0
                                            1	        1.0000	        1
                                            2	        0.3333	        0
                                            3	        0.1111	        0
                                            4	        0.0370	        0
                                            5	        0.0123	        0
                                            6	        0.0041	        0
                                            7	        0.0014	        0
                                            8	        0.0005	        0
 */ 
    // convert uint32_t current_mapping into vector of mapping divided per island
    for (t=0;t<n_actual_tasks;++t){
        // check_yaml.cpp checks for possible overflow. this result must be < 2^32. 
        island = current_mapping / ((int) pow(f_n_island,t)) % hw->n_islands;
        (*mapping_per_island)[island] |=  1 << t;
    }
    #if LOG_LEVEL >=4 // DEBUG level
    uint8_t bit_cnt;
    // all tasks must be mapped into one, and only one, island
    for (t=0;t<n_actual_tasks;t++){
        bit_cnt = 0;
        for (i=0;i<hw->n_islands;++i){
            // increment the counter if bit t is set
            bit_cnt = (*mapping_per_island)[i] &  1 << t? bit_cnt+1 : bit_cnt;
        }
        if (bit_cnt != 1){
            cerr << "ERROR: invalid mapping encoding for task " << t << endl;
            exit(EXIT_FAILURE);
        }
    }
    #endif

    // convert vector of mapping divided per island by a matrix diveded by island and per dag
    uint32_t island_mapping;
    for (i=0;i<hw->n_islands;++i){
        island_mapping = (*mapping_per_island)[i];
        for (d=0;d<(*mapping_for_eval)[i].size();++d){
            (*mapping_for_eval)[i][d] |= (island_mapping >> starting_task_idx[d]) &  mapping_mask[d];
        }
    }
}

