#include <iostream>
#include <string>
#include <cassert>

#include <yaml-cpp/yaml.h>

#include <sw_model.h>
#include <log.h>

void SwModel::load(const char* filename){
    uint32_t i,j,e;
    uint8_t source, target;
    char dot_fname[64];

    YAML::Node sw_config = YAML::LoadFile(filename);

    // this does not have constructor, so using resize is ok
    this->dags.resize(sw_config["dags"].size());
    // reserve the memory space, but not call the constructor just yet
    this->tom_dag.reserve(sw_config["dags"].size());
    for (i=0;i<sw_config["dags"].size();i++){
        YAML::Node dag = sw_config["dags"][i];

        // kind of repeating the code, but at least this class is not aware of dag.c stuff
        // now call the constructor in the previously allocated vector space
        this->tom_dag.emplace_back(dag);
        this->tom_dag[i].dump();
        snprintf(dot_fname, sizeof(dot_fname), "dag%u.dot", i);
        this->tom_dag[i].dump_dot(dot_fname);
        this->tom_dag[i].dump_unrelated();

        // copy dag attributes
        this->dags[i].activation_period = dag["activation_period"].as<uint32_t>();
        this->dags[i].deadline = dag["deadline"].as<uint32_t>();

        // load the tasks, 
        this->dags[i].tasks.resize(dag["tasks"].size());
        for (j=0;j<dag["tasks"].size();j++){
            YAML::Node task = dag["tasks"][j];
            this->dags[i].tasks[j].wcet_ref = task["wcet_ref"].as<uint32_t>();
            this->dags[i].tasks[j].wcet_ref_ns = task["wcet_ref_ns"].as<uint32_t>();
        }

        // load the edges, used only to build the minizinc model, used to check the utilization constraint
        for (e=0;e<dag["edge_list"].size();e++){
            YAML::Node edge_node = dag["edge_list"][e];
            source = edge_node[0].as<uint8_t>();
            target = edge_node[1].as<uint8_t>();
            this->dags[i].edge_list.push_back(make_tuple(source, target));
        }

        // duplicate unrelated list
        this->tom_dag[i].copy_unrelated_tasks(this->dags[i].unrelated);
    }
    assert(dags.size() == tom_dag.size());
}

void SwModel::dump() const{
    uint32_t i,j,k;

    cout << "DAGs:" << endl;
    for (i=0;i<this->dags.size();i++){
        cout << " - DAG: " << i << ", activation_period:" << this->dags[i].activation_period << ", deadline:" << this->dags[i].deadline << endl;

        cout << "   Tasks:" << endl;
        for (j=0;j<this->dags[i].tasks.size();j++){
            cout << "     - Task: " << j << ", wcet_ref:" << this->dags[i].tasks[j].wcet_ref << ", wcet_ref_ns:" << this->dags[i].tasks[j].wcet_ref_ns << endl;
        }

        assert (this->dags[i].unrelated.size() > 0);
        cout << "   Unrelated:" << endl;
        for (j = 0; j < this->dags[i].unrelated.size(); j++) {
            printf("{ ");
            for (k = 0; k <= dags[i].tasks.size(); k++){
                if (this->dags[i].unrelated[j] & (1<<k)){
                    printf("%u, ", k);
                }
            }
            printf("}, ");
        }
        printf("\n");
    }
    cout << "\n";
}

uint32_t  SwModel::get_n_actual_tasks() const{
    assert(dags.size() > 0);
    uint32_t total_tasks=0;
    for(uint32_t i=0;i<dags.size();++i){
        total_tasks += dags[i].tasks.size();
    }
    return total_tasks;
}

bool SwModel::get_rel_deadline(const vector < vector <float> >* wcet, vector < vector <float> >* rel_deadline, bool ignore_bounds) const {
    uint32_t d;
    uint8_t t;
    float path_length;
    
    // for each dag, 
    LOG(INFO,"Calculate the relative deadline for all nodes\n");
    for (d=0;d<wcet->size();++d){
        // copy data to dag.c internal data structure
        for (t=0;t<(*wcet)[d].size();++t){
            tom_dag[d].set_wcet(t, (*wcet)[d][t]);
        }
        // calculate the relative deadlines in dag.c
        tom_dag[d].set_rel_deadline();
        // and copy the result back
        for (t=0;t<(*wcet)[d].size();++t){
            (*rel_deadline)[d][t] = tom_dag[d].get_deadline(t);
            LOG(INFO,"Dag: %u task: %u wcet: %f rel_deadline: %f\n", d, (uint32_t)t, (*wcet)[d][t], (*rel_deadline)[d][t]);
            // check whether a single task exceed the dag deadline
            // #if LOG_LEVEL >=4 // debug level
                if ((*rel_deadline)[d][t] <= 0){
                    cerr << "WARNING: SwModel::get_rel_deadline(): returned negative values.\n";
                    exit(1);
                    // return false;
                }
            // #endif
            // check whether a single task exceed the dag deadline
            if (!ignore_bounds && ((*rel_deadline)[d][t] > dags[d].deadline)){
                LOG(INFO,"Dag: %u task: %u, wcet: %f > dag deadline: %f\n", d, (uint32_t)t, (*wcet)[d][t], dags[d].deadline);
                return false;
            }
            // if this is true, it is possible to avoid calculating the longest path
            if (!ignore_bounds && ((*wcet)[d][t] > (*rel_deadline)[d][t])){
                LOG(INFO,"Dag: %u task: %u, wcet: %f > rel_deadline: %f\n", d, (uint32_t)t, (*wcet)[d][t], (*rel_deadline)[d][t]);
                return false;
            }
        }
        // check the longest path if it is shorter than the dag deadline 
        path_length = tom_dag[d].get_longest_path();
        #if LOG_LEVEL >= 3 // Info level
        tom_dag[d].print_longest_path();
        #endif
        if (!ignore_bounds && (path_length > dags[d].deadline)){
            return false;
        }
    }
    return true;
}
