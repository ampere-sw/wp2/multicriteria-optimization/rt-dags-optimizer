#include <iostream>
#include <set>
#include <cassert>

#include "dag_cpp.h"
#include <common.h>

// these are global variables used by dag.h
sys_t *p_sys = NULL;
char *out_lp_fname = NULL;
char *out_sol_fname = NULL;
char *out_sols_pname = NULL;
char *out_dot_fname = NULL;
char *gurobi_log_fname = NULL;
int gurobi_max_threads;
float lp_scale;
float max_util = DEF_MAX_UTIL;
int create_non_binary_bounds;

DagCpp::DagCpp(const YAML::Node dag_node){
    uint32_t i;
    cout << "dag:\n";
    const YAML::Node tasks_node = dag_node["tasks"];

    // TODO: the current dag.c implementation uses 32bits to encode tasks mapping onto island, 
    // limiting the total numeber of tasks to 32.
    // C++ has the bit_vector, that woulnd limite the task number.
    // https://github.com/electronicarts/EASTL/blob/master/doc/Modules.md
    // in the future, consider to reimplement dag.c to get ride of this limitation
    assert(tasks_node.size() <= 32);

    p_dag = (dag_t*)malloc(sizeof(dag_t));
    assert(p_dag != NULL);
    dag_init(p_dag, dag_node["deadline"].as<uint32_t>(), 
                    dag_node["activation_period"].as<uint32_t>());

    // create the nodes
    uint32_t wcet_ref, wcet_ref_ns;
    for (i=0;i<tasks_node.size();++i){
        wcet_ref = tasks_node[i]["wcet_ref"].as<uint32_t>();
        wcet_ref_ns = tasks_node[i]["wcet_ref_ns"].as<uint32_t>();        
        dag_add_elem(p_dag, wcet_ref, wcet_ref_ns, -1);
    }
    uint32_t source, target;
    // load the nodes and edges
    for (uint32_t e=0;e<dag_node["edge_list"].size();e++){
        YAML::Node edge_node = dag_node["edge_list"][e];
        source = edge_node[0].as<uint32_t>();
        target = edge_node[1].as<uint32_t>();
        dag_add_prev(p_dag, target, source);
    }

    compute_unrelated(p_dag);
    assert (p_dag->unrelated_size > 2);
}


void DagCpp::copy_unrelated_tasks( vector <uint32_t> & unrelated ) {
    // and then duplicate it
    unrelated.resize(p_dag->unrelated_size);
    unrelated.assign(p_dag->unrelated, p_dag->unrelated + p_dag->unrelated_size);
    // checking if these assumptions are always true. If so, then it's easy to identify the 1st and last tasks
    assert (p_dag->first == 0);
    assert (p_dag->last == p_dag->num_elems-1);
/*
    uint32_t u_last = (uint32_t)(1<<p_dag->last);
    // TODO optimization trick: sort the unrelated list such that the first sets are the heaviest ones, i.e. the ones most probably will fail
    // the 'heaviest ones' could be the sum of their utilization assumming the highest freq of the high capacity island
    for (uint32_t i =0; i < unrelated.size(); ++i){
        // this is the unrelated of the first dummy task
        if (unrelated[i] == 1){
            unrelated.erase(unrelated.begin()+i);
        }
        // this is the unrelated of the last dummy task
        if (unrelated[i] == u_last){
            unrelated.erase(unrelated.begin()+i);
        }
    }
*/
    // the unrelated calculated by dag.c considers the first dummy task as index 0. however, bb-search ignores it.
    // so, we need to shift right the unrelated indexes
    // cout << "UNRELATED XXXX:" << endl;
    // for(auto &i: unrelated){
    //     cout << i << endl;
    //     // i = i >> 1;
    // }
}

void DagCpp::print_longest_path() const{
    dag_path_t path;
    dag_path_init(&path);
    float C_sum = dag_longest_path(p_dag, nullptr, p_dag->first, p_dag->last, &path);
    printf("path (C_sum=%f): [ ", C_sum);
    dag_path_dump(&path);
    printf(" ]\n");
}

float DagCpp::get_longest_path() const{
    dag_path_t path;
    dag_path_init(&path);
    float C_sum = dag_longest_path(p_dag, nullptr, p_dag->first, p_dag->last, &path);
    return C_sum;
}