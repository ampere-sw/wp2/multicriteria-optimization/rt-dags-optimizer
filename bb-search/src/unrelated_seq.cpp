#include <iostream>
#include <string>
#include <algorithm>
#include <cassert>

#include <unrelated_seq.h>
#include <sw_model_mat.h>
#include <common.h>
#include <Instrumentor.h>

UnrelSeq::UnrelSeq(const SwModelMat* _sw){
    this->sw = _sw;
    assert(sw->sw_model.dags.size() > 0);
    last_unrelated = 0;
    n_sets = 1;
    for (uint8_t i=0;i<sw->sw_model.dags.size();i++){
        n_sets *= sw->sw_model.dags[i].unrelated.size();
    }
    unrelated_idx.resize(sw->sw_model.dags.size());
    reset_unrelated_idx();
}

void UnrelSeq::reset_unrelated_idx(){
    assert(unrelated_idx.size() > 0);
    for(uint32_t i=0;i<unrelated_idx.size();++i){
        unrelated_idx[i] = 0;
    }
    last_unrelated = 0;
}

bool UnrelSeq::get_next_unrelated(uint32_t * next_unrelated){
    PROFILE_FUNCTION();
    // all vectors are initilized before calling this function
    int i,j;
    int dag_size = (int)sw->sw_model.dags.size();
    bool inced = false;

    // write back the pointers to the current unrelated set of each dag
    //cout << "Current unrelated: " ;
    for (i=0; i<dag_size;++i){
        //cout << sw->sw_model.dags[i].unrelated[unrelated_idx[i]] << ", " ;
        next_unrelated[i] = sw->sw_model.dags[i].unrelated[unrelated_idx[i]];
    }
    //cout << endl;

    // the sequence indexes is not incremented if it reached the end the sequence
    if (last_unrelated == 0){
        // fix the unrelated_idx to point to the next unrelated set
        for (i=dag_size-1; i>=0;i--){
            // increament dag i if the index didnt reach the max
            // cout << sw->sw_model.dags[i].unrelated.size() << endl;
            if (unrelated_idx[i] < sw->sw_model.dags[i].unrelated.size()-1){
                unrelated_idx[i] ++;
                inced= true;
                break;
            // otherwise, zero the current and the next ones
            }else{
                // this way, when we reach the 1st dag we can return to zero all the indexes
                if (i==0)
                    j = 0;
                else
                    j = i+1;
                for (;j<dag_size;++j){
                    unrelated_idx[j] = 0;
                }
            }
        }
        // if it was incremented, then this was not the last unrelated set
        if (!inced){
            last_unrelated ++;
        }
    }else{
        last_unrelated ++;
    }
    return last_unrelated <= 1;
}


void UnrelSeq::set_minizinc_str(const SwModelMat* sw){
    if (UnrelSeq::s_minizinc_str.empty()){
        uint32_t d, j, unrelated;
        const uint8_t n_dags = (uint8_t)sw->sw_model.dags.size();

        UnrelSeq temp_useq(sw);
        uint32_t * curr_unrelated = new uint32_t[sw->sw_model.dags.size()];
        string unrelated_line;
        uint8_t longest_unrelated=0, n_unrelated=0;

        // run the sequence once just to get the longest unrelated set
        temp_useq.reset_unrelated_idx();
        while(temp_useq.get_next_unrelated(curr_unrelated)){
            n_unrelated=0;
            for (d=0;d<n_dags;++d){
                unrelated = curr_unrelated[d];
                while(unrelated != 0){
                    // lsb is set
                    if (unrelated & 1){
                        n_unrelated++;
                    }
                    unrelated >>= 1;
                }
            }
            longest_unrelated = max(n_unrelated, longest_unrelated);
        }

        // run the sequence again, now to generate the unrelated lines
        temp_useq.reset_unrelated_idx();
        bool first_line = true;
        bool first_unrelated_task = true;
        uint8_t task_base_index;
        while(temp_useq.get_next_unrelated(curr_unrelated)){
            if (first_line){
                unrelated_line = "   [| ";
                first_line = false;
            }else{
                unrelated_line = "    | ";
            }
            n_unrelated=0;
            task_base_index = 1;
            for (d=0;d<n_dags;++d){
                first_unrelated_task = true;
                for (j=0;j<=sw->sw_model.dags[d].tasks.size();++j){
                    if (curr_unrelated[d] & (1 << j)){
                        if (first_unrelated_task){
                            unrelated_line += to_string(task_base_index+j);
                            first_unrelated_task = false;
                        }else{
                            unrelated_line += string(",")+to_string(task_base_index+j);
                        }
                        n_unrelated ++;
                    }
                }
                // if this is the last one
                if (n_unrelated < longest_unrelated)
                    unrelated_line += ",";
                task_base_index += sw->sw_model.dags[d].tasks.size()+2;
            }
            // fill the rest of the line with 1s, to represent unused 
            for(;n_unrelated<longest_unrelated-1;++n_unrelated){
                unrelated_line += "1,";
            }
            if (n_unrelated < longest_unrelated)
                unrelated_line += "1";
            UnrelSeq::s_minizinc_str = UnrelSeq::s_minizinc_str + unrelated_line + "\n";
        }
        UnrelSeq::s_longest_set =  longest_unrelated;
        delete[] curr_unrelated;
    }
}
