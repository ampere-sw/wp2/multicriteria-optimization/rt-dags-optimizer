#include <iostream>
#include <string>
#include <sstream>
#include <vector>
#include <cassert>
#include <algorithm>    // std::min_element
#include <bits/stdc++.h>

#include <eval_cfg.h>
#include <sw_model_mat.h>
#include <hw_model_mat.h>
#include <Instrumentor.h>
#include <log.h>

#define ABS(N) ((N<0)?(-N):(N))

extern uint32_t g_minizinc_counter;
extern uint32_t g_minizinc_ok_counter;
extern uint32_t g_worst_fit_counter;

string GetStdoutFromCommand(string cmd);

EvalCfg::EvalCfg(const SwModelMat* _sw, const HwModelMat* _hw, bool _ignore_bounds){
    uint32_t i;
    this->ignore_bounds = _ignore_bounds;
    this->sw = _sw;
    this->hw = _hw;
    placement = nullptr;
    freqs = nullptr;
    curr_best_power = std::numeric_limits<float>::infinity();
    uSeq = new UnrelSeq(sw);
    // setup the internal storage area
    wcet.resize(sw->sw_model.dags.size());
    rel_deadline.resize(sw->sw_model.dags.size());
    for (i=0;i<wcet.size();++i){
        wcet[i].resize(sw->sw_model.dags[i].tasks.size());
        rel_deadline[i].resize(sw->sw_model.dags[i].tasks.size());
    }
    // TODO: failed attempt to avoid creating a new vector in every iteration in the main optimization loop. Try it again later !!!
    //curr_unrelated.resize(sw->sw_model.dags.size());
    // uint8_t max_pu=0;
    // for (i=0;i<hw->n_islands;++i){
    //     if (hw->islands[i].n_pus > max_pu)
    //         max_pu = hw->islands[i].n_pus;
    // }
    // utilization_per_pu.resize(max_pu);
}

bool EvalCfg::evaluate(const vector < vector < uint32_t > >* _placement, const vector < uint8_t >* _freqs, float _curr_best_power, float* _calculated_power){    
    assert ((_placement->size() > 0) && (_placement->size() == hw->islands.size()));
    assert ((_freqs->size() > 0) && (_freqs->size() == hw->islands.size()));
    PROFILE_FUNCTION();
    // save the configuration under evaluation
    placement = _placement;
    freqs = _freqs, 
    curr_best_power = _curr_best_power;
    this->current_power = 0;

#if LOG_LEVEL >=3 // info level
    dump();
#endif

    if (!eval_wcet_power(_calculated_power)){
        return false;
    }
    this->current_power = *_calculated_power;
    if (!eval_rel_deadline()){
        return false;
    }
    if (!eval_utilization()){
        return false;
    }

    return true;
}

bool EvalCfg::eval_wcet_power(float * _calculated_power){
    //TIMER_SCOPE("EvalCfg::eval_wcet_power()");
    PROFILE_FUNCTION();
    // wcet related vars
    float curr_freq, curr_wcet, ref_freq, wcet_divisions, dag_deadline;
    // power related vars
    float total_power=0.0f, island_power, activation_period, island_utilization;
    uint8_t i,d,t; // island, dag, task
    const uint8_t n_dags = (uint8_t)sw->sw_model.dags.size();
    uint32_t curr_placement;
    uint32_t curr_freq_idx;

    for (i=0;i<hw->n_islands;++i){
        curr_freq_idx = (*freqs)[i];

        assert (hw->islands[i].freqs[curr_freq_idx] > 0);
        assert (hw->islands[i].freqs.back() > 0);
        assert (hw->islands[i].capacity > 0.0);

        curr_freq = (float)hw->islands[i].freqs[curr_freq_idx];
        ref_freq = (float) hw->islands[i].freqs.back();
        // do this part of the calc only once per island 'ref_freq / curr_freq /hw->islands[i].capacity'
        wcet_divisions = ref_freq / curr_freq /hw->islands[i].capacity;

        island_utilization = 0.0f;
        for (d=0;d<n_dags;++d){
            activation_period = sw->sw_model.dags[d].activation_period;
            dag_deadline = sw->sw_model.dags[d].deadline;
            curr_placement = (*placement)[i][d];
            // there is no task to be placed for this dag in this island
            if (curr_placement == 0){
                continue;
            }
            for(t=0;t<sw->sw_model.dags[d].tasks.size();++t){
                if (curr_placement & 1){
                    curr_wcet = sw->sw_model.dags[d].tasks[t].wcet_ref_ns + (sw->sw_model.dags[d].tasks[t].wcet_ref - sw->sw_model.dags[d].tasks[t].wcet_ref_ns) * wcet_divisions;

                    // bound: no need to continue to check this configuraion if a single task violated the it's DAG deadline constraint
                    if (!ignore_bounds && (curr_wcet > dag_deadline)){
                        *_calculated_power = std::numeric_limits<float>::max();
                        return false;
                    }
                    wcet[d][t] = curr_wcet;
                    // calculate the power
                    island_utilization += curr_wcet/activation_period;
                }
                
                curr_placement = curr_placement >> 1;
                // no more tasks placed
                if (curr_placement == 0)
                    break;
            }
        }
        // TODO: how to enable the user to inform how power must be calculated, because this changes from platform to platform
        island_power = hw->islands[i].idle_power[curr_freq_idx] + (hw->islands[i].busy_power[curr_freq_idx] * island_utilization);
        LOG(INFO,"Island %u  power: %f\n", i, island_power);
        total_power += island_power;
    }
    #if LOG_LEVEL >=4 // DEBUG level
    cout << "Total power: " << total_power << endl << endl;
    for(d=0;d<wcet.size();++d){
        for(t=0;t<wcet[d].size();++t){
            cout << "DAG: " << (uint32_t)d << " task: " << (uint32_t)(t+1) << " wcet: " << wcet[d][t] << endl;
        }
    }    
    #endif
    *_calculated_power = total_power;
    return total_power <= curr_best_power;
}

bool EvalCfg::eval_rel_deadline(){
    //TIMER_SCOPE("EvalCfg::eval_rel_deadline()");
    PROFILE_FUNCTION();
    return sw->sw_model.get_rel_deadline(&wcet,&rel_deadline,ignore_bounds);
}

// sort in descending order
bool UtilizationSort(const tuple<float, uint32_t, uint32_t>& a,
                 const tuple<float, uint32_t, uint32_t>& b) {
  return std::get<0>(a) > std::get<0>(b);
}

bool EvalCfg::eval_utilization(){
    // calculate the intesection of the placement with the unrelated tasks using 'and' 
    // calculate the utilization for the tasks in the intersection (float(G.nodes[t]['wcet']) / float(G.nodes[t]['rel_deadline'])
    // sort the utilization in decreasing order
    // for each in PU, apply worst-fit() to the tasks assinged to this PU
    // if (utilization[pu] <= DEF_MAX_UTIL
    //    return true
    // else{
    //     build_minizinc_model();
    //     return run_minizinc();
    // }
    //TIMER_SCOPE("EvalCfg::eval_utilization()");
    PROFILE_FUNCTION();
    uint32_t i,j,t;
    float utilization,min_util;
    uint32_t * curr_unrelated = new uint32_t[sw->sw_model.dags.size()];
    assert (curr_unrelated != nullptr);
    const uint8_t n_dags = (uint8_t)sw->sw_model.dags.size();
    uint8_t d,pu,min_util_pu;
    uint32_t place_mask;


    // the utilization requirement for each task. pre-compute it to avoid doing several times
    vector < vector < float > > task_utilization;
    task_utilization.resize(n_dags);
    for (i=0;i<n_dags;++i){
        task_utilization[i].resize(sw->sw_model.dags[i].tasks.size());
        for (j=0;j<task_utilization[i].size();++j){
            task_utilization[i][j] = wcet[i][j] / rel_deadline[i][j];
            // TODO: When dag.c C and D become float, this assert must hold
            // #if LOG_LEVEL >=3 // debug level
                if (!ignore_bounds && (task_utilization[i][j] > DEF_MAX_UTIL)){
                    LOG(INFO,"Dag %u, task %u has utilization %f\n",i,j,task_utilization[i][j]);
                    return false;
                    // fprintf(stderr, "ERROR: Dag %u, task %u has utilization %f\n",i,j,task_utilization[i][j]);
                    // exit(EXIT_FAILURE);
                }
            // #endif 
        }
        #if LOG_LEVEL >=3
            ostringstream ss; 
            // Convert all but the last element to avoid a trailing ","
            std::copy(task_utilization[i].begin(), task_utilization[i].end()-1, std::ostream_iterator<float>(ss, ", "));
            // Now add the last element with no delimiter
            ss << task_utilization[i].back();                
            LOG(INFO,"Dag %u, Tasks utilization: %s\n", i, ss.str().c_str());
        #endif
    }


    uSeq->reset_unrelated_idx();
    while(uSeq->get_next_unrelated(curr_unrelated)){
        // calculate the utilization for each PU of each island
        for (i=0;i<hw->n_islands;++i){
            // it is the subset of 'task_utilization', but constrained to the current unrelated set and placement
            vector < tuple<float, uint32_t, uint32_t > > worst_fit_task_utilization;
            uint8_t n_unrelated_task_in_island=0;
            // each dag has a unrelated and a placement, this mask shows the tasks we need to test for utilization
            // i.e. the set of tasks that dont have precedence constraint and could be running concurrently in the same PU
            for (d=0;d<n_dags;++d){
                #if LOG_LEVEL >= 3 // info mode
                {   char unrelated_str[64];
                    uint32_t unrelated_str_offset=0;
                    placement_to_str(curr_unrelated[d], unrelated_str, 64, &unrelated_str_offset);
                    assert(strlen(unrelated_str)< 64);
                    LOG(INFO,"Island %u, Dag %u, Unrelated %s\n", i, d, unrelated_str);
                }
                #endif

                // place_mask == 0 means that the island does not include unrealted tasks from dag d
                place_mask = (*placement)[i][d] & curr_unrelated[d];
                t=0;
                while (place_mask > 0){
                    if (place_mask & 1){
                        assert(t < task_utilization[d].size());
                        worst_fit_task_utilization.push_back( make_tuple (task_utilization[d][t],d,t) );
                        ++n_unrelated_task_in_island;
                        LOG(INFO,"Task %u, Utilization %f\n", t, task_utilization[d][t]);
                    }
                    place_mask = place_mask >> 1;
                    t++;
                }
            }
            // if the number of task in this unrelated for this island is <= than the number of PU of this island,
            // than an obvious solution is to place each task in a different PU. This means that it is not necessary to 
            // run worst-fit or minizinc task placement on PUs.
            // TODO: confirm if this is true
            if (n_unrelated_task_in_island <= hw->islands[i].n_pus){
                continue;
            }

            // TODO: do these bound logic to reduce the computation time to check the utilization constraint
            // utilization bounding 1:
            // if the sum of the utilization of the current unrelated set is <= DEF_MAX_UTIL, then the skip worst-fit test because this one is feasible

            // utilization bounding 2: relaxing the constraint the dont alow a task to be split in multiple PUs
            // if the sum of the utilization of the current unrelated divided by the n_pus is > DEF_MAX_UTIL, then return false because not even minizinc will find a solution it

            // once we know the individual utilization of all the tasks of all dags,
            // sort the utilization list in deacreasing order by the 1st item of the tuple, the utilization
            {
            PROFILE_SCOPE("worst-fit");
            sort(worst_fit_task_utilization.begin(),worst_fit_task_utilization.end(), UtilizationSort);
            vector <float> utilization_per_pu(hw->islands[i].n_pus);
            for (tuple<float, uint32_t, uint32_t > u: worst_fit_task_utilization){
                utilization = get<0>(u);
                d = get<1>(u);
                t = get<2>(u);
                // cout << utilization << ","  << d << "," << t << endl;
                // TODO: requires a test with islands with diff number of PUs
                //auto pu_idx = min_element(utilization_per_pu.begin(),utilization_per_pu.begin()+(hw->islands[i].n_pus));
                //auto pu_idx = min_element(utilization_per_pu.begin(),utilization_per_pu.end());
                min_util = utilization_per_pu[0];
                min_util_pu = 0;
                for(pu=0;pu<utilization_per_pu.size();++pu){
                    if (utilization_per_pu[pu] <  min_util){
                        min_util = utilization_per_pu[pu];
                        min_util_pu = pu;
                    }
                }
                if ((utilization_per_pu[min_util_pu] + utilization) > DEF_MAX_UTIL){
                    // TODO: minizinc always returns false
                    if (!ignore_bounds){
                        bool ret_code;
                        (void) ret_code; // unused in release mode
                        ret_code=false;
                        LOG(INFO, "Worst-fit utilization check failed in PU %u and util %f.\n", min_util_pu, utilization_per_pu[min_util_pu]);
                        delete[] curr_unrelated;
                        #ifdef ENABLE_MINIZINC
                            ret_code = run_minizinc(i);
                            LOG(INFO, "Minizinc utilization check %s\n", ret_code?"passed":"failed");
                        #endif
                        return ret_code;
                    }else{
                        // if it is ignoring the checks, then continue to increase the utilization beyond DEF_MAX_UTIL
                        utilization_per_pu[min_util_pu] += utilization;
                    }
                }else{
                    utilization_per_pu[min_util_pu] += utilization;
                }
            }

            #if LOG_LEVEL >=3
                ostringstream ss; 
                // Convert all but the last element to avoid a trailing ","
                std::copy(utilization_per_pu.begin(), utilization_per_pu.end()-1, std::ostream_iterator<float>(ss, ", "));
                // Now add the last element with no delimiter
                ss << utilization_per_pu.back();                
                LOG(INFO,"Island %u, Utilization per PU: %s\n", i, ss.str().c_str());
            #endif

            // global to count how many times worst-fit is executed
            ++g_worst_fit_counter;
            worst_fit_task_utilization.clear();
            utilization_per_pu.clear();
            }
        }
    }
    delete[] curr_unrelated;
    // if it reaches this point, then the utilization constraint was obeyed for all PUs considering the current placement/freq
    return true;
}

bool EvalCfg::run_minizinc(uint8_t island){
    const char* unsatisfiable = "=====UNSATISFIABLE=====";
    // create file with minizinc data file *.dzn
    // by using thread_id, this is ok in terms of race condition when running multithreaded
    std::stringstream filename;
    filename << "data_" << std::this_thread::get_id() << ".dzn";
    FILE * fp = fopen(filename.str().c_str(),"w");
    if(fp == NULL) {
        cerr << "ERROR: minizinc data file not created" << endl;
        exit(EXIT_FAILURE);
    }
    int ret_code = build_minizinc_model(island,fp);
    fclose(fp);
    if (ret_code == 0){
        // this only happens when it finds a single task requiring more than DEF_MAX_UTIL of utilization
        // avoiding the time spent to launch minizinc
        return false;
    }

    // run minizinc
    string minizinc_log = GetStdoutFromCommand(string("minizinc model.mzn ") + filename.str());
    ++g_minizinc_counter;

    #ifdef DEBUG
    if (minizinc_log.find("Warning") != string::npos){
        cout << "minizinc warning" << endl;
    }
    #endif        
    if (minizinc_log.find(unsatisfiable) != string::npos){
        #ifdef DEBUG
        cout << "minizinc failed" << endl;
        #endif
        return false;
    }else{
        #ifdef DEBUG
        cout << "minizinc found a solution" << endl;
        #endif
        ++g_minizinc_ok_counter;
        return true;
    }
}

int EvalCfg::build_minizinc_model(uint8_t island, FILE * fp){
    uint32_t i,j,d,e;
    uint32_t place;
    float util;
    const uint32_t n_dags = sw->sw_model.dags.size();
    uint32_t buffer_offset;
    char buffer[256];

    assert(fp != NULL);
    assert(island < hw->islands.size());

    vector<uint8_t> placement_v;
    fprintf(fp,"%% The problem instance:\n");
    fprintf(fp,"%%   island = %d;\n",island);
    // print the placement
    uint8_t task_base_index = 2;
    for (d=0;d<n_dags;++d){
        place = (*placement)[island][d];
        for (j=0;j<sw->sw_model.dags[d].tasks.size();++j){
            // if the task is not mapped, the utilization is 0.0
            if (place & 1){
                placement_v.push_back(j+task_base_index);
            }
            place = place >> 1;
            // no more tasks placed
            if (place == 0)
                break;        
        }
        task_base_index += sw->sw_model.dags[d].tasks.size()+2;
    }
    // it makes no sense to run minizinc on a empty island
    if (placement_v.size() == 0){
        cerr << "ERROR:  attempt to run minizinc with an empty island\n";
        exit(EXIT_FAILURE);
    }
    buffer_offset = snprintf(buffer,sizeof(buffer),"%d",placement_v[0]);
    for (i=1;i<placement_v.size();++i){
        buffer_offset += snprintf(buffer + buffer_offset,sizeof(buffer)-buffer_offset,",%d",placement_v[i]);
    }
    fprintf(fp,"%%   placement = [%s];\n", buffer);
    fprintf(fp,"%%   freq_seq = %d hz;\n\n", hw->islands[island].freqs[(*freqs)[island]]);
    fprintf(fp,"N_CORES = %d;\n", hw->islands[island].n_pus);
    fprintf(fp,"N_DAGS = %u;\n\n", n_dags);
    // All nodes are included into the model instead of only the ones assigned to the island.
    // This way, it's easier to check the precedence constraints
    fprintf(fp,"%% these two constants represent the sum of all nodes and edges of all DAGs\n");
    uint32_t n_tasks = 0;
    uint32_t n_edges = 0;
    for (i=0;i<n_dags;++i){
        n_tasks += sw->sw_model.dags[i].tasks.size()+2;
        n_edges += sw->sw_model.dags[i].edge_list.size();
    }
    fprintf(fp,"N_NODES = %u;\n", n_tasks);
    fprintf(fp,"N_EDGES = %u;\n\n", n_edges);

    fprintf(fp,"N_UNREL_ROWS = %d;\n", uSeq->get_n_sets());
    fprintf(fp,"N_UNREL_COLS = %d;\n\n", uSeq->get_longest_set());

    fprintf(fp,"%% the deadline of each DAG\n");
    // string str_deadline = [G.graph['deadline'] for G in dags]
    buffer_offset = snprintf(buffer,sizeof(buffer),"%d",(unsigned)sw->sw_model.dags[0].deadline);
    for (d=1;d<n_dags;++d){
        buffer_offset += snprintf(buffer + buffer_offset,sizeof(buffer)-buffer_offset,",%d",(unsigned)sw->sw_model.dags[d].deadline);
    }
    fprintf(fp,"Ddag = [%s];\n\n", buffer);

    fprintf(fp,"%% required to fix the index of the matrices shared among the DAGs\n");
    // string str_first_task = [G.graph['first_task']+1 for G in dags]
    buffer_offset = snprintf(buffer,sizeof(buffer),"1");
    long int total_tasks = 1;
    for (d=1;d<n_dags;++d){
        total_tasks += sw->sw_model.dags[d-1].tasks.size() ;
        buffer_offset += snprintf(buffer + buffer_offset,sizeof(buffer)-buffer_offset,",%ld",total_tasks);
    }
    fprintf(fp,"first_node = [%s];\n", buffer);
    // string str_last_task = [G.graph['last_task']+1 for G in dags]
    total_tasks = sw->sw_model.dags[0].tasks.size()+2;
    buffer_offset = snprintf(buffer,sizeof(buffer),"%ld",total_tasks);
    for (d=1;d<n_dags;++d){
        total_tasks += sw->sw_model.dags[d].tasks.size()+2;
        buffer_offset += snprintf(buffer + buffer_offset,sizeof(buffer)-buffer_offset,",%ld",total_tasks);
    }
    fprintf(fp,"last_node = [%s];\n\n", buffer);

    // gather the utilization and relative deadline required from each task considering all DAGs
    vector <float> task_utilization;
    vector <uint32_t> temp_rel_deadline;
    uint32_t r_dline;
    for (d=0;d<n_dags;++d){
        place = (*placement)[island][d];
        // 1st task has always utilzation 0.0
        task_utilization.push_back(0.0f);
        temp_rel_deadline.push_back(0);
        for (j=0;j<sw->sw_model.dags[d].tasks.size();++j){
            // if the task is not mapped, the utilization is 0.0
            util = 0.0f;
            r_dline = 0;
            if (place & 1){
                util = wcet[d][j] / rel_deadline[d][j];
                r_dline = (uint32_t)rel_deadline[d][j];
            }
            task_utilization.push_back(util);
            temp_rel_deadline.push_back(r_dline);
            place = place >> 1;
        }
        // last task has always utilzation 0.0
        task_utilization.push_back(0.0f);
        temp_rel_deadline.push_back(0);
    }

    buffer_offset = snprintf(buffer,sizeof(buffer),"%d",temp_rel_deadline[0]);
    for (i=1;i<temp_rel_deadline.size();++i){
        buffer_offset += snprintf(buffer + buffer_offset,sizeof(buffer)-buffer_offset,",%d",temp_rel_deadline[i]);
    }
    assert(buffer_offset < sizeof(buffer));
    fprintf(fp,"%% relative deadline to each task\n");
    fprintf(fp,"D = [%s];\n\n", buffer);

    // string str_task_utilization = task_utilization
    buffer_offset = snprintf(buffer,sizeof(buffer),"%1.4f",task_utilization[0]);
    for (i=1;i<task_utilization.size();++i){
        // if a single task has a utilization greater than DEF_MAX_UTIL, abort
        if (task_utilization[i] >= DEF_MAX_UTIL){
            return 0;
        }
        buffer_offset += snprintf(buffer + buffer_offset,sizeof(buffer)-buffer_offset,",%1.4f",task_utilization[i]);
    }
    assert(buffer_offset < sizeof(buffer));
    fprintf(fp,"%% utilization required by task: T[i]/D[i]\n");
    fprintf(fp,"U = [%s];\n\n", buffer);

    fprintf(fp,"%% non related nodes, i.e., nodes that could be executed concurrently\n");
    // # the 1st line
    fprintf(fp,"unrelated_node = \n");
    // # the middle lines
    fprintf(fp,"%s", uSeq->get_minizinc_str().c_str());
    // # the last line
    fprintf(fp,"    |];\n\n");

    fprintf(fp,"%% from: the leaving node for each edge\n");
    fprintf(fp,"%% to: the entering node for each edge\n");
    fprintf(fp,"%% list of edges indicating which nodes are connected\n");
    fprintf(fp,"E =  \n");
    // idx = 0
    // for G in dags:
    //     for n1, n2 in G.edges():
    //         if idx == 0:
    //             fprintf(fp,"   [");
    //         else:
    //             fprintf(fp,"    ");
    //         fprintf(fp,"| "+str(n1+1)+","+str(n2+1)+"\n");
    //         idx = idx = 1
    // base task index needs to be fixed for minizinc. it starts with 1 and it's as all tasks belong to the same dag
    // it goes from 1 to n, where n is the total number of tasks
    task_base_index = 1;
    tuple<uint8_t,uint8_t> edge;
    // the 1st line is a bit different
    edge = sw->sw_model.dags[0].edge_list[0];
    fprintf(fp,"   [| %d,%d\n", task_base_index+get<0>(edge), task_base_index+get<1>(edge));
    // all the reamaning lines
    for (d=0;d<n_dags;++d){
        if (d==0){
            // dont print the 1st line again
            e = 1;
        }else{
            e = 0;
        }
        for (;e<sw->sw_model.dags[d].edge_list.size();++e){
            edge = sw->sw_model.dags[d].edge_list[e];
            fprintf(fp,"    | %d,%d\n", task_base_index+get<0>(edge), task_base_index+get<1>(edge));
        }
        task_base_index += sw->sw_model.dags[d].tasks.size()+2;
    }
    // # the last line
    fprintf(fp,"    |];\n");
    return 1;
}

// source: https://www.jeremymorgan.com/tutorials/c-programming/how-to-capture-the-output-of-a-linux-command-in-c/
string GetStdoutFromCommand(string cmd) {

  string data;
  FILE * stream;
  const int max_buffer = 256;
  char buffer[max_buffer];
  cmd.append(" 2>&1");

  stream = popen(cmd.c_str(), "r");
  if (stream) {
    while (!feof(stream))
      if (fgets(buffer, max_buffer, stream) != NULL) data.append(buffer);
    pclose(stream);
  }
  return data;
}

void EvalCfg::dump(){
    dump_freq_placement2(curr_best_power,placement,freqs);
}

void EvalCfg::dump_freq(const vector < uint8_t>* _freqs, char *str_out, const unsigned str_out_size){
    uint32_t offset;
    uint32_t i;

    // convert the freq index to actual freq in string
    offset = snprintf(str_out,str_out_size,"%d hz",hw->islands[0].freqs[(*_freqs)[0]]);
    for (i=1;i<hw->islands.size();++i){
        offset += snprintf(str_out + offset,str_out_size-offset,",%d hz",hw->islands[i].freqs[(*_freqs)[i]]);
    }
    assert(offset < 63);
}

void EvalCfg::dump_freq_placement2(float power, const vector < vector < uint32_t > >* _placement, const vector < uint8_t>* _freqs){
    char freq_buf[64];
    char map_buf[128];

    uint32_t offset, curr_placement;
    uint32_t i,d;

    dump_freq(_freqs,freq_buf,64);
    // convert the mapping encoding into a human readable format
    offset = 0;
    // and repeat the code :/
    for (i=0;i<hw->islands.size();++i){
        for (d=0;d<sw->sw_model.dags.size();++d){
            curr_placement = (*_placement)[i][d];
            if (curr_placement==0){
                // use '-' to indicate an empty island
                offset += snprintf(map_buf + offset,sizeof(map_buf)-offset,"-");
            }
            placement_to_str(curr_placement, map_buf, sizeof(map_buf), &offset);
            offset--; // overwrite the last ',' w ':', except for the last island
            if (d < sw->sw_model.dags.size()-1){
                offset += snprintf(map_buf + offset,sizeof(map_buf)-offset,":");
            }
        }
        if (i < hw->islands.size()-1){
            offset += snprintf(map_buf + offset,sizeof(map_buf)-offset,";");
        }else{
            //overwrite the ',', for the last island
            map_buf[offset] = '\0';
        }
    }
    assert(offset < 127);
    printf("power: %.4f mapping [%s] at freq [%s]\n", power, map_buf, freq_buf);
}

void EvalCfg::dump_freq_placement(float power, const vector < uint32_t >* _placement, const vector < uint8_t>* _freqs){
    char freq_buf[64];
    char map_buf[128];

    uint32_t offset, curr_placement;
    uint32_t i;

    dump_freq(_freqs,freq_buf,64);
    // convert the mapping encoding into a human readable format
    offset = 0;
    // and repeat the code :/
    for (i=0;i<hw->islands.size();++i){
        curr_placement = (*_placement)[i];
        if (curr_placement==0){
            // use '-' to indicate an empty island
            offset += snprintf(map_buf + offset,sizeof(map_buf)-offset,"-");
        }else{
            placement_to_str(curr_placement, map_buf, sizeof(map_buf), &offset);
            offset--; // overwrite the last ',' w ';', except for the last island
        }
        if (i < hw->islands.size()-1){
           offset += snprintf(map_buf + offset,sizeof(map_buf)-offset,";");
        }else{
            //overwrite the ',', for the last island
            map_buf[offset] = '\0';
        }
    }
    assert(offset < 127);
    printf("power: %.4f mapping [%s] at freq [%s]\n", power, map_buf, freq_buf);
}

void EvalCfg::placement_to_str(uint32_t placement, char *out_str, const uint32_t out_str_size, uint32_t *offset){
    const uint32_t n_actual_tasks = sw->sw_model.get_n_actual_tasks();
    uint32_t t;

    for(t=0;t<n_actual_tasks;++t){
        if (placement & 1){
            *offset += snprintf(out_str + *offset, out_str_size-*offset, "%u,",t);
        }
        placement = placement >> 1;
        // no more tasks placed in this island
        if (placement == 0)
            break;
    }
}

string EvalCfg::dump_yaml(){
/* this part of the yaml file has to be filled in by the optimization tool main function

tool_name: "dag.c"
dag_names: ["rnd_00","rnd_01"]
platform_name: "odroid-xu3"
*/

/* Here is an example of the expected format
power: 1.23
n_dags: 2
islands:
    - capacity: 0.3
      freq: 1200
      tasks: [(0,0),(0,2),(1,0)] // tuple format (dag,task)
      pus:
        - []
        - [(0.4,0,0),(0.2,0,2] // tuple format (utilization, dag, task)
        - []
        - [(0.5,1,0)]
    - capacity: 1.0
      freq: 500
      tasks: [(0,1),(1,1)]
      pus:
        - [(0.3,0,1)]
        - [(0.5,1,1)]
        - []
        - []
tasks:
    - id: (0,0)
      wcet: 1234
      deadline: 1500
    - id: (0,1)
      wcet: 1000
      deadline: 1500
    - id: (0,2)
      wcet: 1000
      deadline: 1200
    - id: (1,0)
      wcet: 500
      deadline: 1000
    - id: (1,1)
      wcet: 500
      deadline: 1000
*/
    string yaml_str;
    // fill the pu placement before saving data
    this->pu_task_placement();

    yaml_str += "n_dags: " + to_string(sw->sw_model.dags.size()) + "\n";
    yaml_str += "power: " + to_string(this->current_power) + "\n";
    yaml_str += "islands:\n";
    for (unsigned i=0;i<hw->islands.size();++i){
        yaml_str += "    - capacity: " + to_string(hw->islands[i].capacity) + "\n";
        uint32_t curr_freq_idx = (*freqs)[i];
        unsigned curr_freq = hw->islands[i].freqs[curr_freq_idx];
        assert (curr_freq != 0);
        yaml_str += "      frequency: " + to_string(curr_freq) + "\n";
        yaml_str += "      tasks: [\n";
        for (unsigned pu=0;pu<task_placement[i].size();++pu){
            for (unsigned t=0;t<task_placement[i][pu].size();++t){
                tuple<float, uint32_t, uint32_t > pu_placement = task_placement[i][pu][t];
                yaml_str += "                ["+to_string(get<1>(pu_placement))+","+to_string(get<2>(pu_placement))+"],\n";
            }
        }
        yaml_str += "             ]\n";
        yaml_str += "      pus:\n";
        for (unsigned pu=0;pu<task_placement[i].size();++pu){
            yaml_str += "              - [\n";
            for (unsigned t=0;t<task_placement[i][pu].size();++t){
                tuple<float, uint32_t, uint32_t > pu_placement = task_placement[i][pu][t];
                yaml_str += "                 ["+to_string(get<0>(pu_placement))+","+to_string(get<1>(pu_placement))+","+to_string(get<2>(pu_placement))+"],\n";
            }
            yaml_str += "                ]\n";
        }
    }
    /*
    - id: (0,0)
      wcet: 1234
      deadline: 1500    
      */
    yaml_str += "tasks:\n";
    for (unsigned d=0;d<sw->sw_model.dags.size();++d){
        for (unsigned t=0;t<sw->sw_model.dags[d].tasks.size();++t){
            // tuple<unsigned, unsigned> task_id = make_tuple(d,t);
            yaml_str += "    - id: [" + to_string(d) + "," + to_string(t) + "]\n";
            yaml_str += "      wcet: " + to_string(wcet[d][t]) + "\n";
            yaml_str += "      deadline: " + to_string(rel_deadline[d][t]) + "\n";
        }
    }

    return yaml_str;
}

void EvalCfg::pu_task_placement(){
    PROFILE_FUNCTION();
    uint32_t i,j,t;
    float utilization,min_util;
    const uint8_t n_dags = (uint8_t)sw->sw_model.dags.size();
    uint8_t d,pu,min_util_pu;
    uint32_t place_mask;

    // ignore whatever it was here
    task_placement.clear();
    task_placement.resize(hw->n_islands);

    // calculate the utilization requirement for each task of all dags. 
    vector < vector < float > > task_utilization;
    task_utilization.resize(n_dags);
    for (i=0;i<n_dags;++i){
        task_utilization[i].resize(sw->sw_model.dags[i].tasks.size());
        for (j=0;j<task_utilization[i].size();++j){
            task_utilization[i][j] = wcet[i][j] / rel_deadline[i][j];
        }
    }

    // place all the tasks 
    for (i=0;i<hw->n_islands;++i){
        vector < tuple<float, uint32_t, uint32_t > > worst_fit_task_utilization;
        task_placement[i].resize(hw->islands[i].n_pus);

        // get all the tasks mapped to this island, no matter the dag
        for (d=0;d<n_dags;++d){
            place_mask = (*placement)[i][d];
            t=0;
            while (place_mask > 0){
                if (place_mask & 1){
                    assert(t < task_utilization[d].size());
                    worst_fit_task_utilization.push_back( make_tuple (task_utilization[d][t],d,t) );
                    LOG(INFO,"Dag %u, task %u, Utilization %f\n", d, t, task_utilization[d][t]);
                }
                place_mask = place_mask >> 1;
                t++;
            }
        }

        // now run minizinc or worst-fit for task placement onto the PUs of island i
        //   which happens in :
        //     task_placement[i][min_util_pu].push_back(u);
        #ifdef ENABLE_MINIZINC
            #error "minizinc not implemented"
            // ret_code = run_minizinc(i);
            // LOG(INFO, "Minizinc utilization check %s\n", ret_code?"passed":"failed");
        #else
            {
            PROFILE_SCOPE("worst-fit");
            sort(worst_fit_task_utilization.begin(),worst_fit_task_utilization.end(), UtilizationSort);
            vector <float> utilization_per_pu(hw->islands[i].n_pus);
            for (tuple<float, uint32_t, uint32_t > u: worst_fit_task_utilization){
                utilization = get<0>(u);
                d = get<1>(u);
                t = get<2>(u);
                // cout << utilization << ","  << d << "," << t << endl;
                // TODO: requires a test with islands with diff number of PUs
                //auto pu_idx = min_element(utilization_per_pu.begin(),utilization_per_pu.begin()+(hw->islands[i].n_pus));
                //auto pu_idx = min_element(utilization_per_pu.begin(),utilization_per_pu.end());
                min_util = utilization_per_pu[0];
                min_util_pu = 0;
                for(pu=0;pu<utilization_per_pu.size();++pu){
                    if (utilization_per_pu[pu] <  min_util){
                        min_util = utilization_per_pu[pu];
                        min_util_pu = pu;
                    }
                }
                // since the unrelated set was previoulsy checked, it's garanteed that the utilization is <= DEF_MAX_UTIL.
                // no need to check it again and it is also no problem to have utilization >= DEF_MAX_UTIL here
                // because this is not accounting the DAG predence constraints, which naturally limits the utilization 

                // the PU min_util_pu receives the task u
                task_placement[i][min_util_pu].push_back(u);
                utilization_per_pu[min_util_pu] += utilization;
            }
            }
        #endif
    }

}
